cff-version: 1.2.0
message: "If you use this software, please cite both the article from preferred-citation and the software itself."
abstract:
  AntiBody Sequence Database, a website to easily build a set of antibody
  chain sequences gathered from different sources.
authors:
  - affiliation: Institut Pasteur
    email: simon.malesys@pasteur.fr
    family-names: Malesys
    given-names: Simon
    orcid: https://orcid.org/0000-0003-1863-1970
  - affiliation: Institut Pasteur
    email: nicolas.maillet@pasteur.fr
    family-names: Maillet
    given-names: Nicolas
    orcid: https://orcid.org/0000-0003-1611-5243
date-released: "2024-06-03"
keywords:
  - amino-acid
  - antibody
  - database
  - deep-learning
  - immunology
  - web
license: GPL-3.0-or-later
repository-code: https://gitlab.pasteur.fr/hub/absd
url: https://absd.pasteur.cloud/
title: ABSD
type: software
version: "1.0"
preferred-citation:
  title: "Research article about ABSD, published in NAR Genomics and Bioinformatics"
  type: article
  doi: 10.1093/nargab/lqae171
  url: https://doi.org/10.1093/nargab/lqae171
  year: 2024
