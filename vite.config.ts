import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import brotliPlugin from 'rollup-plugin-brotli'
import gzipPlugin from 'rollup-plugin-gzip'
import serverConfig from './src/server/config/env'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    brotliPlugin({ minSize: 1000 }),
    gzipPlugin({ minSize: 1000 })
  ],
  root: 'src/client',
  build: {
    outDir: '../../dist',
    emptyOutDir: true
  },
  server: {
    port: 8080,
    proxy: {
      // Proxy to the server API
      '/api/': `http://127.0.0.1:${serverConfig.ABSD_SERVER_PORT}`
    }
  },
  define: {
    // Bundle feature flags to make the build slightly lighter.
    // https://github.com/vuejs/core/tree/main/packages/vue#bundler-build-feature-flags
    __VUE_OPTIONS_API__: false,
    __VUE_PROD_DEVTOOLS__: false,
    __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: false
  }
})
