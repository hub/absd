# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

"""Tests for core.py"""
import os
import sys
import pathlib
import pytest
from collections import defaultdict
from .context import scripts
from scripts import format
from format import core

@pytest.fixture
def path():
    return pathlib.Path(__file__).parent.resolve()

def test_create_db_file():
    """ Not a test, just create a test files in a specific folder """
    with open("../../manual//test.fasta", "w", encoding="utf-8") as tmpf:
        tmpf.write("!Pwet\n")
        tmpf.write(">Poulpe\n")
        tmpf.write("AZERTY\n")
        tmpf.write(">Pwet 1\n")
        tmpf.write("QWERTY\n")
        tmpf.write("\n")
        tmpf.write(">Klick\n")
        tmpf.write("AZERTYU\n")
        tmpf.write("\n")

    with open("../../manual//test2.fasta", "w", encoding="utf-8") as tmpf:
        tmpf.write("!pwet\n")
        tmpf.write(">Poulpe\n")
        tmpf.write("AZERTY\n")
        tmpf.write("\n")
        tmpf.write(">Pwet 1\n")
        tmpf.write("QWERTY\n")
        tmpf.write("\n")
        tmpf.write(">Klick\n")
        tmpf.write("AZERTYU\n")
        tmpf.write("\n")
        tmpf.write(">Klick2\n")
        tmpf.write("QWERTYUU\n")
        tmpf.write("\n")

def test_igg():
    """ Test class 'Sequence(header, seq)'
        and basic functions"""
    head_1 = "bjr"
    seq_1 = "AEVQKQ"
    igg1 = core.Sequence(head_1, seq_1)
    # Test __repr__
    assert repr(igg1) == "bjr\nAEVQKQ\n"

    head_2 = "yooo"
    seq_2 = "AEVQKQ"
    igg2 = core.Sequence(head_2, seq_2)
    # Test __repr__
    assert repr(igg2) == "yooo\nAEVQKQ\n"

    head_3 = "yooo"
    seq_3 = "WASD"
    igg3 = core.Sequence(head_3, seq_3)
    # Test __repr__
    assert repr(igg3) == "yooo\nWASD\n"

    # Test __eq__
    assert igg1 == igg2
    assert igg1 != igg3
    assert igg2 != igg3

def test_is_seq_present():
    """ Test function 'is_seq_present(l_obj_seq, seq)'"""
    tup = []
    tup.append(("Zbla", core.Sequence("Zbla", "AZERTY")))
    tup.append(("Zbla2", core.Sequence("Zbla2", "UIOP")))
    tup.append(("Zbla3", core.Sequence("Zbla3", "WASD")))
    seq = "AEVQKQ"

    assert not core.is_seq_present(tup, seq)

    tup.append(("Zbla4", core.Sequence("Zbla4", "AEVQKQ")))

    assert core.is_seq_present(tup, seq)

def test_clean_sub_seq():
    """ Test function 'clean_sub_seq(all_data)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla0", core.Sequence("Zbla0", "AZERTY")))
    all_data["Zbla"].append(("Zbla1", core.Sequence("Zbla1", "QWERTY")))
    all_data["Pwet"].append(("Pwet0", core.Sequence("Pwet0", "AZERTY")))
    all_data["Pwet"].append(("Pwet1", core.Sequence("Pwet1", "QWERTY")))
    all_data["Pwet"].append(("Pwet2", core.Sequence("Pwet2", "QWERTY")))
    res = core.clean_sub_seq(all_data)

    assert len(all_data) == 2
    assert len(all_data["Zbla"]) == 2
    assert len(all_data["Pwet"]) == 3

    assert len(res) == 2
    assert len(res["Zbla"]) == 2
    assert len(res["Pwet"]) == 2 # cleaned

    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla0", core.Sequence("Zbla0", "AZERTY")))
    all_data["Zbla"].append(("Zbla1", core.Sequence("Zbla1", "QWERTY")))
    all_data["Pwet"].append(("Pwet1", core.Sequence("Pwet1", "QWERTY")))
    all_data["Pwet"].append(("Pwet0", core.Sequence("Pwet0", "AZERTY")))
    all_data["Pwet"].append(("Pwet2", core.Sequence("Pwet2", "QWERTY")))
    all_data["Pwet"].append(("Pwet3", core.Sequence("Pwet3", "QWERTY")))
    res = core.clean_sub_seq(all_data)

    assert len(all_data) == 2
    assert len(all_data["Zbla"]) == 2
    assert len(all_data["Pwet"]) == 4

    assert len(res) == 2
    assert len(res["Zbla"]) == 2
    assert len(res["Pwet"]) == 2 # cleaned

def test_pairing_igg_auto(path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path))'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet light", core.Sequence("Pwet light", "AZERTY")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet light", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))}}
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test", path)

    assert res == good_res

def test_pairing_igg_auto_2(path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet light", core.Sequence("Pwet light", "AZERTY")))
    all_data["Pwet"].append(("Poulpe", core.Sequence("Poulpe", "AZERTY")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet light", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))}}
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test", path)

    assert res == good_res

def test_pairing_igg_manual(capsys, monkeypatch, path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 2", core.Sequence("Zbla 2", "AZERTY")))
    all_data["Zbla"].append(("Zbla 3", core.Sequence("Zbla 3", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet light", core.Sequence("Pwet light", "AZERTY")))
    all_data["Pwet"].append(("Poulpe", core.Sequence("Poulpe", "AZERTY")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 2", "AZERTY"), core.Sequence("Zbla 3", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet light", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))}}

    # Answers from user
    answers = iter(["0-1\n"])
    monkeypatch.setattr('builtins.input', lambda msg: next(answers))
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test3", path)
    capsys.readouterr()

    assert res == good_res

def test_pairing_igg_naive(path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Poulpe 1", core.Sequence("Poulpe 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet light", core.Sequence("Pwet light", "AZERTY")))


    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet light", "AZERTY"), core.Sequence("Poulpe 1", "QWERTY"))}}
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test", path)

    assert res == good_res

def test_pairing_igg_previous(path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Poulpe", core.Sequence("Poulpe", "AZERTY")))
    all_data["Pwet"].append(("Kliqk", core.Sequence("Kliqk", "AZERTYU")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Poulpe", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))}}
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test", path)

    assert res == good_res

def test_pairing_igg_previous_2(path):
    """ Test function 'pairing_igg(all_data, light_chains, heavy_chains, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["pwet"].append(("Poulpe", core.Sequence("Poulpe", "AZERTY")))
    all_data["pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["pwet"].append(("Klick", core.Sequence("Klick", "AZERTYU")))
    all_data["pwet"].append(("Klick2", core.Sequence("Klick2", "QWERTYUU")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    # What we should obtain
    good_res = {}
    res = core.pairing_igg(all_data, light_chains, heavy_chains, "test2", path)

    assert res == good_res

def test_automatic_pairing():
    """ Test function 'automatic_pairing(all_data, light_chains, heavy_chains, all_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))}}
    res, seq_founded, pairing_fail = core.automatic_pairing(all_data, light_chains, heavy_chains, all_chains)

    assert res == good_res
    assert seq_founded == {"Zbla 0", "Zbla 1", "Pwet 0", "Pwet 1"}
    assert pairing_fail == set()

def test_automatic_pairing_2():
    """ Test function 'automatic_pairing(all_data, light_chains, heavy_chains, all_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Poulpe", "AZERTYU")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Poulpe", "QWERTYU")))


    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))}}
    res, seq_founded, pairing_fail = core.automatic_pairing(all_data, light_chains, heavy_chains, all_chains)

    assert res == good_res
    assert seq_founded == {"Zbla 0", "Zbla 1", "Pwet 0", "Pwet 1"}
    assert pairing_fail == {"pwet"}

def test_automatic_pairing_3():
    """ Test function 'automatic_pairing(all_data, light_chains, heavy_chains, all_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0 but longer", core.Sequence("Zbla 0 but longer", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1 but longer", core.Sequence("Pwet 1 but longer", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    # What we should obtain
    good_res = {"Zbla": {(core.Sequence("Zbla 0 but longer", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))},
                "Pwet": {(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1 but longer", "QWERTY"))}}
    res, seq_founded, pairing_fail = core.automatic_pairing(all_data, light_chains, heavy_chains, all_chains)

    assert res == good_res
    assert seq_founded == {"Zbla 0 but longer", "Zbla 1", "Pwet 0", "Pwet 1 but longer"}
    assert pairing_fail == set()

def test_a_pair():
    """ Test function 'a_pair(light, heavy, all_chains)'"""
    light = core.Sequence("Zbla light", "AZERTY")
    heavy = core.Sequence("Zbla 1", "QWERTY")

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    res = core.a_pair(("Zbla light", light), ("Zbla 1", heavy), all_chains)

    # What we should obtain
    good_res = ("zbla", light, heavy)

    assert res == good_res

def test_auto_pairing_easy():
    """ Test function 'auto_pairing_easy(light, heavy, light_chains, heavy_chains, all_chains)'"""
    light = core.Sequence("Zbla 0", "AZERTY")
    heavy = core.Sequence("Zbla 1", "QWERTY")

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    # What we should obtain
    good_res = ("zbla", light, heavy)
    tmp_res, seq_founded, found = core.auto_pairing_easy(("Zbla 0", light), ("Zbla 1", heavy), light_chains, heavy_chains, all_chains)

    assert tmp_res == good_res
    assert seq_founded == {"Zbla 0", "Zbla 1"}
    assert found == True

def test_auto_pairing_easy_fail():
    """ Test function 'auto_pairing_easy(light, heavy, light_chains, heavy_chains, all_chains)'"""
    light = core.Sequence("Zbla 0", "AZERTY")
    heavy = core.Sequence("Pwet 1", "QWERTY")

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]
    all_chains = "|".join(light_chains+heavy_chains)

    tmp_res, seq_founded, found = core.auto_pairing_easy(("Zbla 0", light), ("Pwet 1", heavy), light_chains, heavy_chains, all_chains)

    assert tmp_res == None
    assert seq_founded == set()
    assert found == False

def test_is_pairing_possible():
    """ Test function 'is_pairing_possible(seq_a, seq_b, some_chains)'"""
    light = core.Sequence("Zbla 0", "AZERTY")
    heavy = core.Sequence("Zbla 1", "QWERTY")

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    # What we should obtain
    assert core.is_pairing_possible(("Zbla 0 coucou", light), ("Zbla 1 coucou", heavy), light_chains)

def test_is_pairing_possible_fail():
    """ Test function 'is_pairing_possible(seq_a, seq_b, some_chains)'"""
    light = core.Sequence("Zbla 0", "AZERTY")
    heavy = core.Sequence("Zbla 1", "QWERTY")

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    # What we should obtain
    assert not core.is_pairing_possible(("Zbla 0 coucou", light), ("Zbla 1 couco", heavy), light_chains)

def test_naive_pairing():
    """ Test function 'naive_pairing(all_data, seq_founded, light_chains, heavy_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0 but longer", core.Sequence("Zbla 0 but longer", "AZERTY")))
    all_data["Zbla"].append(("Poulpe 1", core.Sequence("Poulpe 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1 but longer", core.Sequence("Pwet 1 but longer", "QWERTY")))
    all_data["Pwet"].append(("Kliqk 0", core.Sequence("Kliqk 0", "AZERTY")))
    # Nothing already paired
    seq_founded = set()
    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    res = core.naive_pairing(all_data, seq_founded, light_chains, heavy_chains)

    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 0 but longer", "AZERTY"), core.Sequence("Poulpe 1", "QWERTY"))],
                "Pwet": [(core.Sequence("Kliqk 0", "AZERTY"), core.Sequence("Pwet 1 but longer", "QWERTY"))]}

    assert res == good_res
    assert seq_founded == {"Kliqk 0", "Zbla 0 but longer", "Pwet 1 but longer", "Poulpe 1"}

def test_naive_pairing_more_than_2():
    """ Test function 'naive_pairing(all_data, seq_founded, light_chains, heavy_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0 but longer", core.Sequence("Zbla 0 but longer", "AZERTY")))
    all_data["Zbla"].append(("Poulpe 1", core.Sequence("Poulpe 1", "QWERTY")))
    all_data["Zbla"].append(("Pwet 1 but longer", core.Sequence("Pwet 1 but longer", "QWERTY")))
    # Nothing already paired
    seq_founded = set()
    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    res = core.naive_pairing(all_data, seq_founded, light_chains, heavy_chains)

    # What we should obtain
    good_res = {}

    assert res == good_res
    assert seq_founded == set()

def test_naive_pairing_fail():
    """ Test function 'naive_pairing(all_data, seq_founded, light_chains, heavy_chains)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0 but longer", core.Sequence("Zbla 0 but longer", "AZERTY")))
    all_data["Zbla"].append(("Poulpe", core.Sequence("Poulpe", "QWERTY")))

    # Nothing already paired
    seq_founded = set()
    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    res = core.naive_pairing(all_data, seq_founded, light_chains, heavy_chains)

    # What we should obtain
    good_res = {}

    assert res == good_res
    assert seq_founded == set()

def test_get_previous_pairs(path):
    """ Test function 'get_previous_pairs(db_name, path)'"""
    # test.fasta in manual folder
    res = core.get_previous_pairs("test", path)
    assert res == {'Pwet': {('AZERTY', 'QWERTY'), ('AZERTYU', None)}}

def test_previous_pairing(path):
    """ Test function 'previous_pairing(all_data, seq_founded, pairing_fail, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = {"Zbla 0", "Zbla 1"}
    # Which headers need to be paired
    pairing_fail = {"Pwet"}

    res = core.previous_pairing(all_data, seq_founded, pairing_fail, "test", path)

    # What we should obtain
    good_res = {"Pwet": [(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))]}

    assert res == good_res

def test_previous_pairing_nothing(path):
    """ Test function 'previous_pairing(all_data, seq_founded, pairing_fail, db_name, path)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = {"Pwet 0", "Pwet 1"}
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    res = core.previous_pairing(all_data, seq_founded, pairing_fail, "test", path)

    # What we should obtain
    good_res = defaultdict(list)

    assert res == good_res

def test_nb_ids_to_pair():
    """ Test function 'nb_ids_to_pair(all_data, seq_founded, pairing_fail)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = set()
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    res = core.nb_ids_to_pair(all_data, seq_founded, pairing_fail)

    assert res == 2

def test_nb_ids_to_pair_only_1():
    """ Test function 'nb_ids_to_pair(all_data, seq_founded, pairing_fail)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = {"Pwet 0", "Pwet 1"}
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    res = core.nb_ids_to_pair(all_data, seq_founded, pairing_fail)

    assert res == 1

def test_manual_pairing(capsys, monkeypatch, path):
    """ Test function 'manual_pairing(all_data, seq_founded, pairing_fail, db_name, path)'"""
    if os.path.isfile(f"../../manual//db_name.fasta"):
        os.remove("../../manual//db_name.fasta")
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = {"Pwet 0", "Pwet 1"}
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    # Answers from user
    answers = iter(["0-1\n"])
    monkeypatch.setattr('builtins.input', lambda msg: next(answers))
    res = core.manual_pairing(all_data, seq_founded, pairing_fail, "db_name", path)
    capsys.readouterr()
    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))]}
    assert res == good_res

    res = []
    with open("../../manual//db_name.fasta") as tmpf:
        for line in tmpf:
            res.append(line)
    assert len(res) == 6
    assert res[0] == "!Zbla\n"
    assert res[1] == "Zbla 0\n"
    assert res[2] == "AZERTY\n"
    assert res[3] == "Zbla 1\n"
    assert res[4] == "QWERTY\n"
    assert res[5] == "\n"

    os.remove("../../manual//db_name.fasta")

def test_manual_pairing_with_singleton(capsys, monkeypatch, path):
    """ Test function 'manual_pairing(all_data, seq_founded, pairing_fail, db_name, path)'"""
    if os.path.isfile(f"../../manual//db_name.fasta"):
        os.remove("../../manual//db_name.fasta")
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Zbla"].append(("Zbla 2", core.Sequence("Zbla 2", "AZERTYU")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = {"Pwet 0", "Pwet 1"}
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    # Answers from user
    answers = iter(["0-1\n"])
    monkeypatch.setattr('builtins.input', lambda msg: next(answers))
    res = core.manual_pairing(all_data, seq_founded, pairing_fail, "db_name", path)
    capsys.readouterr()
    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))]}
    assert res == good_res

    res = []
    with open("../../manual//db_name.fasta") as tmpf:
        for line in tmpf:
            res.append(line)
    assert len(res) == 9
    assert res[0] == "!Zbla\n"
    assert res[1] == "Zbla 0\n"
    assert res[2] == "AZERTY\n"
    assert res[3] == "Zbla 1\n"
    assert res[4] == "QWERTY\n"
    assert res[5] == "\n"
    assert res[6] == "Zbla 2\n"
    assert res[7] == "AZERTYU\n"
    assert res[8] == "\n"

    os.remove("../../manual//db_name.fasta")

def test_manual_pairing_with_2(capsys, monkeypatch, path):
    """ Test function 'manual_pairing(all_data, seq_founded, pairing_fail, db_name, path)'"""
    if os.path.isfile(f"../../manual//db_name.fasta"):
        os.remove("../../manual//db_name.fasta")
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "AZERTY")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QWERTY")))
    all_data["Zbla"].append(("Zbla 2", core.Sequence("Zbla 2", "AZERTYU")))
    all_data["Zbla"].append(("Zbla 3", core.Sequence("Zbla 3", "QWERTYU")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QWERTY")))
    all_data["Pwet"].append(("Pwet 0", core.Sequence("Pwet 0", "AZERTY")))

    # Nothing already paired
    seq_founded = set()
    # Which headers need to be paired
    pairing_fail = {"Zbla"}

    # Answers from user
    answers = iter(["0-1 2-3\n", "1-0\n"])
    monkeypatch.setattr('builtins.input', lambda msg: next(answers))
    res = core.manual_pairing(all_data, seq_founded, pairing_fail, "db_name", path)
    capsys.readouterr()

    assert len(res) == 2
    assert len(res["Zbla"]) == 2
    assert len(res["Pwet"]) == 1
    assert (core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")) in res["Zbla"]
    assert (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU")) in res["Zbla"]
    assert (core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY")) in res["Pwet"]

    res = []
    with open("../../manual//db_name.fasta") as tmpf:
        for line in tmpf:
            res.append(line)
    assert len(res) == 17
    assert res[0] == "!Zbla\n"
    assert res[1] == "Zbla 0\n"
    assert res[2] == "AZERTY\n"
    assert res[3] == "Zbla 1\n"
    assert res[4] == "QWERTY\n"
    assert res[5] == "\n"
    assert res[6] == "Zbla 2\n"
    assert res[7] == "AZERTYU\n"
    assert res[8] == "Zbla 3\n"
    assert res[9] == "QWERTYU\n"
    assert res[10] == "\n"
    assert res[11] == "!Pwet\n"
    assert res[12] == "Pwet 0\n"
    assert res[13] == "AZERTY\n"
    assert res[14] == "Pwet 1\n"
    assert res[15] == "QWERTY\n"
    assert res[16] == "\n"

    os.remove("../../manual//db_name.fasta")

def test_remove_short_seq():
    """ Test function 'remove_short_seq(full_res, min_size)'"""
    full_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")),
                         (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))],
                "Pwet": [(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))]}

    res, nb_removed = core.remove_short_seq(full_res, 7)
    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))]}

    assert good_res == res
    assert nb_removed == 1

def test_correct_labels():
    """ Test function 'correct_labels(full_res, light_starts, heavy_starts)'"""
    full_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")),
                         (core.Sequence("Zbla 2", "QWERTYU"), core.Sequence("Zbla 3", "AZERTYU"))],
                "Pwet": [(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))]}

    light_starts = ["AZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_labels(full_res, light_starts, heavy_starts)
    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")),
                         (core.Sequence("Zbla 3", "AZERTYU"), core.Sequence("Zbla 2", "QWERTYU"))],
                "Pwet": [(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))]}

    assert good_res == res
    assert corrected == 1

def test_correct_errors():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    light = core.Sequence("Zbla 2", "QWERTYU")
    heavy = core.Sequence("Zbla 3", "AZERTYU")

    light_starts = ["AZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "AZERTYU"), core.Sequence("Zbla 2", "QWERTYU"))

    assert good_res == res
    assert corrected

def test_correct_errors_nop():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    heavy = core.Sequence("Zbla 2", "QWERTYU")
    light = core.Sequence("Zbla 3", "AZERTYU")

    light_starts = ["AZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "AZERTYU"), core.Sequence("Zbla 2", "QWERTYU"))

    assert good_res == res
    assert not corrected

def test_correct_errors_2():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    light = core.Sequence("Zbla 3", "QWEAZERTYU")
    heavy = core.Sequence("Zbla 2", "QWERTYU")

    light_starts = ["AZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "QWEAZERTYU"), core.Sequence("Zbla 2", "QWERTYU"))

    assert good_res == res
    assert not corrected

def test_correct_errors_3():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    light = core.Sequence("Zbla 3", "AZERTYU")
    heavy = core.Sequence("Zbla 2", "AZEQWERTYU")

    light_starts = ["AZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "AZERTYU"), core.Sequence("Zbla 2", "AZEQWERTYU"))

    assert good_res == res
    assert not corrected

def test_correct_errors_4():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    light = core.Sequence("Zbla 3", "AZERTYU")
    heavy = core.Sequence("Zbla 2", "AZEQWERTYU")

    light_starts = ["AZE"]
    heavy_starts = ["QWE", "AZEQWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "AZERTYU"), core.Sequence("Zbla 2", "AZEQWERTYU"))

    assert good_res == res
    assert not corrected

def test_correct_errors_5():
    """ Test function 'correct_errors(light, heavy, light_starts, heavy_starts)'"""
    light = core.Sequence("Zbla 3", "QWEAZERTYU")
    heavy = core.Sequence("Zbla 2", "QWERTYU")

    light_starts = ["AZE", "QWEAZE"]
    heavy_starts = ["QWE"]

    res, corrected = core.correct_errors(light, heavy, light_starts, heavy_starts)
    # What we should obtain
    good_res = (core.Sequence("Zbla 3", "QWEAZERTYU"), core.Sequence("Zbla 2", "QWERTYU"))

    assert good_res == res
    assert not corrected

def test_get_proper_igg():
    """ Test function 'get_proper_igg(full_res, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    full_res = {"Zbla": [(core.Sequence("Zbla 0", "KLIQK"), core.Sequence("Zbla 1", "KLAQK")),
                         (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))],
                "Oups": [(core.Sequence("Oups 0", "KLIK"), core.Sequence("Oups 1", "KLAK"))],
                "Pwet": [(core.Sequence("Pwet 0", "PWETPWET"), core.Sequence("Pwet 1", "POULPE")),
                         (core.Sequence("Pwet 2", "AZERBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBTYU"), core.Sequence("Pwet 3", "QWERBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBTYU"))]}

    light_starts = ["AZE"]
    light_ends = ["TYU"]
    light_ends_before = []
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, validated, not_validated = core.get_proper_igg(full_res, 5, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = {"Zbla": [(core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))]}

    assert good_res == res
    assert validated == 1
    assert not_validated == 4

def test_make_smaller_igg_nothing():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERTYU")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["TYU"]
    light_ends_before = []
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))

    assert good_res == res
    assert valid

def test_make_smaller_igg():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERTYU")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["TY"]
    light_ends_before = []
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERTY"), core.Sequence("Zbla 3", "QWERTYU"))

    assert good_res == res
    assert valid

def test_make_smaller_igg_nop():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERTYU")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["TYR"]
    light_ends_before = []
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))

    assert good_res == res
    assert not valid

def test_make_smaller_igg_before():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERTYUWASD")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["PWET"]
    light_ends_before = ["WASD"]
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTYU"))

    assert good_res == res
    assert valid


def test_make_smaller_igg_before():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERTYUWASD")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["PWET"]
    light_ends_before = ["WASD"]
    heavy_starts = ["QWE"]
    heavy_ends = ["RTY", "ERT"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERTYU"), core.Sequence("Zbla 3", "QWERTY"))

    assert good_res == res
    assert valid

def test_make_smaller_igg_size():
    """ Test function 'make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)'"""
    light = core.Sequence("Zbla 2", "AZERBBBBBBBBBBBBBBBBTYU")
    heavy = core.Sequence("Zbla 3", "QWERTYU")

    light_starts = ["AZE"]
    light_ends = ["TYU"]
    light_ends_before = ["WASD"]
    heavy_starts = ["QWE"]
    heavy_ends = ["TYU"]
    heavy_ends_before = []

    res, valid, reason = core.make_smaller_igg(light, heavy, 10, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before)
    # What we should obtain
    good_res = (core.Sequence("Zbla 2", "AZERBBBBBBBBBBBBBBBBTYU"), core.Sequence("Zbla 3", "QWERTYU"))

    assert good_res == res
    assert not valid

def test_count_pairs():
    """ Test function 'count_pairs(full_res)'"""
    full_res = {"Zbla": [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")),
                         (core.Sequence("Zbla 2", "QWERTYU"), core.Sequence("Zbla 3", "AZERTYU"))],
                "Pwet": [(core.Sequence("Pwet 0", "AZERTY"), core.Sequence("Pwet 1", "QWERTY"))]}

    res = core.count_pairs(full_res)

    assert res == 3

def test_launch_igblast(path):
    """ Test function 'launch_igblast(chain, org_name, res_with_unique_id, path, db_name)'"""
    chain = "LIGHT"
    org_name = "Homo_sapiens"
    db_name = "test"
    full_res = {0: [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))],
                1: [(core.Sequence("Pwet 0", "NFMLTQPHSVSESPGKTVTISCTRSSGSVASDYVQWYQQRPGSAPTTVVYEDNQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDNSSWVFGGGTKLTVLGQPK"), core.Sequence("Pwet 1", "QVQLVQSGAEVKKPGASVKVSCKASGYTFTGYYMHWVRQAPGQGLEWMGWINPNSGGTNYAQKFQGRVTMTRDTSISTAYMELSRLRSDDTAVYYCARSPNPYYYDSSGYYYPGAFDIWGQGTMVTVSS"))]}
    # Unique id for each pair
    cpt_id = 0
    with open(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta", "w", encoding="utf-8") as light_f,\
         open(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta", "w", encoding="utf-8") as heavy_f:
        # For each id
        for an_id in full_res:
            # For each pair
            for pair in full_res[an_id]:
                # Write both sequences
                light_f.write(f">{cpt_id}\n{pair[0].seq}\n")
                heavy_f.write(f">{cpt_id}\n{pair[1].seq}\n")
                cpt_id += 1

    no_group_ids, all_igg_groups = core.launch_igblast(chain, org_name, str(path)+"/../../scripts/format", db_name)

    assert len(no_group_ids) == 1
    assert 0 in no_group_ids
    assert len(all_igg_groups) == 1
    assert all_igg_groups[1] == "IGLV6"

    if os.path.isfile(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta"):
        os.remove(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta")
    if os.path.isfile(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta"):
        os.remove(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta")

def test_get_v_group(path):
    """ Test function 'get_v_group(full_res, org_name, db_name, path)'"""
    org_name = "Homo_sapiens"
    db_name = "test"
    full_res = {0: [(core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY"))],
                1: [(core.Sequence("Pwet 0", "NFMLTQPHSVSESPGKTVTISCTRSSGSVASDYVQWYQQRPGSAPTTVVYEDNQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDNSSWVFGGGTKLTVLGQPK"), core.Sequence("Pwet 1", "QVQLVQSGAEVKKPGASVKVSCKASGYTFTGYYMHWVRQAPGQGLEWMGWINPNSGGTNYAQKFQGRVTMTRDTSISTAYMELSRLRSDDTAVYYCARSPNPYYYDSSGYYYPGAFDIWGQGTMVTVSS"))]}
    
    res_with_unique_id, no_group, all_igg_groups, all_groups_l, all_groups_h = core.get_v_group(full_res, org_name, db_name, str(path)+"/../../scripts/format")
    assert len(res_with_unique_id) == 2

    assert len(no_group) == 1
    assert full_res[0][0] in no_group

    assert len(all_igg_groups) == 1
    assert 1 in all_igg_groups
    assert all_igg_groups[1] == ("IGLV6", "IGHV1")

    assert len(all_groups_l) == 1
    assert "IGLV6" in all_groups_l

    assert len(all_groups_h) == 1
    assert "IGHV1" in all_groups_h

    if os.path.isfile(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta"):
        os.remove(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta")
    if os.path.isfile(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta"):
        os.remove(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta")

def test_print_files(tmp_path, path):
    """ Test function 'print_files(res_with_unique_id, no_group, all_igg_groups, all_groups_l, light_file, heavy_file, path, db_name)'
    WARNING, MODIFY all.txt files!"""
    full_res = {0: (core.Sequence("Zbla 0", "AZERTY"), core.Sequence("Zbla 1", "QWERTY")),
                1: (core.Sequence("Pwet 0", "AZERTYU"), core.Sequence("Pwet 1", "QWERTYU"))}

    folder = tmp_path / "CLEAN"
    folder.mkdir()
    light_file = folder / "light.fasta"
    heavy_file = folder / "heavy.fasta"

    all_igg_groups = {0: ("IGLV1", "IGHV1"), 1: ("IGLV2", "IGHV3")}
    all_groups_l = {"IGLV1", "IGLV2"}

    core.print_files(full_res, set(), all_igg_groups, all_groups_l, light_file, heavy_file, path, "test")

    res_light = []
    with open(str(light_file).replace("/CLEAN/", "/CLEAN/IGLV1_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_light.append(line)
    with open(str(light_file).replace("/CLEAN/", "/CLEAN/IGLV2_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_light.append(line)

    res_heavy = []
    with open(str(heavy_file).replace("/CLEAN/", "/CLEAN/IGLV1_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_heavy.append(line)
    with open(str(heavy_file).replace("/CLEAN/", "/CLEAN/IGLV2_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_heavy.append(line)

    assert len(res_light) == 4
    assert "Zbla 0;IGLV1\n" in res_light
    assert "AZERTY\n" in res_light
    assert "Pwet 0;IGLV2\n" in res_light
    assert "AZERTYU\n" in res_light
    assert len(res_heavy) == 4
    assert "Zbla 1;IGHV1\n" in res_heavy
    assert "QWERTY\n" in res_heavy
    assert "Pwet 1;IGHV3\n" in res_heavy
    assert "QWERTYU\n" in res_heavy

def test_get_igg(capsys, monkeypatch,tmp_path):
    """ Test function 'get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size)'"""
    all_data = defaultdict(list)
    all_data["Zbla"].append(("Zbla 0", core.Sequence("Zbla 0", "NFMLTQPHSVSESPGKTVTISCTRSSGSIASYYVQWYQQRPGSSPTTVIYEDSQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDSSNVVFGGGTKLTVLGQPK")))
    all_data["Zbla"].append(("Zbla 1", core.Sequence("Zbla 1", "QVQLQQSGPGLVKPSQTLSLTCAISGDSVSSNSAAWNWIRQSPSRGLEWLGRTYYRSKWFNDYAVSVQSRITINPDTSKNQFSLQLNSVTPEDTAVYYCARGIVFSYAMDVWGQGTTVTVSS")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "QVQLVQSGAEVKKPGASVKVSCKASGYTFTGYYMHWVRQAPGQGLEWMGWINPNSGGTNYAQKFQGRVTMTRDTSISTAYMELSRLRSDDTAVYYCARSPNPYYYDSSGYYYPGAFDIWGQGTMVTVSS")))
    all_data["Pwet"].append(("Pwet 1", core.Sequence("Pwet 1", "EVQLVQSGAEVKKPGESLKISCKGSGYRFTSYWIVWVRQMPGKGLEWMGIIYPGDFDTKYSPSFQGQVTISADKSISTAYLQWSSLKASDTAMYYCARLGGRYYHDSSGYYYLDYWGQGTLVTVSS")))
    all_data["Pwet"].append(("Pwet light", core.Sequence("Pwet light", "NFMLTQPHSVSESPGKTVTISCTRSSGSVASDYVQWYQQRPGSAPTTVVYEDNQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDNSSWVFGGGTKLTVLGQPK")))

    # Keywords linked to LIGHT sequences
    light_chains = ["0", "light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["1"]

    folder = tmp_path / "CLEAN"
    folder.mkdir()
    light_file = folder / "zbla_Homo_sapiens_light.fasta"
    heavy_file = folder / "zbla_Homo_sapiens_heavy.fasta"

    # Answers from user
    answers = iter(["2-0\n"])
    monkeypatch.setattr('builtins.input', lambda msg: next(answers))
    core.get_igg(all_data, light_chains, heavy_chains, light_file, heavy_file, 5, 200, "Homo_sapiens")
    capsys.readouterr()

    res_light = []
    with open(str(light_file).replace("/CLEAN/", "/CLEAN/IGLV6_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_light.append(line)

    res_heavy = []
    with open(str(heavy_file).replace("/CLEAN/", "/CLEAN/IGLV6_"), encoding="utf-8") as f_name:
        for line in f_name:
            if line != "\n":
                res_heavy.append(line)

    assert len(res_light) == 4
    assert "Zbla 0;IGLV6\n" in res_light
    assert "NFMLTQPHSVSESPGKTVTISCTRSSGSIASYYVQWYQQRPGSSPTTVIYEDSQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDSSNVVFGGGTKLTVLGQPK\n" in res_light
    assert "Pwet light;IGLV6\n" in res_light
    assert "NFMLTQPHSVSESPGKTVTISCTRSSGSVASDYVQWYQQRPGSAPTTVVYEDNQRPSGVPDRFSGSIDSSSNSASLTISGLKTEDEADYYCQSYDNSSWVFGGGTKLTVLGQPK\n" in res_light

    assert len(res_heavy) == 4
    assert "Zbla 1;IGHV6\n" in res_heavy
    assert "QVQLQQSGPGLVKPSQTLSLTCAISGDSVSSNSAAWNWIRQSPSRGLEWLGRTYYRSKWFNDYAVSVQSRITINPDTSKNQFSLQLNSVTPEDTAVYYCARGIVFSYAMDVWGQGTTVTVSS\n" in res_heavy
    assert "Pwet 1;IGHV1\n" in res_heavy
    assert "QVQLVQSGAEVKKPGASVKVSCKASGYTFTGYYMHWVRQAPGQGLEWMGWINPNSGGTNYAQKFQGRVTMTRDTSISTAYMELSRLRSDDTAVYYCARSPNPYYYDSSGYYYPGAFDIWGQGTMVTVSS\n" in res_heavy

def test_clean():
    """ Not a test, just cleaning test files """
    if os.path.isfile("../../manual//db_name.fasta"):
        os.remove("../../manual//db_name.fasta")
    if os.path.isfile("../../manual//all-test.txt"):
        os.remove("../../manual//all-test.txt")
    if os.path.isfile("../../manual//test.fasta"):
        os.remove("../../manual//test.fasta")
    if os.path.isfile("../../manual//test2.fasta"):
        os.remove("../../manual//test2.fasta")
    if os.path.isfile("../../manual//test3.fasta"):
        os.remove("../../manual//test3.fasta")
    if os.path.isfile("../../manual//all-zbla.txt"):
        os.remove("../../manual//all-zbla.txt")
    if os.path.isfile("../../manual//zbla.fasta"):
        os.remove("../../manual//zbla.fasta")
