# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

"""Tests for merge_data.py"""
import unittest.mock
import os
import pytest
import pathlib
from .context import scripts
from scripts import merge_data

@pytest.fixture
def org_name():
    return "Homo_sapiens"

@pytest.fixture
def min_size():
    return 80

@pytest.fixture
def max_size():
    return 150

def test_igg():
    """ Test class 'Igg(header_l, header_h, seq_l, seq_h)'
        and basic functions"""
    head_l1 = "bjr"
    head_h1 = "yooo"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)
    # Test __repr__
    assert repr(igg1) == "{'bjr'}\n{'AEVQKQ'}\n{'yooo'}\n{'VTSSA'}\n"

    head_l2 = "pwet1"
    head_h2 = "hsh1"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)
    assert repr(igg2) == "{'pwet1'}\n{'EVQK'}\n{'hsh1'}\n{'VTSS'}\n"

    # igg3 is igg2
    head_l3 = "pwet3"
    head_h3 = "hsh3"
    seq_l3 = "EVQK"
    seq_h3 = "VTSS"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # Test __eq__
    assert igg1 != igg2
    assert igg3 == igg2

def test_is_in():
    """ Test function 'is_in()' of Igg"""
    head_l1 = "bjr"
    head_h1 = "yooo"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "pwet1"
    head_h2 = "hsh1"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is igg2
    head_l3 = "pwet3"
    head_h3 = "hsh3"
    seq_l3 = "EVQK"
    seq_h3 = "VTSS"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg4 is igg2 and igg1
    igg4 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)
    igg4.header_l.add(head_l1)
    igg4.header_h.add(head_h1)
    igg4.seq_l.add(seq_l1)
    igg4.seq_h.add(seq_h1)

    # igg5 is almost igg2
    igg5 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2+"W")
    igg5.seq_l.add(seq_l2+"W")

    # igg6
    igg6 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2+"X")
    igg6.seq_h.add(seq_h1)


    assert igg2.is_in(igg1)
    assert not igg1.is_in(igg2)

    assert igg3.is_in(igg1)
    assert igg3.is_in(igg2)
    assert igg2.is_in(igg3)

    assert igg1.is_in(igg4)
    assert igg2.is_in(igg4)
    assert igg4.is_in(igg1)

    assert igg4.is_in(igg2)
    
    assert not igg5.is_in(igg2)
    assert not igg5.is_in(igg4)

    assert igg6.is_in(igg4)

def test_is_partially_in():
    """ Test function 'is_partially_in()' of Igg"""
    head_l1 = "bjr"
    head_h1 = "yooo"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "pwet1"
    head_h2 = "hsh1"
    seq_l2 = "EFQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is igg2 for l and igg1 for h
    head_l3 = "pwet3"
    head_h3 = "hsh3"
    seq_l3 = "EVQK"
    seq_h3 = "VTSSA"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg4 is igg2 and igg1
    igg4 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)
    igg4.header_l.add(head_l1)
    igg4.header_h.add(head_h1)
    igg4.seq_l.add(seq_l1)
    igg4.seq_h.add(seq_h1)

    # igg5 is almost igg2
    igg5 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2+"W")
    igg5.seq_l.add(seq_l2+"W")

    # igg6
    igg6 = merge_data.Igg({head_l2}, {head_h2}, seq_l2[:-2]+"X", seq_h2+"X")
    igg6.seq_h.add(seq_h1)

    # igg6
    igg7 = merge_data.Igg({head_l2}, {head_h3}, seq_l3[:-2]+"X", seq_h2)

    # Should be symmetrical
    assert igg1.is_partially_in(igg2)
    assert igg2.is_partially_in(igg1)

    assert igg1.is_partially_in(igg3)
    assert igg2.is_partially_in(igg3)

    assert igg3.is_partially_in(igg4)

    # Diff with is_in function
    assert igg5.is_partially_in(igg2)
    assert not igg5.is_partially_in(igg1)

    assert igg5.is_partially_in(igg4)

    assert igg6.is_partially_in(igg4)

    assert igg3.is_partially_in(igg6)

    assert igg4.is_partially_in(igg7)

def test_merge_headers(capsys):
    """ Test function 'merge_headers()' of Igg"""
    head_l1 = "bjr"
    head_h1 = "yooo"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "pwet1"
    head_h2 = "hsh1"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # Before merge
    assert repr(igg1) == "{'bjr'}\n{'AEVQKQ'}\n{'yooo'}\n{'VTSSA'}\n"
    assert "bjr" in igg1.header_l
    assert "pwet1" not in igg1.header_l
    assert "yooo" in igg1.header_h
    assert "hsh1" not in igg1.header_h

    # Correct merge
    igg1.merge_headers(igg2)
    assert "bjr" in igg1.header_l
    assert "pwet1" in igg1.header_l
    assert "yooo" in igg1.header_h
    assert "hsh1" in igg1.header_h

    # Identical
    assert len(igg1.header_l) == 2
    assert len(igg1.header_h) == 2
    assert len(igg2.header_l) == 1
    assert len(igg2.header_h) == 1
    igg2.merge_headers(igg1)
    assert len(igg1.header_l) == 2
    assert len(igg1.header_h) == 2
    assert len(igg2.header_l) == 2
    assert len(igg2.header_h) == 2
    assert "bjr" in igg1.header_l
    assert "pwet1" in igg1.header_l
    assert "yooo" in igg1.header_h
    assert "hsh1" in igg1.header_h

def test_merge():
    """ Test function 'merge()' of Igg"""
    head_l1 = "bjr"
    head_h1 = "yooo"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "pwet1"
    head_h2 = "hsh1"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # Before merge
    assert repr(igg1) == "{'bjr'}\n{'AEVQKQ'}\n{'yooo'}\n{'VTSSA'}\n"
    assert "bjr" in igg1.header_l
    assert "pwet1" not in igg1.header_l
    assert "AEVQKQ" in igg1.seq_l
    assert "EVQK" not in igg1.seq_l

    assert "yooo" in igg1.header_h
    assert "hsh1" not in igg1.header_h
    assert "VTSSA" in igg1.seq_h
    assert "VTSS" not in igg1.seq_h

    # Correct merge
    igg1.merge(igg2)
    assert "bjr" in igg1.header_l
    assert "pwet1" in igg1.header_l
    assert "AEVQKQ" in igg1.seq_l
    assert "EVQK" in igg1.seq_l

    assert "yooo" in igg1.header_h
    assert "hsh1" in igg1.header_h
    assert "VTSSA" in igg1.seq_h
    assert "VTSS" in igg1.seq_h

def test_get_data(tmp_path, capsys):
    """ Test function 'get_data()'"""

    # Light file bad, one extra line in light
    light_file = tmp_path.joinpath("light.fasta")
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nDIQMEIK\n>light2\nALQLDVR\npwet\n")

    # Heavy file bad
    heavy_file = tmp_path.joinpath("heavy.fasta")
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEVQLVTSS\n>heavy2\nEVQLTSS\n")

    # Different number of line
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        merge_data.get_data(light_file, heavy_file, "test")
    assert pytest_wrapped_e.value.code == 1
    captured = capsys.readouterr()
    assert "Error: not same number of lines in" in captured.out


    # Light file empty
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write("")

    # Heavy file empty
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write("")
    # Empty files
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        merge_data.get_data(light_file, heavy_file, "test")
    assert pytest_wrapped_e.value.code == 1
    captured = capsys.readouterr()
    assert "Error: files" in captured.out
    assert "are empty" in captured.out


    # Light file ok
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nDIQMEIK\n>light2\nALQLDVR\n")

    # Heavy file ok
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEVQLVTSS\n>heavy2\nEVQLTSS\n")
    # Everything is fine
    all_igg, nb_igg = merge_data.get_data(light_file, heavy_file, "test")
    captured = capsys.readouterr()
    # Test output
    assert "IgG founded in files" in captured.out
    # There should be 2 igg in all_igg
    assert len(all_igg) == nb_igg
    assert len(all_igg) == 2
    # Test both igg seq (headers are not taken into account)
    igg1 = merge_data.Igg({"light1;test"}, {"heavy1;test"}, "DIQMEIK", "EVQLVTSS")
    assert igg1 in all_igg
    igg2 = merge_data.Igg({"light2_test"}, {"heavy2_test"}, "ALQLDVR", "EVQLTSS")
    assert igg2 in all_igg
    # Test headers
    for i in all_igg:
        if i == igg1:
            assert i.header_l == igg1.header_l
            assert i.header_h == igg1.header_h
        if i == igg2:
            assert i.header_l != igg2.header_l
            assert i.header_h != igg2.header_h

def test_remove_duplicate_from_list():
    """ Test function 'remove_duplicate_from_list(all_igg)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is igg2
    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "EVQK"
    seq_h3 = "VTSS"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg4 is igg2 and igg1
    head_l4 = "igg4_l"
    head_h4 = "igg4_h"
    igg4 = merge_data.Igg({head_l4}, {head_h4}, seq_l2, seq_h2)
    igg4.seq_l.add(seq_l1)
    igg4.seq_h.add(seq_h1)

    # igg5 is almost igg2
    head_l5 = "igg5_l"
    head_h5 = "igg5_h"
    seq_l5 = seq_l2+"W"
    seq_h5 = seq_h2+"W"
    igg5 = merge_data.Igg({head_l5}, {head_h5}, seq_l5, seq_h5)

    # igg6 is unique
    head_l6 = "igg6_l"
    head_h6 = "igg6_h"
    seq_l6 = "DIMQS"
    seq_h6 = "KEIKL"
    igg6 = merge_data.Igg({head_l6}, {head_h6}, seq_l6, seq_h6)

    # igg7 is igg2 and something different, with two seq_l/seq_h
    # It will be merge in something,
    # because at least one l and h of its seq are in some another
    head_l7 = "igg7_l"
    head_h7 = "igg7_h"
    igg7 = merge_data.Igg({head_l7}, {head_h7}, seq_l2, seq_h2)
    igg7.seq_l.add("AZERTY")
    igg7.seq_h.add("QWERTY")

    all_igg = [igg2, igg1, igg3, igg4, igg5, igg6, igg7]

    # igg2, igg3 and igg7 are merged with igg1 and also igg5, igg6 is alone
    all_igg = merge_data.remove_duplicate_from_list(all_igg)

    assert len(all_igg) == 3
    # Check header light
    assert head_l1 in all_igg[0].header_l
    assert head_l2 in all_igg[0].header_l
    assert head_l3 in all_igg[0].header_l
    assert head_l4 in all_igg[0].header_l
    assert head_l7 in all_igg[0].header_l

    assert head_l2 in all_igg[1].header_l
    assert head_l3 in all_igg[1].header_l
    assert head_l4 in all_igg[1].header_l
    assert head_l5 in all_igg[1].header_l
    assert head_l7 in all_igg[1].header_l

    assert head_l6 in all_igg[2].header_l

    # Check header heavy
    assert head_h1 in all_igg[0].header_h
    assert head_h2 in all_igg[0].header_h
    assert head_h3 in all_igg[0].header_h
    assert head_h4 in all_igg[0].header_h
    assert head_h7 in all_igg[0].header_h

    assert head_h2 in all_igg[1].header_h
    assert head_h3 in all_igg[1].header_h
    assert head_h4 in all_igg[1].header_h
    assert head_h5 in all_igg[1].header_h
    assert head_h7 in all_igg[1].header_h

    assert head_h6 in all_igg[2].header_h

    # Check the sequences
    assert all_igg[0].seq_l == {seq_l1}
    assert all_igg[0].seq_h == {seq_h1}
    assert all_igg[1].seq_l == {seq_l5}
    assert all_igg[1].seq_h == {seq_h5}
    assert all_igg[2].seq_l == {seq_l6}
    assert all_igg[2].seq_h == {seq_h6}

def test_merge_and_remove_duplicate():
    """ Test function 'merge_and_remove_duplicate(data, all_igg)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is igg2
    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "EVQK"
    seq_h3 = "VTSS"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg4 is igg2 and igg1
    head_l4 = "igg4_l"
    head_h4 = "igg4_h"
    igg4 = merge_data.Igg({head_l4}, {head_h4}, seq_l2, seq_h2)
    igg4.seq_l.add(seq_l1)
    igg4.seq_h.add(seq_h1)

    # igg5 is almost igg2
    head_l5 = "igg5_l"
    head_h5 = "igg5_h"
    seq_l5 = seq_l2+"W"
    seq_h5 = seq_h2+"W"
    igg5 = merge_data.Igg({head_l5}, {head_h5}, seq_l5, seq_h5)

    # igg6 is unique
    head_l6 = "igg6_l"
    head_h6 = "igg6_h"
    seq_l6 = "DIMQS"
    seq_h6 = "KEIKL"
    igg6 = merge_data.Igg({head_l6}, {head_h6}, seq_l6, seq_h6)

    # igg7 is igg2 and something different, with two seq_l/seq_h
    # It will be merge in something,
    # because at least one l and h of its seq are in some another
    head_l7 = "igg7_l"
    head_h7 = "igg7_h"
    igg7 = merge_data.Igg({head_l7}, {head_h7}, seq_l2, seq_h2)
    igg7.seq_l.add("AZERTY")
    igg7.seq_h.add("QWERTY")

    data = merge_data.remove_duplicate_from_list([igg1, igg3, igg2, igg5, igg7])

    all_igg = []
    all_igg = merge_data.merge_and_remove_duplicate(data, all_igg)
    assert len(all_igg) == 2
    assert all_igg == data

    all_igg = [igg4, igg6]
    # igg6 is alone, all other are merged
    all_igg = merge_data.merge_and_remove_duplicate(data, all_igg)

    assert len(all_igg) == 2
    # Check header light
    assert head_l1 in all_igg[1].header_l
    assert head_l2 in all_igg[1].header_l
    assert head_l3 in all_igg[1].header_l
    assert head_l4 in all_igg[1].header_l
    assert head_l5 in all_igg[1].header_l
    assert head_l7 in all_igg[1].header_l

    assert head_l6 in all_igg[0].header_l

    # Check header heavy
    assert head_h1 in all_igg[1].header_h
    assert head_h2 in all_igg[1].header_h
    assert head_h3 in all_igg[1].header_h
    assert head_h4 in all_igg[1].header_h
    assert head_h5 in all_igg[1].header_h
    assert head_h7 in all_igg[1].header_h

    assert head_h6 in all_igg[0].header_h

    # Check the sequences
    assert all_igg[1].seq_l == {seq_l5}
    assert all_igg[1].seq_h == {seq_h5}

    assert all_igg[0].seq_l == {seq_l6}
    assert all_igg[0].seq_h == {seq_h6}

def test_generalized_lcs():
    """ Test function 'generalized_lcs(all_seq, min_size)'"""
    seq1 = "bapwetcouc"
    seq2 = "wetpwet"
    seq3 = "coucoupwet"
    all_data = [seq1, seq2, seq3]
    assert merge_data.generalized_lcs(all_data, 3) == "pwet"

    seq1 = "bapwetcouc"
    seq2 = "wetpwet"
    seq3 = "coucoupwet"
    all_data = [seq1, seq2, seq3]
    assert merge_data.generalized_lcs(all_data, 30) is None

    seq1 = "bapwetcou"
    seq2 = "wazpwet"
    seq3 = "couwazpcou"
    all_data = [seq1, seq2, seq3]
    assert merge_data.generalized_lcs(all_data, 3) is None

    seq1 = "azertyui"
    seq2 = "qsdfghj"
    seq3 = "wxcvbn,"
    all_data = [seq1, seq2, seq3]
    assert merge_data.generalized_lcs(all_data, 3) is None

def test_create_cluster():
    """ Test function 'create_cluster(igg1, all_igg)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "PWET"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is unique
    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "DIMQS"
    seq_h3 = "KEIKL"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    all_igg = [igg2, igg3]

    assert len(igg1.header_l) == 1
    assert len(igg1.header_h) == 1
    assert len(igg1.seq_l) == 1
    assert len(igg1.seq_h) == 1

    merge_data.create_cluster(igg1, all_igg)

    assert len(igg1.header_l) == 2
    assert len(igg1.header_h) == 2
    assert len(igg1.seq_l) == 2
    assert len(igg1.seq_h) == 2

    assert igg1.header_l == {head_l1, head_l2}
    assert igg1.header_h == {head_h1, head_h2}
    assert igg1.seq_l == {seq_l1, seq_l2}
    assert igg1.seq_h == {seq_h1, seq_h2}

def test_reduce_igg(org_name, min_size, max_size):
    """ Test function 'reduce_igg(igg, org_name, min_size, max_size)' """
    # Two mergeable heavy
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h1 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"
    seq_h2 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSSQVLLE"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)
    igg1.seq_h.add(seq_h2)

    assert len(igg1.seq_h) == 2

    igg1 = merge_data.reduce_igg(igg1, org_name, min_size, max_size)

    assert len(igg1.seq_h) == 1
    assert igg1.seq_h == {"EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"}

    # No possible merge
    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h2 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"
    seq_h3 = "EVQLLESGGGLVQPGGSLRLSCAASGFTFSSYAMSWVRQAPGKGLEWVSAISGSGGSTYYADSVKGRFTISRDNSKNTLYLQMNSLRAEDTAVYYCAKVRGMATIKDYFDYWGQGTLVTVSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)
    igg2.seq_h.add(seq_h3)

    assert len(igg2.seq_h) == 2

    igg2 = merge_data.reduce_igg(igg2, org_name, min_size, max_size)

    assert igg2 is None

    # Two mergeable light
    head_l3 = "igg1_l"
    head_h3 = "igg1_h"
    seq_l3 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_l4 = "WEWEQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h3 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)
    igg3.seq_l.add(seq_l4)

    assert len(igg3.seq_l) == 2

    igg3 = merge_data.reduce_igg(igg3, org_name, min_size, max_size)

    assert len(igg3.seq_l) == 1
    assert igg3.seq_l == {"QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"}

    # Two mergeable light with bad result
    head_l4 = "igg1_l"
    head_h4 = "igg1_h"
    seq_l4 = "SGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_l5 = "GTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h4 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"
    igg4 = merge_data.Igg({head_l4}, {head_h4}, seq_l4, seq_h4)
    igg4.seq_l.add(seq_l5)

    assert len(igg4.seq_l) == 2

    igg4 = merge_data.reduce_igg(igg4, org_name, min_size, max_size)

    assert igg4 is None

    # No mergeable light 
    head_l5 = "igg1_l"
    head_h5 = "igg1_h"
    seq_l5 = "SGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_l6 = "EVQLLESGGGLVQPGGSLRLSCAASGFTFSSYAMSWVRQAPGKGLEWVSAISGSGGSTYYADSVKGRFTISRDNSKNTLYLQMNSLRAEDTAVYYCAKVRGMATIKDYFDYWGQGTLVTVSS"
    seq_h5 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVSS"
    igg5 = merge_data.Igg({head_l5}, {head_h5}, seq_l5, seq_h5)
    igg5.seq_l.add(seq_l6)

    assert len(igg5.seq_l) == 2

    igg5 = merge_data.reduce_igg(igg5, org_name, min_size, max_size)

    assert igg5 is None

def test_generalized_reduce_igg(org_name, min_size, max_size):
    """ Test function 'generalized_reduce_igg(igg1, all_igg, org_name, min_size, max_size)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h1 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)
    igg1.unique = False

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h2 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVWQVLLE"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is unique
    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "DIMQS"
    seq_h3 = "KEIKL"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    all_igg = [igg2, igg3]

    assert len(igg1.header_l) == 1
    assert len(igg1.header_h) == 1
    assert len(igg1.seq_l) == 1
    assert len(igg1.seq_h) == 1

    igg1 = merge_data.generalized_reduce_igg(igg1, all_igg, org_name, min_size, max_size)

    assert len(igg1.header_l) == 2
    assert len(igg1.header_h) == 2
    assert len(igg1.seq_l) == 1
    assert len(igg1.seq_h) == 1

    assert igg1.seq_h == {"EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMV"}

def test_generalized_reduce_igg_fail(org_name, min_size, max_size):
    """ Test function 'generalized_reduce_igg(igg1, all_igg, org_name, min_size, max_size)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h1 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)
    igg1.unique = False

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h2 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVWQVLLE"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg3 is unique
    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "DIMQS"
    seq_h3 = "KEIKL"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    all_igg = [igg2, igg3]

    assert len(igg1.header_l) == 1
    assert len(igg1.header_h) == 1
    assert len(igg1.seq_l) == 1
    assert len(igg1.seq_h) == 1

    igg1 = merge_data.generalized_reduce_igg(igg1, all_igg, org_name, min_size, max_size)

    assert igg1 is None

def test_separate_uniques_iggs_lcs(org_name, min_size, max_size):
    """ Test function 'separate_uniques_iggs_lcs(all_igg, org_name, min_size, max_size)'"""
    head_l1 = "igg1_l"
    head_h1 = "igg1_h"
    seq_l1 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h1 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "igg2_l"
    head_h2 = "igg2_h"
    seq_l2 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h2 = "EVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVWQVLLE"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    head_l3 = "igg3_l"
    head_h3 = "igg3_h"
    seq_l3 = "QSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL"
    seq_h3 = "ELQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMV"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg6 is unique
    head_l4 = "igg4_l"
    head_h4 = "igg4_h"
    seq_l4 = "DIMQS"
    seq_h4 = "KEIKL"
    igg4 = merge_data.Igg({head_l4}, {head_h4}, seq_l4, seq_h4)

    head_l5 = "igg5_l"
    head_h5 = "igg5_h"
    seq_l5 = "DIQMTQSPSSLSASVGDRVTITCRTSQSLSSYTHWYQQKPGKAPKLLIYAASSRGSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQSRTF"
    seq_h5 = "QVQLQQSGPGLVKPSQTLSLTCAISGDSVSSYNAVWNWIRQSPSRGLEWLGRTYYRSGWYNDYAESVKSRITINPDTSKNQFSLQLNSVTPEDTA"
    igg5 = merge_data.Igg({head_l5}, {head_h5}, seq_l5, seq_h5)

    head_l6 = "igg6_l"
    head_h6 = "igg6_h"
    seq_l6 = "DIQMTQSPSSLSASVGDRVTITCRTSQSLSSYTHWYQQKPGKAPKLLIYAASSRGSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQSRTFGQGTKV"
    seq_h6 = "QVQLQQSGPGLVKPSQTLSLTCAISGDSVSSYNAVWNWIRQSPSRGLEWLGRTYYRSGWYNDYAESVKSRITINPDTSKNQFSLQLNSVTPEDTAVYYCA"
    igg6 = merge_data.Igg({head_l6}, {head_h6}, seq_l6, seq_h6)

    all_igg = [igg3, igg2, igg1, igg4, igg5, igg6]
    # All are tagged unique before the separation (default)
    for i in all_igg:
        assert i.unique

    unique_igg, non_unique_igg = merge_data.separate_uniques_iggs_lcs(all_igg, org_name, min_size, max_size)

    # Only igg4 is unique
    assert len(unique_igg) == 2
    assert len(non_unique_igg) == 1

    assert igg4 in unique_igg

    # all_igg must have changed
    assert not igg1.unique
    assert not igg2.unique
    assert not igg3.unique
    assert igg4.unique

def test_export_fasta(tmp_path):
    """ Test function 'main()'"""
    head_l1 = "bjr_db1"
    head_h1 = "yooo_db1"
    seq_l1 = "AEVQKQ"
    seq_h1 = "VTSSA"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    head_l2 = "pwet1_db2"
    head_h2 = "hsh1_db2"
    seq_l2 = "EVQK"
    seq_h2 = "VTSS"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg4 is igg2 and igg1
    igg4 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)
    igg4.header_l.add(head_l1)
    igg4.header_h.add(head_h1)

    all_igg = [igg1, igg2, igg4]

    # Tmp result files
    res_out_file = tmp_path.joinpath("res.fasta")

    # Print the result files
    merge_data.export_fasta(all_igg, res_out_file)

    # Check light file
    with open(res_out_file, encoding="UTF-8") as res_file:
        # igg1 light
        line = res_file.readline()
        assert line == ">bjr|||bjr_db1\n"
        line = res_file.readline()
        assert line == "AEVQKQ\n"
        # igg1 heavy
        line = res_file.readline()
        assert line == ">yooo|||yooo_db1\n"
        line = res_file.readline()
        assert line == "VTSSA\n"
        #igg2 light
        line = res_file.readline()
        assert line == ">pwet1|||pwet1_db2\n"
        line = res_file.readline()
        assert line == "EVQK\n"
        #igg2 heavy
        line = res_file.readline()
        assert line == ">hsh1|||hsh1_db2\n"
        line = res_file.readline()
        assert line == "VTSS\n"
        #igg4 light
        line = res_file.readline()
        assert line == ">bjr|||bjr_db1|||pwet1_db2\n"
        line = res_file.readline()
        assert line == "EVQK\n"
        #igg4 heavy
        line = res_file.readline()
        assert line == ">hsh1|||hsh1_db2|||yooo_db1\n"
        line = res_file.readline()
        assert line == "VTSS\n"

def test_main(capsys, org_name):
    """ Test function 'export_fasta()'"""
    group = "IGLV6"
    filename = "PDB"
    path = pathlib.Path(__file__).parent.resolve()
    light_file = f"{path}/../../CLEAN/{group}_{filename}_{org_name}_light.fasta"
    heavy_file = f"{path}/../../CLEAN/{group}_{filename}_{org_name}_heavy.fasta"

    # Light file ok
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light2\nEVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT\n")
    # Heavy file ok
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEIVLTQSPGTLSLSPGERATLSCRASQIVSRNHLAWYQQKPGQAPRLLIFGASSRATGIPVRFSGSGSGTDFTLTINGLAPEDFAVYYCLSSDSSIFTFGQGTKVDFKRTVAAPSV\n>heavy2\nQVQLVQSGAEVKKPGATVKVSCKISGHTLIKLSIHWVRQAPGKGLEWMGGYEGEVDEIFYAQKFQHRLTVIADTATDTVYMELGRLTSDDTAVYFCGTLGVTVTEAGLGIDDYWGQGTLVTVSS\n")
    # Everything is fine
    merge_data.main(org_name, group)
    captured = capsys.readouterr()

    # Test output
    assert "IgG founded in files" in captured.out
    assert "There are 2 IgG after removing duplicates (2 before)" in captured.out
    assert "2 are unique IgG and 0 are non unique IgG, in 0 clusters" in captured.out
    assert "There are 4 unique sequences" in captured.out

    # 3 IgG in one cluster test
    captured = ""
    # Light file ok
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light2\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light3\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n")
    # Heavy file ok
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT\n>heavy2\nQVQLVQSGAEVKKPGATVKVSCKISGHTLIKLSIHWVRQAPGKGLEWMGGYEGEVDEIFYAQKFQHRLTVIADTATDTVYMELGRLTSDDTAVYFCGTLGVTVTEAGLGIDDYWGQGTLVTVSS\n>heavy3\nEVQLLESGGGLVQPGGSLRLSCAASGFTFSSYGMAWVRQAPGKGLEWVSFISATGLSTYFADSVKGRFTISRDTTKNTLYLQMNSLRADDTAVYFCARMRRTMIAFGGNDFWGQGTLVTVSS\n")
    # Everything is fine
    merge_data.main(org_name, group)
    captured = capsys.readouterr()

    # Test output
    assert "IgG founded in files" in captured.out
    assert "There are 3 IgG after removing duplicates (3 before)" in captured.out
    assert "0 are unique IgG and 1 are non unique IgG, in 1 clusters" in captured.out
    assert "There are 4 unique sequences" in captured.out

    # 3 IgG in one cluster, with one duplicate test
    captured = ""
    # Light file ok
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light2\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light3\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n")
    # Heavy file ok
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT\n>heavy2\nEVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVTVWQVLLE\n>heavy3\nQQVQLVESGGGVVQPGRSLRLSCAASGFTFSTYAMHWVRQAPGKGLEWVAVISYDANYKYYADSVKGRFTISRDNSKNTLYLQMNSLR\n")
    # Everything is fine
    merge_data.main(org_name, group)
    captured = capsys.readouterr()

    # Test output
    assert "IgG founded in files" in captured.out
    assert "There are 2 IgG after removing duplicates (3 before)" in captured.out
    assert "0 are unique IgG and 1 are non unique IgG, in 1 clusters" in captured.out
    assert "There are 3 unique sequences" in captured.out

    # 2 IgG in one cluster, 1 alone test
    captured = ""
    # Light file ok
    with open(light_file, "w", encoding="utf-8") as f_name:
        f_name.write(">light1\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light2\nQSALTQPASVSGSPGQSITISCTGTSSDVGGYDYVSWYQHHPGKSPKLLISGVTHRPSGVSNRFSGSKSGDTASLTISGLQAEDEADYYCSAYTSTTTVIFGGGTKVTVL\n>light3\nDIVMTQSPDSLAVSLGERATINCKSSQSVTFNYKNYLAWYQQKPGQPPKLLIYWASTRESGVPDRFSGSGSGTDFTLTISSLQAEDVAVYYCQQHYRTPPTFGQGTKVEIKR\n")
    # Heavy file ok
    with open(heavy_file, "w", encoding="utf-8") as f_name:
        f_name.write(">heavy1\nEVQVVESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLVWVSRINGDGTTTTYADSVKGRFTISRDNAKNTLSLQMNSLRAEDTAVYYCARGLSGSHTGGPASDVWGQGTMVT\n>heavy2\nGVQLVESGGGLVQPGRSLRLSCAASGFTFSNYAMYWVRQAPGKGLEWVALISYDISTDYYADSVKGRFTISRDNSKNTIYLQMNNLRTEDTALYYCTNTYYWGQGTLVT\n>heavy3\nQQVQLVESGGGVVQPGRSLRLSCAASGFTFSTYAMHWVRQAPGKGLEWVAVISYDANYKYYADSVKGRFTISRDNSKNTLYLQMNSLR\n")
    # Everything is fine
    merge_data.main(org_name, group)
    captured = capsys.readouterr()

    # Test output
    assert "IgG founded in files" in captured.out
    assert "There are 3 IgG after removing duplicates (3 before)" in captured.out
    assert "1 are unique IgG and 1 are non unique IgG, in 1 clusters" in captured.out
    assert "There are 5 unique sequences" in captured.out

    if os.path.isfile("IGLV6_Homo_sapiens.fasta"):
        os.remove("IGLV6_Homo_sapiens.fasta")

def test_functional(capsys, org_name, min_size, max_size):

    head_l1 = "head_l1"
    head_h1 = "head_h1"
    seq_l1 = "VVMTQSPST"
    seq_h1 = "PSVFLFPPKP"
    igg1 = merge_data.Igg({head_l1}, {head_h1}, seq_l1, seq_h1)

    # igg2 is in igg1
    head_l2 = "head_l2"
    head_h2 = "head_h2"
    seq_l2 = "VMTQSPST"
    seq_h2 = "PSVFLFPP"
    igg2 = merge_data.Igg({head_l2}, {head_h2}, seq_l2, seq_h2)

    # igg1 is in igg3
    head_l3 = "head_l3"
    head_h3 = "head_h3"
    seq_l3 = "VVMTQSPSTLSASV"
    seq_h3 = "PSVFLFPPKP"
    igg3 = merge_data.Igg({head_l3}, {head_h3}, seq_l3, seq_h3)

    # igg4 is unique
    head_l4 = "head_l4"
    head_h4 = "head_h4"
    seq_l4 = "AWYQQKPGKAPKL"
    seq_h4 = "VESGGGLV"
    igg4 = merge_data.Igg({head_l4}, {head_h4}, seq_l4, seq_h4)

    # igg5, igg6 and igg7 are only one IgG
    # So, same light, but:
    # -in igg5, the light is short on right,
    # -in igg6, the light is complete,
    # -in igg7, the light is short on left,
    # Also, same heavy, but:
    # -in igg5, the heavy is complete,
    # -in igg6, the heavy is short on left,
    # -in igg7, the heavy is short on right,
    # There is no simple way to merge sequences in this software
    # so in the end, it will be one IgG with 3 sequences
    head_l5 = "head_l5"
    head_h5 = "head_h5"
    seq_l5 = "EVQLVETG"
    seq_h5 = "LELVVKGEDV"
    igg5 = merge_data.Igg({head_l5}, {head_h5}, seq_l5, seq_h5)

    head_l6 = "head_l6"
    head_h6 = "head_h6"
    seq_l6 = "EVQLVETGPG"
    seq_h6 = "LVVKGEDV"
    igg6 = merge_data.Igg({head_l6}, {head_h6}, seq_l6, seq_h6)

    head_l7 = "head_l7"
    head_h7 = "head_h7"
    seq_l7 = "VQLVETGPG"
    seq_h7 = "LELVVKGE"
    igg7 = merge_data.Igg({head_l7}, {head_h7}, seq_l7, seq_h7)


    all_igg = [igg1, igg2, igg3, igg4, igg5, igg6, igg7]

    assert len(all_igg) == 7
    # Merge headers of identical IgG
    all_igg = merge_data.remove_duplicate_from_list(all_igg)
    # igg1, igg2 and igg3 are merged, not igg5, igg6 and igg7 yet
    assert len(all_igg) == 5
    assert "head_l1" in all_igg[0].header_l
    assert "head_l2" in all_igg[0].header_l
    assert "head_l3" in all_igg[0].header_l
    assert "head_l4" in all_igg[1].header_l

    assert "head_l5" in all_igg[2].header_l
    assert "head_l6" in all_igg[3].header_l
    assert "head_l7" in all_igg[4].header_l

    assert "head_h1" in all_igg[0].header_h
    assert "head_h1" in all_igg[0].header_h
    assert "head_h3" in all_igg[0].header_h
    assert "head_h4" in all_igg[1].header_h
    assert "head_h5" in all_igg[2].header_h
    assert "head_h6" in all_igg[3].header_h
    assert "head_h7" in all_igg[4].header_h

    assert all_igg[0].seq_l == {seq_l3}
    assert all_igg[1].seq_l == {seq_l4}
    assert all_igg[2].seq_l == {seq_l5}
    assert all_igg[3].seq_l == {seq_l6}
    assert all_igg[4].seq_l == {seq_l7}

    assert all_igg[0].seq_h == {seq_h1}
    assert all_igg[1].seq_h == {seq_h4}
    assert all_igg[2].seq_h == {seq_h5}
    assert all_igg[3].seq_h == {seq_h6}
    assert all_igg[4].seq_h == {seq_h7}

    # Separate unique from non-unique IgG
    unique_igg, non_unique_igg = merge_data.separate_uniques_iggs_lcs(all_igg, org_name, min_size, max_size)
    # igg123 and igg4
    assert len(unique_igg) == 2

    # igg5, igg6 and igg7 are one single IgG, with three sequences
    assert len(non_unique_igg) == 1

    final_res = list(unique_igg.union(non_unique_igg))

    # Run everything another time, to not have classical IgG,
    # but already processed IgG. Everything should identical, except
    # the initial length of igg and its size after removing duplicate
    assert len(final_res) == 3
    # Merge headers of identical IgG
    all_igg = merge_data.remove_duplicate_from_list(final_res)
    # igg1, igg2 and igg3 are merged, not igg5, igg6 and igg7 yet
    assert len(all_igg) == 3
