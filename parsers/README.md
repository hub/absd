# Parsers for ABSD

This file explains how data are extracted and how to add a new parser.

## RAW folder
The RAW folder contains files from other databases than are parsed to extract information available in ABSD.


#### AbDb.fasta
Data from <http://www.abybank.org/abdb/> (**Download Dataset** tab) can be downloaded from the **Complete Dataset** of **Redundant Antibody List** (<http://www.abybank.org/abdb/Data/Redundant_files/Redundant_LH_Combined_Martin.txt>). New version seems to be stored in <http://www.abybank.org/abdb/snapshots/>. Inside, the file **antibodies.txt** is the easiest to parse.

From this file, retrieve all PDB IDs (4 characters before the **_**) to create a temporary file (*tmp.txt*), containing one ID per line.

After extracting all IDs, use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. For example, you can use `sort -u tmp.txt | split -l 1000` to produce *xaa*, *xab*, and *xac* files. Then, use these files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single FASTA file using a command like `cat *.fasta > AbDb.fasta`.


#### AbPDB.fasta
From <http://www.abybank.org/abpdbseq/> (file **abpdbseq_latest.faa**), retrieve all PDB IDs (4 characters before the **_**) to create a temporary file (*tmp.txt*), containing one ID per line.

After extracting all IDs, use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. For example, you can use `sort -u tmp.txt | split -l 1000` to produce *xaa*, *xab*, and *xac* files. Then, use these files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single FASTA file using a command like `cat *.fasta > AbPDB.fasta`.


#### ALPHASEQ.csv (removed)
From <https://zenodo.org/record/5095284>, file **MITLL_AAlphaBio_Ab_Binding_dataset.csv**) is directly used. However, all sequences are duplicated multiple times, resulting in no unique heavy/light pairs. **Data not used**.


#### CATNAP-HIV*.fasta
From <https://www.hiv.lanl.gov/components/sequence/HIV/neutralization/download_db.comp>, two fasta files are downloaded : Antibody heavy chain aa sequences (<https://www.hiv.lanl.gov/cgi-bin/common_code/download.cgi?/scratch/NEUTRALIZATION/heavy_seqs_aa.fasta>) and Antibody light chain aa sequences (<https://www.hiv.lanl.gov/cgi-bin/common_code/download.cgi?/scratch/NEUTRALIZATION/light_seqs_aa.fasta>).


#### CoV-AbDab.csv
From <https://opig.stats.ox.ac.uk/webapps/covabdab/>, a csv file can be accessed from **Downloads** tab, **Database (CSV)**.


#### CoV-AbDab-PDB.fasta
From <https://opig.stats.ox.ac.uk/webapps/covabdab/>, a folder with PDB sequences can be accessed from **Downloads** tab, **PDB Structures (.tar.gz)**. From this folder, retrieve PDB IDs (4 characters before the **_**) of each files, for example with: `ls -1 | cut -d "_" -f 1 | sort -u | split -l 1000`.

Don't forget to use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. Then, use the produced files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single fasta file using a command like `cat *.fasta > CoV-AbDab-PDB.fasta`.


#### EBOLA files
Data retrieved from Table S7 of Supplementary Materials for *[Isolation of potent neutralizing antibodies from a survivor of the 2014 Ebola virus outbreak](https://doi.org/10.1126/science.aad5788)*.
**EBOLA_ids_paired.tsv** contains IDs (from [GeneBank](https://www.ncbi.nlm.nih.gov/genbank/)) and links between heavy/light chains. **EBOLA.gb** contains the raw data from GenBank.


#### example.fasta
A mock file used in the example parser, see below.


#### EMBL (removed)
Data comes from <http://www.abybank.org/emblig/> and I was not able to use it. It lacks links between heavy/light sequences. **Data not used**.


#### IMGT-INN folder
From the folder obtained at <https://www.imgt.org/download/3Dstructure-DB/IMGT3DFlatFiles.tgz>, the INN (ungzipped) files are moved to **IMGT-INN** folder.


#### IMGT.fasta
From the folder obtained at <https://www.imgt.org/download/3Dstructure-DB/IMGT3DFlatFiles.tgz>, the PDB (ungzipped) files are just used to retrieve IDs (4 characters before the **_**, for example: `ls -1 *.pdb | cut -d "-" -f 2 | cut -d "." -f 1 | sort -u | split -l 1000`).

Don't forget to use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. Then, use the produced files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single fasta file using a command like `cat *.fasta > IMGT.fasta`.


#### IMGT.txt
Data are obtained from a dump of <https://www.imgt.org/3Dstructure-DB/> using:

- **Display results**: *Domains and sequence alignment*
- **IMGT complex type** (in **IDENTIFICATION**, in **Search using IMGT-ONTOLOGY concepts**): *IG/Ag*

Copy/paste the whole file result page in a txt file.
> **_Warning:_** tabulations must be tabulations, not 4 spaces


#### IMGT2.txt
Data are obtained from a dump of <https://www.imgt.org/3Dstructure-DB/> using:

- **Display results**: *Domains and sequence alignment*
- **IMGT receptor type** (in **IDENTIFICATION**, in **Search using IMGT-ONTOLOGY concepts**): *IG*

Copy/paste the whole file result page in a txt file.
> **_Warning:_** tabulations must be tabulations, not 4 spaces


#### OAS folder
Files were obtained at <https://opig.stats.ox.ac.uk/webapps/oas/oas_paired/>, when clicking "Search" without using any attributes, then clicking on **here** link. A custom script rename all file before the download to get origin of the files in the name.


#### PairedNGS
The folder was obtained from <https://www.naturalantibody.com/paired-ngs/>.


#### PLAbDab.csv
The file was obtained from <https://opig.stats.ox.ac.uk/webapps/plabdab/static/downloads/paired_sequences.csv.gz>.


#### PDB.fasta
All IDs were taken from the PDB advanced search page (<https://www.rcsb.org/search/advanced>) by searching **Macromolecule Name**, **has any of words**, *light heavy*

After extracting all IDs, use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. For example, you can use `sort -u tmp.txt | split -l 1000` to produce *xaa*, *xab*, and *xac* files. Then, use these files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single FASTA file using a command like `cat *.fasta > PDB.fasta`.


#### kabat2000.faa
The file was obtained from <http://www.abybank.org/kabat/>.
> **_Warning:_** All spaces must be replaced by _

#### SAbDab.fasta
From <https://opig.stats.ox.ac.uk/webapps/sabdab-sabpred/sabdab/search/?all=true#downloads>, [Download the summary file](https://opig.stats.ox.ac.uk/webapps/sabdab-sabpred/sabdab/summary/all/) of all sequences. From this file, extract all PDB IDs, for example using: `tail -n +2 sabdab_summary_all.tsv | cut -f 1 -d $'\t' | sort -u | split -l 1000`.

Don't forget to use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. Then, use the produced files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single fasta file using a command like `cat *.fasta > SAbDab.fasta`.


#### SACS.fasta
From http://www.abybank.org/sacs/ (**Download Chain List**), get the **antibodies.txt** file. From this file, retrieve PDB IDs (4 characters before the **_**).

After extracting all IDs, use `sort -u` followed by the `split` command, since the PDB accepts a maximum of 1,000 IDs per request. For example, you can use `sort -u tmp.txt | split -l 1000` to produce *xaa*, *xab*, and *xac* files. Then, use these files to query the PDB (<https://www.rcsb.org/downloads/fasta>) and finally merge all result files into a single FASTA file using a command like `cat *.fasta > SACS.fasta`.


#### TheraSAbDab_SeqStruc_OnlineDownload.csv
From Thera-SAbDab page https://opig.stats.ox.ac.uk/webapps/sabdab-sabpred/therasabdab/search/?all=true, download the csv file (https://opig.stats.ox.ac.uk/webapps/sabdab-sabpred/static/downloads/TheraSAbDab_SeqStruc_OnlineDownload.csv).


#### UNIPROT.fasta
A fasta file can be accessed from https://www.uniprot.org/uniprotkb?query=Immunoglobulin%2C%20Antibody%2C%20IG. It is not easy to get good data from it, here we got around 2000 IgG. It could be improved.



## scripts folder

Each database has its own parser in **scripts/format** folder and generate two files: one for heavy sequences and one for the corresponding light sequences (in the same order in both files).

Then, `merge_data.py` script merges all data from all files for each specie.

Other scripts and folders are to automatize the download and extraction of data.


#### Creating a new parser

The `scripts/format/example.py` script is an example of what a parser should do for ABSD.

It parses the file `RAW/example.fasta` that contains 7 sequences. The first sequence (*Ex1_1*) is not an IgG sequence. Then, there are two IgGs (*Ex1_2 - Ex1_3* and *Ex2_1 - Ex2_2*). Finally the last sequences, (*Ex3_1* and *Ex3_2*) are not IgG sequences. All headers are formatted in the same way: `ID|Useless description|Important description|specie`.

`example.py` has two main objectives. The first objective is to extract IgG sequences, along with their headers, from the FASTA file and the second is to make the pairing of these sequences, two-by-two. The `get_data()` function creates a dictionary of all relevant sequences, enabling to then call `core.get_igg()` that will make the pairing.

`get_data()` is the function that need to be implemented when creating a new parser. It requires four parameters:

- `input_file` (`str`), the path of the file to parse
- `all_chains` (`list(str)`), keywords associated with to LIGHT or HEAVY sequences in headers
- `organisms` (`list(str)`): keywords that should appear (at least one) in correct headers
- `to_remove` (`list(str)`, optional): keywords that should NOT appear in correct headers

It must return all potentially good sequences not paired in this specific format:

`defaultdict(id_: list(tuple('id_ description', Sequence object)))`.

In this dictionary, the `keys` are simple IDs; e.g., for PDB IDs *2DD8_1*, *2DD8_2* and *2DD8_3*, the ID is *2DD8*. Sequences that should be paired together **must** have the same key, the pairing is attempted only between sequences sharing the same key.

The associated values are lists of 'sequences', where each sequence is a tuple of two elements:

- The first element consists of the previously described **key**, followed by **a space**, and then **the text that will be used for pairing**. Ideally, this first element of the tuple is strictly identical between a heavy and a light sequence to pair, except for 'light' and 'heavy' keywords.
- The second element is a `core.Sequence()` object which takes a header and an amino-acid sequence as arguments. The header **must** end with an semicolon (**;**) followed by the ID used in the original database's URL, to create the link to this database in ABSD.

For example, for the following PDB sequence, accessible with the PDB ID *3TT3* at the url <https://www.rcsb.org/structure/3TT3>:

\>3TT3_3|Chain C|mouse light chain|Mus musculus<br>
QVQLQQSGTELMRPGASVKISCKAFGYTFTNHHINWMKQ...

- The key is: `3TT3`
- The first element of the tuple is: `3TT3 mouse light chain`
- The object is: `core.Sequence('>3TT3_3|Chain C|mouse light chain|Mus musculus;3TT3', 'QVQLQQS...')`

> **_Warning:_** While in this example, the key is also the ID in the PDB and used in the url, this is not always true. The key can be anything, but it **must** be shared between sequences to be paired together, and **must** be at the start of the tuple, while the end of the header in the `core.Sequence()` should be the ID in the original database, if given.

Once `get_data()` is functional, the `main` function can be easily adapted by configuring the relevant paths and keywords. When automatic pairing fails, the user is asked to manually input the pairs. The user's choices are then saved in **manual** folder, allowing them to avoid redoing the same choices during future updates. Two files of paired IgGs are generated (by default in **CLEAN** folder) containing all heavy (resp. light) sequences, in the same order.


#### merge_data.py

This script merges the desired files generated by parsers into a single file of non-redundant IgG. The `init()` function is used to specify the files and their corresponding species.

All unique IgGs of a specie are outputted in a single file within the **CLEAN** folder, where the sequences are ordered as light1-heavy1-light2-heavy2, and so on.

Optionally, none-unique IgGs can be outputted into separate cluster files, with one file per cluster (typically containing 2-3 IgGs).


## CLEAN folder

This folder contains files generated by each parser and by `merge_data.py` script.


## manual folder

This folder stores previous manual pairing information provided by the user for each parser.
