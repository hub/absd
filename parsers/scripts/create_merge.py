# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Launch in parallel the merging of data, per group, per species. """
import os
import sys
import glob

# Find all groups ans all species
all_groups = set()
all_species = set()
for filename in glob.glob("../CLEAN/IG*.fasta"):
    all_groups.add(filename.split("_")[0].split("/")[-1])
    all_species.add("_".join(filename.split("_")[-3:-1]))
# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")
# Create a config file for the job_array
nb_job = 0
with open(f"run/config_merge.txt", "w", encoding="utf-8") as path_f:
    for group in all_groups:
        for species in all_species:
            path_f.write(f"pypy3 -u ../merge_data.py {species} {group}\n")
            nb_job += 1

# Write the cluster script
with open(f"run/merge_sample.sh", "w", encoding="utf-8") as script_f:
    script_f.write("#!/bin/sh\n")
    script_f.write("#SBATCH -p hubbioit\n")
    script_f.write("#SBATCH --mem 10G\n")
    script_f.write(f"#SBATCH --job-name=merge_data\n")
    script_f.write("#SBATCH --cpus-per-task=1\n")
    script_f.write("#SBATCH --mail-type=array_tasks,fail\n")
    script_f.write("#SBATCH --mail-user=nmaillet\n")
    script_f.write(f"#SBATCH --array=1-{nb_job}\n\n")

    script_f.write("source /opt/gensoft/adm/etc/profile.d/modules.sh\n")
    script_f.write("module purge\n")
    script_f.write("module load pypy/3.10-v7.3.16\n\n")

    # The list of files to run
    script_f.write("LINE=$(sed -n ${SLURM_ARRAY_TASK_ID}p config_merge.txt)\n")
    script_f.write("eval $LINE\n")