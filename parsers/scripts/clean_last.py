import gzip
import sys
import os
from itertools import permutations, combinations
from collections import Counter
import time

def next_sequence(file):
    """ Return each sequence of a file as a tuple (header, seq) using a
    generator. The file can be in fasta or fastq format, gzipped or not.

    :param file: fasta/fastq file to read, gzipped or not
    :type file: str

    :return: the current peptide in the file
    :rtype: tuple(str, str)
    """

    # If the file is empty, return None
    # @WARNING does not work for empty gz file
    if not os.path.isfile(file) or os.path.getsize(file) <= 0:
        return None
    # Is it a GZIP file?
    test_file = open(file, "rb")
    # Get the first values
    magic = test_file.read(2)
    # Close the file
    test_file.close()

    # Open the file, GZIP or not
    with (gzip.open(file, "rb") if magic == b"\x1f\x8b"
          else open(file, "rb")) as in_file:
        first_line = in_file.readline().decode('utf-8')
        # FASTQ file
        if first_line.startswith("@"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fastq header
                if header.startswith("@"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Skip the two next lines
                    in_file.readline()
                    in_file.readline()
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # (multi?)FASTA file
        elif first_line.startswith(">"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fasta header
                if header.startswith(">"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Get current offset
                    current_offset = in_file.tell()
                    # Get next line
                    next_l = in_file.readline().decode('utf-8').strip()
                    # While this next line is not a fasta header...
                    while next_l and not next_l.startswith(">"):
                        # Add this to the Sequence
                        sequence += next_l
                        # Get current offset
                        current_offset = in_file.tell()
                        # Get next line
                        next_l = in_file.readline().decode('utf-8').strip()
                    # Next line is a fasta header, go back to its beginning
                    in_file.seek(current_offset)
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # Not a valid file
        else:
            # Stop the generator with the error to show
            print(f"File error: enable to understand type of file {file} "\
                  f"({first_line[0]})", file=sys.stderr)
            sys.exit(1)



file_h = "../../data/Homo_sapiens.fasta"
file_h_out = "../../data/Homo_sapiens_clean.fasta"

all_seq_l = []
all_seq_h = []
all_err_l = set()
all_err_h = set()
light = True

for seq in next_sequence(file_h):
    if light:
        all_seq_l.append(seq[1])
    else:
        all_seq_h.append(seq[1])
    light = not light

size = len(all_seq_l)
for pos, seq1 in enumerate(all_seq_l):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_l[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_l.add(seq1)
            all_err_l.add(seq2)
            break

size = len(all_seq_h)
for pos, seq1 in enumerate(all_seq_h):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_h[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_h.add(seq1)
            all_err_h.add(seq2)
            break

print(len(all_err_l))
print(len(all_err_h))

light = True
light_seq = None
heavy_seq = None
with open(file_h_out, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_h):
        if light:
            if not seq[1] in all_err_l:
                light_seq = seq
        else:
            if not seq[1] in all_err_h:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light



file_m = "../../data/Mus_musculus.fasta"
file_m_out = "../../data/Mus_musculus_clean.fasta"

all_seq_l = []
all_seq_h = []
all_err_l = set()
all_err_h = set()
light = True

for seq in next_sequence(file_m):
    if light:
        all_seq_l.append(seq[1])
    else:
        all_seq_h.append(seq[1])
    light = not light

size = len(all_seq_l)
for pos, seq1 in enumerate(all_seq_l):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_l[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_l.add(seq1)
            all_err_l.add(seq2)
            break

size = len(all_seq_h)
for pos, seq1 in enumerate(all_seq_h):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_h[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_h.add(seq1)
            all_err_h.add(seq2)
            break

print(len(all_err_l))
print(len(all_err_h))

light = True
light_seq = None
heavy_seq = None
with open(file_m_out, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_m):
        if light:
            if not seq[1] in all_err_l:
                light_seq = seq
        else:
            if not seq[1] in all_err_h:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light



file_ma = "../../data/Macaca_mulatta.fasta"
file_ma_out = "../../data/Macaca_mulatta_clean.fasta"

all_seq_l = []
all_seq_h = []
all_err_l = set()
all_err_h = set()
light = True

for seq in next_sequence(file_ma):
    if light:
        all_seq_l.append(seq[1])
    else:
        all_seq_h.append(seq[1])
    light = not light

size = len(all_seq_l)
for pos, seq1 in enumerate(all_seq_l):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_l[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_l.add(seq1)
            all_err_l.add(seq2)
            break

size = len(all_seq_h)
for pos, seq1 in enumerate(all_seq_h):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_h[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_h.add(seq1)
            all_err_h.add(seq2)
            break

print(len(all_err_l))
print(len(all_err_h))

light = True
light_seq = None
heavy_seq = None
with open(file_ma_out, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_ma):
        if light:
            if not seq[1] in all_err_l:
                light_seq = seq
        else:
            if not seq[1] in all_err_h:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light



file_o = "../../data/Oryctolagus_cuniculus.fasta"
file_o_out = "../../data/Oryctolagus_cuniculus_clean.fasta"

all_seq_l = []
all_seq_h = []
all_err_l = set()
all_err_h = set()
light = True

for seq in next_sequence(file_o):
    if light:
        all_seq_l.append(seq[1])
    else:
        all_seq_h.append(seq[1])
    light = not light

size = len(all_seq_l)
for pos, seq1 in enumerate(all_seq_l):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_l[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_l.add(seq1)
            all_err_l.add(seq2)
            break

size = len(all_seq_h)
for pos, seq1 in enumerate(all_seq_h):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_h[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_h.add(seq1)
            all_err_h.add(seq2)
            break

print(len(all_err_l))
print(len(all_err_h))

light = True
light_seq = None
heavy_seq = None
with open(file_o_out, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_o):
        if light:
            if not seq[1] in all_err_l:
                light_seq = seq
        else:
            if not seq[1] in all_err_h:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light



file_r = "../../data/Rattus_norvegicus.fasta"
file_r_out = "../../data/Rattus_norvegicus_clean.fasta"

all_seq_l = []
all_seq_h = []
all_err_l = set()
all_err_h = set()
light = True

for seq in next_sequence(file_r):
    if light:
        all_seq_l.append(seq[1])
    else:
        all_seq_h.append(seq[1])
    light = not light

size = len(all_seq_l)
for pos, seq1 in enumerate(all_seq_l):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_l[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_l.add(seq1)
            all_err_l.add(seq2)
            break

size = len(all_seq_h)
for pos, seq1 in enumerate(all_seq_h):
    if pos%1000 == 0:
        print(f"{pos}/{size}")
    for seq2 in all_seq_h[pos+1:]:
        if seq1 in seq2 or seq2 in seq1:
            all_err_h.add(seq1)
            all_err_h.add(seq2)
            break

print(len(all_err_l))
print(len(all_err_h))

light = True
light_seq = None
heavy_seq = None
with open(file_r_out, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_r):
        if light:
            if not seq[1] in all_err_l:
                light_seq = seq
        else:
            if not seq[1] in all_err_h:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light





# Remove redundant between different IG*V
all_seq_h = []
all_seq_m = []
all_seq_ma = []
all_seq_o = []
all_seq_r = []
for seq in next_sequence(file_h_out):
    all_seq_h.append(seq)
for seq in next_sequence(file_m_out):
    all_seq_m.append(seq)
for seq in next_sequence(file_ma_out):
    all_seq_ma.append(seq)
for seq in next_sequence(file_o_out):
    all_seq_o.append(seq)
for seq in next_sequence(file_r_out):
    all_seq_r.append(seq)

all_tmp = set()
to_rm = set()
cpt = 0
# Human
pwet = 0
size = len(all_seq_h)
for seq_h in all_seq_h:
    pwet += 1
    if pwet%1000 == 0:
        print(f"{pwet}/{size}")
    for seq in all_seq_m:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_ma:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_o:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_r:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1

# Mouse
pwet = 0
size = len(all_seq_m)
for seq_h in all_seq_m:
    pwet += 1
    if pwet%1000 == 0:
        print(f"{pwet}/{size}")
    for seq in all_seq_ma:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_o:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_r:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1

# Monkey
pwet = 0
size = len(all_seq_ma)
for seq_h in all_seq_ma:
    pwet += 1
    if pwet%1000 == 0:
        print(f"{pwet}/{size}")
    for seq in all_seq_o:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
    for seq in all_seq_r:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1

# Rabbit
pwet = 0
size = len(all_seq_o)
for seq_h in all_seq_o:
    pwet += 1
    if pwet%1000 == 0:
        print(f"{pwet}/{size}")
    for seq in all_seq_r:
        if seq_h[1] in seq[1] or seq[1] in seq_h[1]:
            to_rm.add(seq_h[1])
            to_rm.add(seq[1])
            cpt += 1
print(f"{cpt} sequences redundant")

# Last writing
# Human
light = True
light_seq = None
heavy_seq = None
with open(file_h, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_h_out):
        if light:
            if not seq[1] in to_rm:
                light_seq = seq
        else:
            if not seq[1] in to_rm:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light

# Mouse
light = True
light_seq = None
heavy_seq = None
with open(file_m, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_m_out):
        if light:
            if not seq[1] in to_rm:
                light_seq = seq
        else:
            if not seq[1] in to_rm:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light

# Monkey
light = True
light_seq = None
heavy_seq = None
with open(file_ma, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_ma_out):
        if light:
            if not seq[1] in to_rm:
                light_seq = seq
        else:
            if not seq[1] in to_rm:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light

# Rabbit
light = True
light_seq = None
heavy_seq = None
with open(file_o, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_o_out):
        if light:
            if not seq[1] in to_rm:
                light_seq = seq
        else:
            if not seq[1] in to_rm:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light

# Rat
light = True
light_seq = None
heavy_seq = None
with open(file_r, "w", encoding="utf-8") as out_f:
    for seq in next_sequence(file_r_out):
        if light:
            if not seq[1] in to_rm:
                light_seq = seq
        else:
            if not seq[1] in to_rm:
                heavy_seq = seq
            if light_seq and heavy_seq:
                out_f.write(f"{light_seq[0]}\n{light_seq[1]}\n{heavy_seq[0]}\n{heavy_seq[1]}\n")
            light_seq = None
            heavy_seq = None

        light = not light

os.remove(file_h_out)
os.remove(file_m_out)
os.remove(file_ma_out)
os.remove(file_o_out)
os.remove(file_r_out)

