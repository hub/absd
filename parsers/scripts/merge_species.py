# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Merge IgG coming from different fasta files. """
import copy
import gzip
import sys
import os
from difflib import SequenceMatcher
from itertools import combinations
from format import core

class Igg:
    """ Definition of an IgG. A sequence can have multiple headers,
        corresponding to each identical sequences (or seq in seq).
        An IgG can have multiple sequences, when for example a light is
        identical for two different heavy. This light will then be twice
        in this IgG.

    :param header_l: headers for light sequences
    :param header_h: headers for heavy sequences
    :param seq_l: light sequences
    :param seq_h: heavy sequences
    :type header_l: list(set(string))
    :type header_h: list(set(string))
    :type seq_l: list(string)
    :type seq_h: list(string)
    """

    def __init__(self, header_l, header_h, seq_l, seq_h):
        # header_l must be a set
        self.header_l = [header_l]
        # header_h must be a set
        self.header_h = [header_h]
        # seq_l must be a string
        self.seq_l = [seq_l]
        # seq_h must be a string
        self.seq_h = [seq_h]
        # Is this sequence unique in the datasets?
        self.unique = True

    # self representation for print
    def __repr__(self):
        return f"{self.header_l}\n{self.seq_l}\n{self.header_h}\n{self.seq_h}\n"

    # Equality between two IgG, only sequences matters
    def __eq__(self, other):
        if isinstance(self, other.__class__):
            if self.seq_l == other.seq_l and self.seq_h == other.seq_h:
                return True
        return False
    # Again, only sequences matters
    def __hash__(self):
        return hash(self.__repr__())

    def is_in(self, other):
        """ Are all sequences of self in other? """
        # Test each l_seq of self
        for i in self.seq_l:
            l_is_in = False
            # Compare it to each seq_l of other
            for j in other.seq_l:
                # Using in for sub-sequences
                if i in j:
                    # We found this one!
                    l_is_in = True
                    break
            # At least one l_seq was not found
            if not l_is_in:
                return False

        # Test each h_seq of self
        for i in self.seq_h:
            h_is_in = False
            # Compare it to each seq_h of other
            for j in other.seq_h:
                # Using in for sub-sequences
                if i in j:
                    # We found this one!
                    h_is_in = True
                    break
            # At least one h_seq was not found
            if not h_is_in:
                return False
        # Still there? self is in other
        return True

    def is_partially_in(self, other):
        """ Is at least one sequence of self in other? """
        # Test each l_seq of self
        for i in self.seq_l:
            # Compare it to each seq_l of other
            for j in other.seq_l:
                # Using in for sub-sequences
                if i in j:
                    # We found this one!
                    return True

        # Test each h seq of self
        for i in self.seq_h:
            # Compare it to each seq_h of other
            for j in other.seq_h:
                # Using in for sub-sequences
                if i in j:
                    # We found this one!
                    return True

        # Still there? self is not at all on other
        return False

    def merge_headers(self, other):
        """ Put headers form other into self """
        # For each light of other
        for pos_l_o, seq_o in enumerate(other.seq_l):
            added = False
            # Where should we merge headers of this light?
            for pos_l_s, seq_s in enumerate(self.seq_l):
                # We found the correct sequence
                if seq_o in seq_s:
                    # Merge headers
                    self.header_l[pos_l_s].update(other.header_l[pos_l_o])
                    added = True
                    break
            if not added:
                print(f"Error on merging headers for {other.header_l[pos_l_o]}")

        # For each heavy of other
        for pos_h_o, seq_o in enumerate(other.seq_h):
            added = False
            # Where should we merge headers of this heavy?
            for pos_h_s, seq_s in enumerate(self.seq_h):
                # We found the correct sequence
                if seq_o in seq_s:
                    # Merge headers
                    self.header_h[pos_h_s].update(other.header_h[pos_h_o])
                    added = True
                    break
            if not added:
                print(f"Error on merging headers for {other.header_h[pos_h_o]}")

    def merge(self, other):
        """ Put everything from other into self """
        # Put headers of other into self, while conserving order
        self.header_l += other.header_l
        self.header_h += other.header_h
        # Put sequences of other into self, while conserving order
        self.seq_l += other.seq_l
        self.seq_h += other.seq_h

def get_data(input_file_l, input_file_h, db_name):
    """ Get data from two fasta files, on containing light chains and
        the other containing heavy chains. File must have the same size.

    """
    all_igg = []
    # Same number of lines in both files?
    nb_l_l = 0
    nb_l_h = 0
    with open(input_file_l, encoding="UTF-8") as tmp_f:
        for nb_l_l, _ in enumerate(tmp_f, 1):
            pass
    with open(input_file_h, encoding="UTF-8") as tmp_f:
        for nb_l_h, _ in enumerate(tmp_f, 1):
            pass
    # Different number of lines
    if nb_l_l != nb_l_h:
        print("Error: not same number of lines in"\
              f" {input_file_l} ({nb_l_l}) and {input_file_h} ({nb_l_h})")
        sys.exit(1)
    # Empty files
    elif nb_l_l == 0:
        print(f"Error: files {input_file_l} and {input_file_h} are empty")
        sys.exit(1)
    # All good!
    print(f"{int(nb_l_l/2)} IgG founded in files {input_file_l} and {input_file_h}")

    # Open both files
    with open(input_file_l, encoding="UTF-8") as l_file,\
         open(input_file_h, encoding="UTF-8") as h_file:
        # Iterate line by line in both files at the same time
        for l_line, h_line in zip(l_file, h_file):
            # We have two headers
            if l_line.startswith(">") and h_line.startswith(">"):
                # Remove the ">" and add the db_name if necessary
                header_l = f"{l_line[1:].strip()}_{db_name.strip()}"
                header_h = f"{h_line[1:].strip()}_{db_name.strip()}"
            # We have two sequences
            elif not l_line.startswith(">") and not h_line.startswith(">"):
                # Create an IgG, headers must be sets
                all_igg.append(Igg({header_l},
                                   {header_h},
                                   l_line.strip(),
                                   h_line.strip()))
    return all_igg


def next_sequence(file):
    """ Return each sequence of a file as a tuple (header, seq) using a
    generator. The file can be in fasta or fastq format, gzipped or not.

    :param file: fasta/fastq file to read, gzipped or not
    :type file: str

    :return: the current peptide in the file
    :rtype: tuple(str, str)
    """

    # If the file is empty, return None
    # @WARNING does not work for empty gz file
    if not os.path.isfile(file) or os.path.getsize(file) <= 0:
        return None
    # Is it a GZIP file?
    test_file = open(file, "rb")
    # Get the first values
    magic = test_file.read(2)
    # Close the file
    test_file.close()

    # Open the file, GZIP or not
    with (gzip.open(file, "rb") if magic == b"\x1f\x8b"
          else open(file, "rb")) as in_file:
        first_line = in_file.readline().decode('utf-8')
        # FASTQ file
        if first_line.startswith("@"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fastq header
                if header.startswith("@"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Skip the two next lines
                    in_file.readline()
                    in_file.readline()
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # (multi?)FASTA file
        elif first_line.startswith(">"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fasta header
                if header.startswith(">"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Get current offset
                    current_offset = in_file.tell()
                    # Get next line
                    next_l = in_file.readline().decode('utf-8').strip()
                    # While this next line is not a fasta header...
                    while next_l and not next_l.startswith(">"):
                        # Add this to the Sequence
                        sequence += next_l
                        # Get current offset
                        current_offset = in_file.tell()
                        # Get next line
                        next_l = in_file.readline().decode('utf-8').strip()
                    # Next line is a fasta header, go back to its beginning
                    in_file.seek(current_offset)
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # Not a valid file
        else:
            # Stop the generator with the error to show
            print(f"File error: enable to understand type of file {file} "\
                  f"({first_line[0]})", file=sys.stderr)
            sys.exit(1)



def get_data2(input_file_1, input_file_2):
    """ 

    """
    all_igg_h = []
    # Open first file
    full = False
    for seq in next_sequence(input_file_1):
        if full:
            header_h = seq[0]
            h_line = seq[1]
            all_igg_h.append(Igg({header_l[1:]}, {header_h[1:]}, l_line.strip(), h_line.strip()))
            full = False
        else:
            header_l = seq[0]
            l_line = seq[1]
            full = True

    all_igg_m = []
    full = False
    for seq in next_sequence(input_file_2):
        if full:
            header_h = seq[0]
            h_line = seq[1]
            all_igg_m.append(Igg({header_l[1:]}, {header_h[1:]}, l_line.strip(), h_line.strip()))
            full = False
        else:
            header_l = seq[0]
            l_line = seq[1]
            full = True
                
    return all_igg_h, all_igg_m

def remove_duplicate(all_igg_h, all_igg_m):
    """ Removes duplicate in IgG. When light and heavy sequences of an
        IgG are inside or equal to another IgG, merge headers and keep
        only one copy. An IgG can be merge to several other
    """
    # Already processed sequences, they should not be used again
    processed = set()
    # Final result
    res = []
    # Igg to remove from all_igg_m
    to_rm = set()
    # For each IgG
    for igg1 in all_igg_h:
        # This one must be added
        to_add = True
        # Remove from all_igg_m
        for tmp in to_rm:
            all_igg_m.remove(tmp)
        to_rm = set()
        # Get all IgG with same sequences
        for igg2 in all_igg_m:
            # We have different IgG
            if igg1 is not igg2:
                # igg2 in igg1?
                # Any IgG 'in' igg2 will also be added in igg1 here
                if igg2.is_in(igg1) or igg1.is_in(igg2):
                    print(igg1)
                    print(igg2)
                    print()
                    print()
                    # Put headers of igg2 in igg1
                    igg1.merge_headers(igg2)
                    # Unique sequences with (potentially) many headers
                    res.append(igg1)
                    # Remove this one from all_igg_m
                    to_rm.add(igg2)
    return res, all_igg_m

def separate_unique_igg(all_igg):
    """ Identify IgG with unique sequences in all data.
        It modify all_igg objects """
    for pos, igg1 in enumerate(all_igg):
        # This IgG has several seq_l/seq_h
        if len(igg1.seq_l) > 1 or len(igg1.seq_h) > 1:
            igg1.unique = False
        # Tag all IgG with redundant sequences as not unique
        for igg2 in all_igg[pos+1:]:
            if igg1.is_partially_in(igg2) or igg2.is_partially_in(igg1):
                igg1.unique = False
                igg2.unique = False
    # Separate the IgG
    unique_igg = []
    non_unique_igg = []
    for i in all_igg:
        if i.unique:
            unique_igg.append(i)
        else:
            non_unique_igg.append(i)
    # Now we can have duplicate on non-unique ICI
    return unique_igg, non_unique_igg

def cluster_linked_igg(all_igg):
    """ Merge IgGs when at least one sequence is identical (or in)
        between two IgG """
    # Prevent the modification of argument
    all_igg = copy.deepcopy(all_igg)
    processed = set()
    for igg1 in all_igg:
        to_add = True
        # A similar sequence has already been processed, skip this one
        for pro in processed:
            if igg1 == pro:
                to_add = False
                break
        # A IgG not processed
        if to_add:
            changed = True
            # Something change on this IgG
            while changed:
                changed = False
                # Get all IgG with same sequences
                for igg2 in all_igg:
                    # We have different IgG
                    if igg1 is not igg2:
                        to_use = True
                        # Was igg2 already processed?
                        for pro in processed:
                            if igg2 == pro:
                                to_use = False
                                break
                        # Use this unprocessed igg2
                        if to_use:
                            # igg2 in igg1?
                            if igg2.is_partially_in(igg1):
                                # Put everything of igg2 into igg1
                                igg1.merge(igg2)
                                # Tag igg2 as processed
                                processed.add(igg2)
                                # Something changed, we will loop again
                                changed = True
                            # igg1 in igg2? Not sure why but it get for example
                            # 6o8d from IMGT with this. Otherwise this id is
                            # nowhere...
                            #if igg1.is_partially_in(igg2):
                            #    # Put everything of igg2 into igg1
                            #    igg1.merge(igg2)
                            #    # Tag igg2 as processed
                            #    processed.add(igg2)
                            #    # Something changed, we will loop again
                            #    changed = True

    # Construct the result, take only IgG not processed
    res = []
    for igg1 in all_igg:
        to_add = True
        for pro in processed:
            if igg1 == pro:
                to_add = False
                break
        if to_add:
            # Unique sequences with (potentially) many headers/seq
            res.append(igg1)
    return res

def generalized_lcs(all_seq, min_size):
    """ Find the 'longest common substring' of all sequences """
    common = set()
    # Pairwise LCS
    for seq_a, seq_b in combinations(all_seq, 2):
        # Longest common substring between two sequences
        tmp = SequenceMatcher(None, seq_a, seq_b).find_longest_match()
        # Add the corresponding substring
        common.add(seq_a[tmp[0]:tmp[0]+tmp[2]])
    # It is not good yet
    good = False
    # We have results?
    if common:
        # Take the smallest substring
        smallest = min(common, key=len)
        # The sequence is long enough
        if len(smallest) >= min_size:
            # We take it
            good = True
            # For each other (longer) substring
            for i in common:
                # The smallest is not inside!
                if smallest not in i:
                    # We don't have a good substring
                    good = False
                    break
    # We have a good substring
    if good:
        # Return it
        return smallest
    # Nothing good
    return None

def truncate_from_clusters(clusters, org_name, min_size, max_size):
    """ Try to get back some IgGs from cluster by removing some aa """
    # Get correct sizes, starts and ends
    light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before = core.STARTS_ENDS[org_name]
    new_good = []
    to_remove = []
    for igg in clusters:
        # All lights shared a correct lcs?
        # Find the smallest lcs for lights
        lcs_l = generalized_lcs(igg.seq_l, min_size)
        # We have a lcs for light
        if lcs_l:
            # Find the smallest lcs for heavy
            lcs_h = generalized_lcs(igg.seq_h, min_size)
            # We have a lcs for heavy
            if lcs_h:
                # Can it form a correct IgG?
                correct, valid, _ = core.make_smaller_igg(core.Sequence("", lcs_l),
                                                          core.Sequence("", lcs_h),
                                                          max_size,
                                                          light_starts,
                                                          light_ends,
                                                          light_ends_before,
                                                          heavy_starts,
                                                          heavy_ends,
                                                          heavy_ends_before)
                # It is (now?) a good IgG?
                if valid and len(correct[0].seq) >= min_size and len(correct[1].seq) >= min_size:
                    # Add this new good IgG with corrected sequences
                    new_good.append(Igg(set().union(*igg.header_l),
                                        set().union(*igg.header_h),
                                        correct[0].seq,
                                        correct[1].seq))
                    # Remove it from clusters
                    to_remove.append(igg)
    # Remove all goods from cluster
    for igg in to_remove:
        clusters.remove(igg)
    # Return the good one
    return new_good

def export_fasta(all_igg, output_file, sep="|||"):
    """ Write two fasta files, one for light, one for heavy.
        Different headers for a single sequence are separated by |||
        by default. First header is the short id (identical between
        light/heavy)"""
    # Open both files
    with open(output_file, "w", encoding="UTF-8") as o_file:
        for igg in all_igg:
            # All list have the same size
            for pos, _ in enumerate(igg.header_l):
                # LIGHT
                # Write header
                res_header = ""
                for a_header in igg.header_l[pos]:
                    res_header += a_header + sep
                short_id = res_header.split("_")[0]
                o_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                o_file.write(f"{igg.seq_l[pos]}\n")
                # HEAVY
                # Write header
                res_header = ""
                for a_header in igg.header_h[pos]:
                    res_header += a_header + sep
                short_id = res_header.split("_")[0]
                o_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                o_file.write(f"{igg.seq_h[pos]}\n")

def export_cluster_fasta(all_igg, output_file_l, output_file_h, sep="|||"):
    """ Write two fasta files, one for light, one for heavy for each cluster
        Different headers for a single sequence are separated by |||
        by default """
    cpt = 0
    for igg in all_igg:
        with open(str(cpt)+"_"+output_file_l, "w", encoding="UTF-8") as l_file,\
             open(str(cpt)+"_"+output_file_h, "w", encoding="UTF-8") as h_file:
            # All list have the same size
            for pos, _ in enumerate(igg.header_l):
                # LIGHT
                # Write header
                res_header = ""
                for a_header in igg.header_l[pos]:
                    res_header += a_header + sep
                short_id = res_header.split("_")[0]
                l_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                l_file.write(f"{igg.seq_l[pos]}\n")
                # HEAVY
                # Write header
                res_header = ""
                for a_header in igg.header_h[pos]:
                    res_header += a_header + sep
                #short_id = res_header.split("_")[0]  # same as light
                h_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                h_file.write(f"{igg.seq_h[pos]}\n")
        cpt += 1

def main(all_files, org_name, min_size, max_size):
    """ Prepare the data to get a list of IgG.
        An IgG with only one light/heavy is unique in the data.
        all_files is a list of tuples (l_file, h_file, db_name)
    """

    # Get all raw data from files
    all_igg_h, all_igg_m = get_data2(all_files[0], all_files[1])
    #for files in all_files:
    #    all_igg += get_data(files[0], files[1], files[2])
    nb_initial_igg_h = len(all_igg_h)
    nb_initial_igg_m = len(all_igg_m)

    # Merge headers of identical IgG
    all_igg_h, all_igg_m = remove_duplicate(all_igg_h, all_igg_m)
    print(f"\nThere are {len(all_igg_h)} IgG after removing duplicates "\
          f"({nb_initial_igg_h} before)")
    print(f"\nThere are {len(all_igg_m)} IgG after removing duplicates "\
          f"({nb_initial_igg_m} before)")

    sys.exit(0)
    # Separate unique from non-unique IgG
    unique_igg, non_unique_igg = separate_unique_igg(all_igg)
    # Clusterize the non-unique IgG
    non_unique_igg = cluster_linked_igg(non_unique_igg)
    # Try to get more IgGs from clusters
    new_unique_igg = truncate_from_clusters(non_unique_igg, org_name, min_size, max_size)

    # Get all potential unique
    all_igg = unique_igg + new_unique_igg
    # Merge headers of identical IgG
    all_igg = remove_duplicate(all_igg)
    # Separate unique from non-unique IgG
    unique_igg, new_non_unique_igg = separate_unique_igg(all_igg)

    # Merge all non-unique
    non_unique_igg += new_non_unique_igg
    # Clusterize the non-unique IgG
    non_unique_igg = cluster_linked_igg(non_unique_igg)
    # Final result, a list of unique and non-unique (clustered) IgG
    final_res = unique_igg + non_unique_igg
    #print(non_unique_igg)
    # Print
    unique = 0
    non_unique = 0
    nb_cluster = 0
    all_unique_seq = set()
    for i in final_res:
        # How many unique IgG?
        if len(i.seq_l) == 1 and len(i.seq_h) == 1:
            unique += 1
        # How many non-unique IgG and clusters?
        else:
            nb_cluster += 1
            non_unique += len(i.seq_l)
        # How many unique sequences?
        for j in i.seq_l:
            all_unique_seq.add(j)
        for j in i.seq_h:
            all_unique_seq.add(j)
    print(f"{unique} are unique IgG and {non_unique} are non unique IgG, "\
          f"in {nb_cluster} clusters")
    print(f"There are {len(all_unique_seq)} unique sequences")

    # Export unique data
    export_fasta(unique_igg, f"../../data/{org_name}.fasta")
    # Export clustered data for further analyzes
    #export_cluster_fasta(non_unique_igg, f"{org_name}_non_unique_light_new.fasta", f"{org_name}_non_unique_heavy_new.fasta")

### Let'z go ###
def init():
    """ Launch the main """
    if __name__ == '__main__':
        # Organism
        org_name = "Homo_sapiens"
        # Min/max size
        min_size = 80
        max_size = 150

        # All databases (fasta files) to process.
        # Each database is a tuple of files (light_fasta, heavy_fasta)
        all_files = [f"../../data/{org_name}.fasta", "../../data/Mus_musculus.fasta"]
 

        main(all_files, org_name, min_size, max_size)
        # Changer le main pour prendre plusioeus especes directeent, le but est de travailler sur les dico et pas sur les fichiers
        # The end
        sys.exit(0)
# GOGOGO
init()
