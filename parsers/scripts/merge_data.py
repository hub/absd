# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Merge IgG coming from different fasta files. """
import os
import sys
import time
from difflib import SequenceMatcher
from itertools import combinations
from format import core

class Igg:
    """ Definition of an IgG. A sequence can have multiple headers,
        corresponding to each identical sequences (or seq in seq).
        An IgG can have multiple sequences, when for example a light is
        identical for two different heavy. This light will then be twice
        in this IgG.

    :param header_l: headers for light sequences
    :param header_h: headers for heavy sequences
    :param seq_l: light sequences
    :param seq_h: heavy sequences
    :type header_l: set(string)
    :type header_h: set(string)
    :type seq_l: set(string)
    :type seq_h: set(string)
    """

    def __init__(self, header_l, header_h, seq_l, seq_h):
        # header_l must be a set
        self.header_l = header_l
        # header_h must be a set
        self.header_h = header_h
        # seq_l must be a string
        self.seq_l = {seq_l}
        # seq_h must be a string
        self.seq_h = {seq_h}
        # Is this sequence unique in the datasets?
        self.unique = True

    # self representation for print
    def __repr__(self):
        # Sorted to get identical hash with same set but different order
        return f"{self.header_l}\n{self.seq_l}\n{self.header_h}\n{self.seq_h}\n"

    # Equality between two IgG, only sequences matters
    def __eq__(self, other):
        if isinstance(self, other.__class__):
            if self.seq_l == other.seq_l and self.seq_h == other.seq_h:
                return True
        return False

    # Again, only sequences matters
    def __hash__(self):
        return hash(f"{sorted(list(self.seq_l))}\n{sorted(list(self.seq_h))}")

    def is_in(self, other):
        """ Are one light and one heavy sequences of self in other? """
        # Test each l_seq of self
        l_is_in = False
        if len(self.seq_l) > 1:
            for i in self.seq_l:
                if len(other.seq_l) > 1:
                    # Compare it to each seq_l of other
                    for j in other.seq_l:
                        # Using in for sub-sequences
                        if i in j:
                            # We found this one!
                            l_is_in = True
                            break
                elif i in next(iter(other.seq_l)):
                    # We found this one!
                    l_is_in = True
                    break
        else:
            s_seq_l = next(iter(self.seq_l))
            if len(other.seq_l) > 1:
                # Compare it to each seq_l of other
                for j in other.seq_l:
                    # Using in for sub-sequences
                    if s_seq_l in j:
                        # We found this one!
                        l_is_in = True
                        break
            elif s_seq_l in next(iter(other.seq_l)):
                # We found this one!
                l_is_in = True
        # No l_seq was found
        if not l_is_in:
            return False

        # Test each h_seq of self
        h_is_in = False
        if len(self.seq_h) > 1:
            for i in self.seq_h:
                if len(other.seq_h) > 1:
                    # Compare it to each seq_h of other
                    for j in other.seq_h:
                        # Using in for sub-sequences
                        if i in j:
                            # We found this one!
                            h_is_in = True
                            break
                elif i in next(iter(other.seq_h)):
                    # We found this one!
                    h_is_in = True
                    break
        else:
            s_seq_h = next(iter(self.seq_h))
            if len(other.seq_h) > 1:
                # Compare it to each seq_h of other
                for j in other.seq_h:
                    # Using in for sub-sequences
                    if s_seq_h in j:
                        # We found this one!
                        h_is_in = True
                        break
            elif s_seq_h in next(iter(other.seq_h)):
                # We found this one!
                h_is_in = True
        # No h_seq was found
        if not h_is_in:
            return False
        # Still there? self is in other
        return True

    def is_partially_in(self, other):
        """ Is at least one sequence of self in other or vice-versa? """
        # More than one sequence light in self
        if len(self.seq_l) > 1:
            # More than one sequence light in other
            if len(other.seq_l) > 1:
                # Test each seq_l of self
                for i in self.seq_l:
                    # Compare it to each seq_l of other
                    for j in other.seq_l:
                        # Using in for sub-sequences
                        if i in j or j in i:
                            # We found this one!
                            return True
            # Only one light in other
            else:
                # Test each seq_l of self
                for i in self.seq_l:
                    # Get the single sequence
                    o_seq_l = next(iter(other.seq_l))
                    # Compare it to each seq_l of self
                    if i in o_seq_l or o_seq_l in i:
                        # We found this one!
                        return True
        # Only one light in self
        else:
            # Get the single sequence
            s_seq_l = next(iter(self.seq_l))
            # More than one sequence light in other
            if len(other.seq_l) > 1:
                # Compare it to each seq_l of other
                for j in other.seq_l:
                    # Using in for sub-sequences
                    if s_seq_l in j or j in s_seq_l:
                        # We found this one!
                        return True
            # Only one light in other
            else:
                # Get the single sequence
                o_seq_l = next(iter(other.seq_l))
                # Compare it to the seq_l of self
                if s_seq_l in o_seq_l or o_seq_l in s_seq_l:
                    # We found this one!
                    return True

        # More than one sequence heavy in self
        if len(self.seq_h) > 1:
            # More than one sequence heavy in other
            if len(other.seq_h) > 1:
                # Test each seq_h of self
                for i in self.seq_h:
                    # Compare it to each seq_h of other
                    for j in other.seq_h:
                        # Using in for sub-sequences
                        if i in j or j in i:
                            # We found this one!
                            return True
            # Only one heavy in other
            else:
                # Test each seq_h of self
                for i in self.seq_h:
                    # Get the single sequence
                    o_seq_l = next(iter(other.seq_h))
                    # Compare it to each seq_h of self
                    if i in o_seq_l or o_seq_l in i:
                        # We found this one!
                        return True
        # Only one heavy in self
        else:
            # Get the single sequence
            s_seq_l = next(iter(self.seq_h))
            # More than one sequence heavy in other
            if len(other.seq_h) > 1:
                # Compare it to each seq_h of other
                for j in other.seq_h:
                    # Using in for sub-sequences
                    if s_seq_l in j or j in s_seq_l:
                        # We found this one!
                        return True
            # Only one heavy in other
            else:
                # Get the single sequence
                o_seq_l = next(iter(other.seq_h))
                # Compare it to the seq_h of self
                if s_seq_l in o_seq_l or o_seq_l in s_seq_l:
                    # We found this one!
                    return True

        # Still there? self is not at all on other
        return False

    def merge_headers(self, other):
        """ Put headers form other into self """
        # Put headers of other into self
        self.header_l.update(other.header_l)
        self.header_h.update(other.header_h)

    def merge(self, other):
        """ Put everything from other into self """
        # Put headers of other into self
        self.header_l.update(other.header_l)
        self.header_h.update(other.header_h)
        # Put sequences of other into self
        self.seq_l.update(other.seq_l)
        self.seq_h.update(other.seq_h)
        # Not unique anymore
        self.unique = False
        other.unique = False

def get_data(input_file_l, input_file_h, db_name):
    """ Get data from two fasta files, on containing light chains and
    the other containing heavy chains. File must have the same size.
    """
    all_igg = []
    # Same number of lines in both files?
    nb_l_l = 0
    nb_l_h = 0
    with open(input_file_l, encoding="UTF-8") as tmp_f:
        for nb_l_l, _ in enumerate(tmp_f, 1):
            pass
    with open(input_file_h, encoding="UTF-8") as tmp_f:
        for nb_l_h, _ in enumerate(tmp_f, 1):
            pass
    # Different number of lines
    if nb_l_l != nb_l_h:
        print("Error: not same number of lines in"\
              f" {input_file_l} ({nb_l_l}) and {input_file_h} ({nb_l_h})")
        sys.exit(1)
    # Empty files
    elif nb_l_l == 0:
        print(f"Error: files {input_file_l} and {input_file_h} are empty")
        sys.exit(1)
    # All good!
    nb_igg = int(nb_l_l/2)
    print(f"{nb_igg} IgG founded in files {input_file_l} and {input_file_h}")

    # Open both files
    with open(input_file_l, encoding="UTF-8") as l_file,\
         open(input_file_h, encoding="UTF-8") as h_file:
        # Iterate line by line in both files at the same time
        for l_line, h_line in zip(l_file, h_file):
            # We have two headers
            if l_line.startswith(">") and h_line.startswith(">"):
                # Remove the ">" and add the db_name if necessary
                header_l = f"{l_line[1:].strip()};{db_name.strip()}"
                header_h = f"{h_line[1:].strip()};{db_name.strip()}"
            # We have two sequences
            elif not l_line.startswith(">") and not h_line.startswith(">"):
                # Create an IgG, headers must be sets
                all_igg.append(Igg({header_l},
                                   {header_h},
                                   l_line.strip(),
                                   h_line.strip()))
    # Remove redundancy inside this file
    start = time.time()
    all_igg = remove_duplicate_from_list(all_igg)
    print(f"remove_duplicate_from_list: {time.time() - start:.2f}")
    return all_igg, nb_igg

def remove_duplicate_from_list(all_igg):
    """ Removes duplicate in a list of IgG. When light and heavy sequences of
    an IgG are inside or equal to another IgG, merge headers and keep only one
    copy. An IgG can be merge to several other
    """
    # Already processed sequences, they should not be used again
    processed = set()
    # Final result
    res = []
    # For each IgG
    for pos, igg1 in enumerate(all_igg):
        # This IgG has not been already processed
        if igg1 not in processed:
            # This one must be added
            to_add = True
            # Get all IgG with same sequences
            for igg2 in all_igg[pos+1:]:
                # igg2 in igg1?
                # Any IgG 'in' igg2 will also be added in igg1 here
                if igg2.is_in(igg1):
                    # Put headers of igg2 in igg1
                    igg1.merge_headers(igg2)
                    # Tag the second as processed, to not used it again
                    processed.add(igg2)
                # igg1 in igg2?
                # Any IgG 'in' igg1 will be added in igg2 later
                elif igg1.is_in(igg2):
                    # Put headers of igg1 in igg2
                    igg2.merge_headers(igg1)
                    # Don't add igg1, igg2 will be added later
                    to_add = False
            # igg1 must be added
            if to_add:
                # Unique sequences with (potentially) many headers
                res.append(igg1)
    return res

def merge_and_remove_duplicate(data, all_igg):
    """ Merge the two lists and removes duplicate in IgG.
    When light and heavy sequences of an IgG are inside or equal to another
    IgG, merge headers and keep only one copy. An IgG can be merge to several
    other Here, data is non-redundant
    """
    # all_igg is not empty
    if all_igg:
        # IgG to remove from all_igg because a newer version exists
        to_rm_all = set()
        # For each IgG in all_igg
        for igg1 in all_igg:
            # IgG to remove from data
            to_rm = set()
            # Get all IgG in data
            for igg2 in data:
                # IgG from data into all_igg?
                if igg2.is_in(igg1):
                    # Put headers of igg2 in igg1
                    igg1.merge_headers(igg2)
                    # Remove it from data
                    to_rm.add(igg2)
                # IgG from all_igg into data?
                elif igg1.is_in(igg2):
                    # Put headers of igg1 in igg2
                    igg2.merge_headers(igg1)
                    # Remove it from all_data
                    to_rm_all.add(igg1)
            # Remove IgG from data
            for igg2 in to_rm:
                data.remove(igg2)
        # Remove IgG from all_data
        for igg in to_rm_all:
            all_igg.remove(igg)
        # Add all none processed form data
        for igg2 in data:
            all_igg.append(igg2)
    # The first sample
    else:
        all_igg = data
    return all_igg

def generalized_lcs(all_seq, min_size):
    """ Find the 'longest common substring' of all sequences """
    common = set()
    # Pairwise LCS
    for seq_a, seq_b in combinations(all_seq, 2):
        # Longest common substring between two sequences
        tmp = SequenceMatcher(None, seq_a, seq_b).find_longest_match()
        # Add the corresponding substring
        common.add(seq_a[tmp[0]:tmp[0]+tmp[2]])
    # It is not good yet
    good = False
    # We have results?
    if common:
        # Take the smallest substring
        smallest = min(common, key=len)
        # The sequence is long enough
        if len(smallest) >= min_size:
            # We take it
            good = True
            # For each other (longer) substring
            for i in common:
                # The smallest is not inside!
                if smallest not in i:
                    # We don't have a good substring
                    good = False
                    break
    # We have a good substring
    if good:
        # Return it
        return smallest
    # Nothing good
    return None

def create_cluster(igg1, all_igg):
    """ Find all IgG in all_igg that clusterize with igg1 (and with IgGs that
    clusterize with igg1, etc). It modifies igg1.
    """
    change = True
    # Something change on this IgG
    while change:
        # Nothing changed this time
        change = False
        # For each IgG
        for igg2 in all_igg:
            # This IgG was not already used
            if igg2.unique and igg1 != igg2:
                # IgGs share at least on sequence
                if igg1.is_partially_in(igg2):
                    # Merge them into igg1
                    igg1.merge(igg2)
                    # This IgG changed
                    change = True

def reduce_igg(igg, org_name, min_size, max_size):
    """ Try to simplify a cluster into a single IgG by removing some aa """
    # Get correct sizes, starts and ends
    light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before = core.STARTS_ENDS[org_name]
    # We have several light
    if len(igg.seq_l) > 1:
        # Find the smallest lcs for lights
        lcs_l = generalized_lcs(igg.seq_l, min_size)
    else:
        # A single sequence
        lcs_l = next(iter(igg.seq_l))
    # We have a lcs for lights
    if lcs_l:
        # We have several heavy
        if len(igg.seq_h) > 1:
            # Find the smallest lcs for heavys
            lcs_h = generalized_lcs(igg.seq_h, min_size)
        else:
            # A single sequence
            lcs_h = next(iter(igg.seq_h))
        # We have a lcs for heavys
        if lcs_h:
            # Can it form a correct IgG?
            correct, valid, _ = core.make_smaller_igg(core.Sequence("", lcs_l),
                                                      core.Sequence("", lcs_h),
                                                      max_size,
                                                      light_starts,
                                                      light_ends,
                                                      light_ends_before,
                                                      heavy_starts,
                                                      heavy_ends,
                                                      heavy_ends_before)
            # It is (now?) a good IgG?
            if valid and len(correct[0].seq) >= min_size and len(correct[1].seq) >= min_size:
                # Return this new good IgG with corrected sequences
                return Igg(igg.header_l, igg.header_h, correct[0].seq, correct[1].seq)
            # Not a new good unique
            else:
                # Not a new unique
                return None
        # No lcs for heavy
        else:
            # Not a new unique
            return None
    # No lcs for lights
    else:
        # Not a new unique
        return None

def generalized_reduce_igg(igg1, all_igg, org_name, min_size, max_size):
    """ Check if an IgG can create a cluster and reduce it if necessary.
    If reduced, the resulting IgG must also be checked, recursively.
    """
    # Try to create a cluster from igg1 with all_igg
    create_cluster(igg1, all_igg)
    # igg1 is unique, i.e. not a cluster
    if igg1.unique:
        # Return it
        return igg1
    # A cluster was created
    else:
        # Reduce the cluster
        res_igg = reduce_igg(igg1, org_name, min_size, max_size)
        # The cluster was successfully reduce into a single IgG
        if res_igg:
            # This new IgG need to be compared to all other IgG
            return generalized_reduce_igg(res_igg,
                                          all_igg,
                                          org_name,
                                          min_size,
                                          max_size)
        # We have a real cluster
        else:
            return None

def separate_uniques_iggs_lcs(all_igg, org_name, min_size, max_size):
    """ Identify IgG with unique sequences in all data, clusterize other
    sequences and try to reduce them. It modifies all_igg objects.
    """
    # Set of final unique IgG
    uniques_iggs = set()
    # Set of final cluster
    clustered_igg = set()
    # Initial size
    init_size = len(all_igg)
    # For each IgG
    for pos, igg1 in enumerate(all_igg):
        # This IgG is not inside a previous one
        if igg1.unique:
            # Only for the initial ones
            if pos <= init_size:
                # Try to create a cluster from igg1 with all following IgG
                create_cluster(igg1, all_igg[pos+1:])
            # Works on the reduced
            else:
                # Try to create a cluster from igg1 with all IgG
                create_cluster(igg1, all_igg)
             # If this IgG is unique
            if igg1.unique:
                # Add it
                uniques_iggs.add(igg1)
            # We have a cluster
            else:
                # Try to reduce it
                res_igg = reduce_igg(igg1, org_name, min_size, max_size)
                # We now have a unique one
                if res_igg:
                    # Is it really unique ?
                    new_igg = generalized_reduce_igg(res_igg,
                                                     all_igg,
                                                     org_name,
                                                     min_size,
                                                     max_size)
                    # It is really unique
                    if new_igg:
                        # Add it
                        uniques_iggs.add(new_igg)
                        all_igg.append(new_igg)
                    # We actually have a cluster
                    else:
                        # Add it
                        clustered_igg.add(igg1)
                # We actually have a cluster
                else:
                    # Add it
                    clustered_igg.add(igg1)
    # We might have not unique in uniques_iggs (from the reduces)
    not_unique = set()
    for igg in list(uniques_iggs):
        if not igg.unique:
            uniques_iggs.remove(igg)
    # Return both lists
    return uniques_iggs, clustered_igg

def export_fasta(all_igg, output_file, sep="|||"):
    """ Write a fasta containing all IgG, light then heavy chain.
        Different headers for a single sequence are separated by |||
        by default. First header is the short id (identical between
        light/heavy)"""
    # Open both files
    with open(output_file, "w", encoding="UTF-8") as o_file:
        for igg in all_igg:
            # LIGHT
            # tmp header
            res_header = ""
            # For each light header
            for header in sorted(list(igg.header_l)):
                # Add this header
                res_header += header + sep
            # Get the short id
            short_id = res_header.split("_")[0]
            # Write the light header, omission the last sep characters
            o_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
            # Get the sequence (unique)
            for seq in igg.seq_l:
                # Write sequence
                o_file.write(f"{seq}\n")
            # HEAVY
            # tmp header
            res_header = ""
            # For each heavy header
            for header in sorted(list(igg.header_h)):
                # Add this header
                res_header += header + sep
            # Get the short id
            short_id = res_header.split("_")[0]
            # Write the light header, omission the last sep characters
            o_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
            # Get the sequence (unique)
            for seq in igg.seq_h:
                # Write sequence
                o_file.write(f"{seq}\n")

'''
def export_cluster_fasta(all_igg, output_file_l, output_file_h, sep="|||"):
    """ Write two fasta files, one for light, one for heavy for each cluster
        Different headers for a single sequence are separated by |||
        by default """
    cpt = 0
    for igg in all_igg:
        with open(str(cpt)+"_"+output_file_l, "w", encoding="UTF-8") as l_file,\
             open(str(cpt)+"_"+output_file_h, "w", encoding="UTF-8") as h_file:
            # All list have the same size
            for pos, _ in enumerate(igg.header_l):
                # LIGHT
                # Write header
                res_header = ""
                for a_header in igg.header_l[pos]:
                    res_header += a_header + sep
                short_id = res_header.split("_")[0]
                l_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                l_file.write(f"{igg.seq_l[pos]}\n")
                # HEAVY
                # Write header
                res_header = ""
                for a_header in igg.header_h[pos]:
                    res_header += a_header + sep
                #short_id = res_header.split("_")[0]  # same as light
                h_file.write(f">{short_id}{sep}{res_header[:-len(sep)]}\n")
                # Write sequence
                h_file.write(f"{igg.seq_h[pos]}\n")
        cpt += 1
'''

def main(org_name, group):
    """ Prepare the data to get a list of IgG.
        An IgG with only one light/heavy is unique in the data.
    """
    # Min/max size
    min_size = 80
    max_size = 150
    # All databases (fasta files) to process.
    # Filename: DB name
    all_db = {"AbDb": "AbDb",
              "AbPDB": "AbPDB",
              "CATNAP-HIV": "CATNAP-HIV",
              "CoV-AbDab-PDB": "CoV-AbDab-PDB",
              "CoV-AbDab": "CoV-AbDab",
              "EBOLA": "EBOLA",
              "IMGT-INN": "IMGT",
              "IMGT-PDB": "IMGT",
              "IMGT": "IMGT",
              "IMGT2": "IMGT",
              "KABAT": "KABAT",
              "OAS": "OAS",
              "PairedNGS": "PairedNGS",
              "PDB": "PDB",
              "PLAbDab": "PLAbDab",
              "SACS": "SACS",
              "SAbDab": "SAbDab",
              "Thera-SAbDab": "Thera-SAbDab",
              "UNIPROT": "UNIPROT"}
    # All files to proceed
    all_files = []
    # Get all files
    for filename, db_name in all_db.items():
        light_f = f"../../CLEAN/{group}_{filename}_{org_name}_light.fasta"
        heavy_f = f"../../CLEAN/{group}_{filename}_{org_name}_heavy.fasta"
        # These files exist
        if os.path.isfile(light_f) and os.path.isfile(heavy_f):
            # Add this database
            all_files.append((light_f, heavy_f, db_name))

    print(group)
    # Get all raw data from files
    start0 = time.time()
    all_igg = set()
    nb_initial_igg = 0
    # For each file
    for files in all_files:
        # Get its data (non redundant)
        data, nb_igg = get_data(files[0], files[1], files[2])
        nb_initial_igg += nb_igg
        # Merge headers of identical IgG between all previous igg and this file
        start = time.time()
        all_igg = merge_and_remove_duplicate(data, all_igg)
        print(f"merge_and_remove_duplicate: {time.time() - start:.2f}\n")

    print(f"\nThere are {len(all_igg)} IgG after removing duplicates "\
          f"({nb_initial_igg} before) ({time.time() - start0:.2f}s)\n")

    start = time.time()
    # Separate unique from non-unique IgG and clusterize non-unique
    uniques_iggs, clustered_igg = separate_uniques_iggs_lcs(all_igg,
                                                        org_name,
                                                        min_size,
                                                        max_size)
    print(f"There are {len(uniques_iggs)} unique IgG and {len(clustered_igg)} "\
          f"clusters after clustering phase ({time.time() - start:.2f}s)\n")

    start = time.time()
    # Print
    unique = 0
    not_unique = 0
    nb_cluster = 0
    all_unique_seq = set()
    # For all unique sequences and clusters
    for i in uniques_iggs.union(clustered_igg):
        # How many unique IgG?
        if len(i.seq_l) == 1 and len(i.seq_h) == 1:
            unique += 1
            if not i.unique:
                print("ERROR, problem in unique!")
                print(i)
        # How many non-unique IgG and clusters?
        else:
            nb_cluster += 1
            not_unique += len(i.seq_l)
        # How many unique sequences?
        for j in i.seq_l:
            all_unique_seq.add(j)
        for j in i.seq_h:
            all_unique_seq.add(j)
    print(f"{unique} are unique IgG and {not_unique} are non unique IgG, "\
          f"in {nb_cluster} clusters")
    print(f"There are {len(all_unique_seq)} unique sequences")
    print(f"End : {time.time() - start:.2f}")

    # Export unique data
    export_fasta(uniques_iggs, f"{group}_{org_name}.fasta")
    # Export clustered data for further analyzes
    #export_cluster_fasta(clustered_igg, f"{org_name}_not_unique_light_new.fasta", f"{org_name}_not_unique_heavy_new.fasta")

if __name__ == '__main__':
    # All good?
    if len(sys.argv) == 3:
        # Launch the main
        main(sys.argv[1], sys.argv[2])
        # The end
        sys.exit(0)
    # There is an error somewhere
    print("Error launching merge_data.py")
    sys.exit(1)
