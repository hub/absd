#!/bin/sh

source /opt/gensoft/adm/etc/profile.d/modules.sh
module purge
module load pypy/3.10-v7.3.16

export BLAST_USAGE_REPORT=false

# Update the repo before starting
git pull

# Remove old run folder
/bin/rm -rf run

# Remove previous error file
/bin/rm -rf ../../data/*.err

# Download all data
for file in $(ls -1 getters/get_*.py)
do
    echo $file
    pypy3 $file
done
# Download from the PDB
pypy3 -u getters/final_PDB.py
echo "Download done!"

# Clean the main folder
/bin/rm -f ../CLEAN/IG*.fasta
# Create the files to run
jobid0=$(sbatch --wait --parsable -p hubbioit --mail-user=nmaillet --mail-type=fail --wrap="pypy3 create_format.py")
# Go in run dir
cd run
# Run all format_*.py
jobid1=$(sbatch --wait --parsable -p hubbioit --dependency=afterok:$jobid0 format_sample.sh)
# Go back
cd ..
# Merge files all-*.txt into all.txt
jobid2=$(sbatch --wait --parsable -p hubbioit --mail-user=nmaillet --mail-type=fail --dependency=afterok:$jobid1 --wrap="pypy3 cat_all.py")
# Create the files to run
jobid3=$(sbatch --wait --parsable -p hubbioit --mail-user=nmaillet --mail-type=fail --dependency=afterok:$jobid2 --wrap="pypy3 create_merge.py")
# Go in run dir
cd run
# Launch all merge_samples.py
jobid4=$(sbatch --wait --parsable -p hubbioit --dependency=afterok:$jobid3 merge_sample.sh)
# Go back
cd ..
# Merge results per species into species.fasta
jobid5=$(sbatch --wait --parsable -p hubbioit --mail-user=nmaillet --mail-type=fail --dependency=afterok:$jobid4 --wrap="pypy3 cat_final.py")
# Remove last redundancy
jobid6=$(sbatch --wait --parsable -p hubbioit --mail-user=nmaillet --mail-type=fail --dependency=afterok:$jobid5 --wrap="pypy3 -u clean_last.py")
# Move to result folder
cd ../../data
# Get the current date
curdate=$(date +'%Y-%m-%d')
# Remove cluster files
/bin/rm -rf ../parsers/scripts/slurm-*
# Pull for modifications during update
git pull
# Push it on git
git add *.fasta
git commit -m "Auto-update $curdate"
git pull
git push
# Done!
exit 0
