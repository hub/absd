# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Merge all-*.txt file ont a coherent all.txt file"""
import pathlib
import glob
import os
import sys

def main():
    """ The main """
    # Current path
    path = pathlib.Path(__file__).parent.resolve()
    # All wanted files
    all_files = glob.glob(f"{path}/../manual/all-*.txt")
    # Get previously manually paired sequences
    all_paired = set()
    # For each all* files
    for file in all_files:
        # Open it
        with open(file, encoding="utf-8") as dbf:
            # Get all paired sequences
            for line in dbf:
                # We have a pair
                if line != "\n":
                    # Add light -> heavy
                    all_paired.add((line.strip(), next(dbf).strip()))

    # Re-write all.txt file for this db
    with open(f"{path}/../manual/all.txt", "w", encoding="utf-8") as allf:
        # For each id
        for light, heavy in all_paired:
            # Write in all.txt file
            allf.write(f"{light}\n")
            allf.write(f"{heavy}\n\n")

    # Remove all* files
    for file in all_files:
        os.remove(file)

if __name__ == '__main__':
    main()
    sys.exit(0)

