# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from IMGT database https://www.imgt.org

import gzip
import os
import glob
import tarfile
import shutil
import sys
import subprocess

filename = "IMGT-INN"
filename2 = "IMGT"

# Create the output dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Display
print(f"Starting {filename}...")

# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")

# Get release version
try:
    subprocess.run(f"wget https://www.imgt.org/download/3Dstructure-DB/RELEASE -O run/version.txt",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)
with open("run/version.txt", encoding="utf-8") as inp_f:
    version = next(inp_f).strip()
# Remove this file
os.remove("run/version.txt")

# Download the file if we don't already have it
if not os.path.isfile(f"../RAW/IMGT3DFlatFiles_{version}.tgz"):
    # Remove previous versions
    for file in glob.glob("../RAW/IMGT3DFlatFiles*.tgz"):
        os.remove(file)
    # Get the raw data
    try:
        subprocess.run(f"wget https://www.imgt.org/download/3Dstructure-DB/IMGT3DFlatFiles.tgz -O ../RAW/IMGT3DFlatFiles_{version}.tgz",
                       shell=True,
                       check=True)
    except subprocess.CalledProcessError as err:
        print(f"command '{err.cmd}' return with error (code "\
              f"{err.returncode}): {err.stderr}", file=sys.stderr)
        sys.exit(1)

# Create the RAW dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Clean the previous downloaded folder
if os.path.isdir(f"../RAW/{filename}"):
    shutil.rmtree(f"../RAW/{filename}")

# Create the output folder
os.makedirs(f"../RAW/{filename}")

# Get all unique ids
all_ids = set()
# Open the tar file without extracting it
tarf = tarfile.open(f"../RAW/IMGT3DFlatFiles_{version}.tgz", "r:gz")
# For each filename
for file in tarf.getnames():
    # Move and extract all INN files
    if file.endswith(".inn.gz"):
        # Extract the file that is a gz
        tarf.extract(file, f"../RAW/{filename}")
        # Get the tmp folder
        tmp_folder = file.split("/")[0]
        # Extract the gz file
        with gzip.open(f"../RAW/{filename}/{file}", "rb") as f_in:
            with open(f"../RAW/{filename}/{file.split('/')[-1][:-3]}", "wb") as f_out:
                shutil.copyfileobj(f_in, f_out)
    # Get all pdb ids
    if file.endswith(".pdb.gz"):
        all_ids.add(file.split("-")[1].split(".")[0])

# Remove old folder
shutil.rmtree(f"../RAW/{filename}/{tmp_folder}")

# Create a clean file
with open(f"run/{filename2}.txt", "w", encoding="utf-8") as out_f:
    for id_ in all_ids:
        out_f.write(f"{id_}\n")

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
