# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from UNIPROT database https://www.uniprot.org/

import os
import sys
import subprocess
import urllib.request, json 

filename = "UNIPROT"

# Create the output dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Display
print(f"Starting {filename}...")

def get_full_page(url, res):
    # Get the raw data
    try:
        result = subprocess.run(("curl", "-is", url),
                                shell=False,
                                check=True,
                                universal_newlines=True,
                                capture_output=True)
    except subprocess.CalledProcessError as err:
        print(f"command '{err.cmd}' return with error (code "\
              f"{err.returncode}): {err.stderr}", file=sys.stderr)
    next_url = None
    all_good = False
    # For each line generated
    for line in result.stdout.split("\n"):
        if all_good:
            if len(line) > 0:
                res += f"{line}\n"
        elif line.startswith("link:"):
            next_url = line.split(" ")[1][1:-2]
        elif line.startswith(">"):
            all_good = True
            if len(line) > 0:
                res += f"{line}\n"
    # There is another page
    if next_url:
        res = get_full_page(next_url, res)
    # Return the current results
    return res

# Write the data
with open(f"../RAW/{filename}.fasta", "w", encoding="utf-8") as out_f:
    res = ""
    res = get_full_page("https://rest.uniprot.org/uniprotkb/search?query=Immunoglobulin%2C%20Antibody%2C%20IG&format=fasta", res)
    out_f.write(res)

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
