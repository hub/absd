# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from IMGT database https://www.imgt.org

import gzip
import os
import glob
import tarfile
import shutil
import sys
import subprocess

filename = "IMGT-DNA"

# Create the output dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Display
print(f"Starting {filename}...")

# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")

# Get release version
try:
    subprocess.run(f"wget https://www.imgt.org/download/GENE-DB/RELEASE -O run/db_version.txt",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)
with open("run/db_version.txt", encoding="utf-8") as inp_f:
    version = next(inp_f).strip()
# Remove this file
os.remove("run/db_version.txt")

# Download the file if we don't already have it
if not os.path.isfile(f"../RAW/IMGTGENEDB_{version}.fasta"):
    # Remove previous versions
    for file in glob.glob("../RAW/IMGTGENEDB*.fasta"):
        os.remove(file)
    # Get the raw data
    try:
        subprocess.run(f"wget https://www.imgt.org/download/GENE-DB/IMGTGENEDB-ReferenceSequences.fasta-AA-WithoutGaps-F+ORF+inframeP -O ../RAW/IMGTGENEDB_{version}.fasta",
                       shell=True,
                       check=True)
    except subprocess.CalledProcessError as err:
        print(f"command '{err.cmd}' return with error (code "\
              f"{err.returncode}): {err.stderr}", file=sys.stderr)
        sys.exit(1)

# Create the RAW dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
