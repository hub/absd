# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from AbPDB database http://www.abybank.org/abpdbseq/

import os
import sys
import subprocess

filename = "AbPDB"

# Display
print(f"Starting {filename}...")

# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")

# Get the raw data
try:
    subprocess.run(f"wget http://www.abybank.org/abpdbseq/FASTA/abpdbseq_latest.faa -O run/{filename}.txt",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)

# Get all unique ids
all_ids = set()
with open(f"run/{filename}.txt", encoding="utf-8") as inp_f:
    for line in inp_f:
        if line.startswith(">"):
            all_ids.add(line[1:].split("_")[0])

# Clean the initial downloaded file
os.remove(f"run/{filename}.txt")

# Create a clean file
with open(f"run/{filename}.txt", "w", encoding="utf-8") as out_f:
    for id_ in all_ids:
        out_f.write(f"{id_}\n")

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
