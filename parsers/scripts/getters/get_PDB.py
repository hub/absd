# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from PDB database https://www.rcsb.org/

import os
import sys
import subprocess
import urllib.request, json 

filename = "PDB"

# Display
print(f"Starting {filename}...")

# Get the raw data and all unique ids
all_ids = set()
# Query the PDB
with urllib.request.urlopen(r"https://search.rcsb.org/rcsbsearch/v2/query?json=%7B%22query%22%3A%7B%22type%22%3A%22terminal%22%2C%22label%22%3A%22text%22%2C%22service%22%3A%22text%22%2C%22parameters%22%3A%7B%22attribute%22%3A%22rcsb_polymer_entity.rcsb_macromolecular_names_combined.name%22%2C%22operator%22%3A%22contains_words%22%2C%22value%22%3A%22light+heavy%22%7D%7D%2C%22return_type%22%3A%22entry%22%2C%22request_options%22%3A%7B%22return_all_hits%22%3Atrue%7D%7D") as url:
    data = json.load(url)
    for res in data["result_set"]:
        all_ids.add(res["identifier"])

# Create a clean file
with open(f"run/{filename}.txt", "w", encoding="utf-8") as out_f:
    for id_ in all_ids:
        out_f.write(f"{id_}\n")

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
