# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from PDB database https://www.rcsb.org/

import os
import sys
import subprocess
import glob
from collections import defaultdict

# Display
print(f"Starting Final PDB...")

# Get all unique ids
all_ids = defaultdict(set)
all_names = set()
all_files_in_run = glob.glob("run/*.txt")
for file in all_files_in_run:
    filename = file.split("/")[-1].split(".txt")[0]
    all_names.add(filename)
    with open(file, encoding="utf-8") as inp_f:
        for line in inp_f:
            all_ids[line.strip().upper()].add(filename)

# Create the RAW dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")
if not os.path.exists("../RAW/PDB"):
    os.makedirs("../RAW/PDB")

# Open all files and add them to a dico
all_files = {}
for filename in all_names:
    # Clean the previous downloaded file
    if os.path.isfile(f"../RAW/{filename}.fasta"):
        os.remove(f"../RAW/{filename}.fasta")
    # Open the file
    tmp = open(f"../RAW/{filename}.fasta", "w", encoding="utf-8")
    # Add it to the dict
    all_files[filename] = tmp

cpt = 0
size = len(all_ids)
# Get all ids from the PDB
for id_ in all_ids:
    cpt += 1
    print(f"{cpt}/{size}")
    # Download the file if we don't already have it
    if not os.path.isfile(f"../RAW/PDB/{id_}.fasta"):
        try:
            subprocess.run(f"wget https://www.rcsb.org/fasta/entry/{id_}/download -O ->> ../RAW/PDB/{id_}.fasta",
                           shell=True,
                           check=True)
        except subprocess.CalledProcessError as err:
            print(f"command '{err.cmd}' return with error (code "\
                  f"{err.returncode}): {err.stderr}", file=sys.stderr)
    # Copy the content of this file
    tmp_seq = ""
    with open(f"../RAW/PDB/{id_}.fasta", encoding="utf-8") as tmp_file:
        for line in tmp_file:
            tmp_seq += line
    # Write it in all required fasta files
    for filename in all_ids[id_]:
        pass
        all_files[filename].write(tmp_seq)

# Close all files
for file in all_files.values():
    # Open the file
    file.close()

# Remove temp files
for file in all_files_in_run:
    os.remove(file)

# Display
print(f"Final PDB is done!")

# All good
sys.exit(0)
