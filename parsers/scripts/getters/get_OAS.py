# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from OAS database https://opig.stats.ox.ac.uk/webapps/oas/oas_paired/

import gzip
import os
import shutil
import sys
import subprocess

filename = "OAS"

# Create the output dir if necessary
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")
if not os.path.exists("../RAW/OAS"):
    os.makedirs("../RAW/OAS")

# Display
print(f"Starting {filename}...")

# Get the raw data
try:
    subprocess.run(f"wget -r --no-parent -N -A '*.gz' https://opig.stats.ox.ac.uk/webapps/ngsdb/paired/ -P ../RAW/",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
