# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from CoV-AbDab database https://opig.stats.ox.ac.uk/webapps/covabdab/

import os
import tarfile
import sys
import subprocess

filename = "CoV-AbDab"

# Display
print(f"Starting {filename}...")

# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")

# Download the index to get good links
try:
    subprocess.run(f"wget https://opig.stats.ox.ac.uk/webapps/covabdab/ -P run/",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)

# Read the index file
with open("run/index.html", encoding="utf-8") as tmp_f:
    for line in tmp_f:
        if "/webapps/covabdab/static/downloads/CoV-AbDab_" in line and ".csv" in line:
            url = line.split("\"")[1]
            name = url.split("/")[-1]

# Remove the file
os.remove("run/index.html")

# Get the raw data
try:
    subprocess.run(f"wget -N https://opig.stats.ox.ac.uk/{url} -P ../RAW/",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
