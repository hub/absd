# -*- coding: utf-8 -*-
########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################
# Get data from AbDb database http://www.abybank.org/abdb/

import os
import sys
import subprocess
import re
import zipfile

filename = "AbDb"

# Display
print(f"Starting {filename}...")

# Create the run dir if necessary
if not os.path.exists("run"):
    os.makedirs("run")
if not os.path.exists("../RAW"):
    os.makedirs("../RAW")

# Get the indexof
try:
    subprocess.run(f"wget -q -O - http://www.abybank.org/abdb/snapshots/ -O run/indexof.txt",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)

# Get all lines having a download link
all_lines = []
with open("run/indexof.txt") as ind_f:
    for line in ind_f:
        if ".zip" in line:
            all_lines.append(line)
            
# Get the name of the latest file
m = re.search("(abdb.*\.zip)\"", all_lines[-1])
latest_file = m.group(1)

# Clean the initial downloaded file
os.remove(f"run/indexof.txt")


# Get the raw data
try:
    subprocess.run(f"wget -N http://www.abybank.org/abdb/snapshots/{latest_file} -P ../RAW/",
                   shell=True,
                   check=True)
except subprocess.CalledProcessError as err:
    print(f"command '{err.cmd}' return with error (code "\
          f"{err.returncode}): {err.stderr}", file=sys.stderr)
    sys.exit(1)


# Get all unique ids
all_ids = set()
# Open the zip
with zipfile.ZipFile(f"../RAW/{latest_file}") as zip_f:
    # Get the file, line by line
    for line in zip_f.read("abdb_newdata_20240706/antibodies.txt").decode("utf-8").split("\n"):
        # Line with ids
        if len(line) > 4:
            # Add it
            all_ids.add(line.split("_")[0])

# Create a clean file
with open(f"run/{filename}.txt", "w", encoding="utf-8") as out_f:
    for id_ in all_ids:
        out_f.write(f"{id_}\n")

# Display
print(f"{filename} is done!")

# All good
sys.exit(0)
