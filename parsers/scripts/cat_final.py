# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Merge all files into a fasta file per species """
import pathlib
import glob
import os
import sys

def main():
    """ The main """
    # Current path
    path = pathlib.Path(__file__).parent.resolve()
    # All wanted files
    all_files = glob.glob(f"{path}/run/*.fasta")
    # Get all possible species
    all_species =set()
    for file in all_files:
        all_species.add("_".join(file.split("_")[-2:]))
    # For each species
    for species in all_species:
        # Open proper output file
        with open(f"{path}/../../data/{species}", "w", encoding="utf-8") as out_f:
            # Open each species file
            for file in all_files:
                # This one is for this species
                if species in file:
                    with open(file, encoding="utf-8") as inp_f:
                        for line in inp_f:
                            out_f.write(line)
    # Remove species files
    for file in all_files:
        os.remove(file)

if __name__ == '__main__':
    main()
    sys.exit(0)

