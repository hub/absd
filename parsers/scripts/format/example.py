# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Example of extracting all paired IgG from a fasta file"""
from collections import defaultdict
import sys
import core

def get_data(input_file, all_chains, organisms, to_remove=None):
    """ Get clean data from file
    :param input_file: Path of the file to parse
    :param all_chains: Keywords linked to LIGHT or HEAVY sequences
    :param organisms: Keywords that should appear (at least one) in correct headers
    :param to_remove: Keywords that should NOT appear in correct headers
    :type input_file: str
    :type all_chains: list(str)
    :type organisms: list(str)
    :type to_remove: list(str), optional

    :return: All potentially good sequences not paired
    :rtype: defaultdict(id_: list(tuple('ids_description', Sequence object)))
    """
    # Headers in the file are in this form
    # Id|Useless description|Important description|specie
    all_data = defaultdict(list)
    with open(input_file, encoding="UTF-8") as ex_file:
        for line in ex_file:
            # Header
            if line.startswith(">"):
                # We need 'sapiens' or 'human', etc in the header
                if any(org in line.split("|")[3].lower() for org in organisms):
                    # Full header
                    header = line.strip()
                    # Complete id with > like >7PMI_4
                    full_id = line.strip().split("|")[0]
                    # Id like 7PMI
                    id_ = full_id[1:].split("_")[0].lower()
                    # Important description
                    description = line.strip().split("|")[2]
                    # Short header, on which the pairing will be done
                    short_header = f"{full_id}|{description}"
                else:
                    short_header = None
            # We have a header, let take the sequence
            elif short_header:
                # We need something like 'light chain' or 'chain kappa'
                if "chain" in short_header.lower() and \
                   any(chain in short_header.lower() for chain in all_chains):
                    to_add = True
                    # Check if this is NOT an IgG
                    for to_rm in to_remove:
                        # At least one wrong keyword, do not add
                        if to_rm.lower() in short_header.lower():
                            to_add = False
                            break
                    # It is a proper IgG, and this seq is not already in
                    if to_add and not core.is_seq_present(all_data[id_],
                                                     line.strip()):
                        # We need the id without the _1 or anything else from
                        # full_id AND the description, separated by space,
                        # to later do the pairing
                        clean_id = f"{id_} {description}"
                        # Key=id_
                        # Val=tuple('id_ description', Sequence object)
                        all_data[id_].append((clean_id,
                                              core.Sequence(f"{header};{id_}",
                                                              line.strip())))
    return all_data

def main():
    """ Format data from raw fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["factor X"]

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"
    #organisms = ["mouse", "musculus"]
    #org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light", "kappa", "lambda"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy", "alpha", "gamma", "delta", "epsilon", "mu"]

    # File to process
    raw_file = "../../RAW/example.fasta"
    # Output files
    output_light = f"../../CLEAN/example_{org_name}_light.fasta"
    output_heavy = f"../../CLEAN/example_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    main()
    sys.exit(0)
