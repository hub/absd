# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a kind of fasta file from PairedNGS
    They might be duplicate sequences, but ids are unique
"""
from collections import defaultdict
import glob
import os
import sys
import core
import pathlib
import shutil

def get_data(input_files, to_remove=None):
    """ Get clean data from PDB file """
    # Dict of list for the main results
    # key -> pdbid_description
    all_data = defaultdict(list)
    header = None
    cpt = 0
    for input_file in input_files:
        # Open this file
        with open(input_file, encoding="UTF-8") as inp_f:
            for line in inp_f:
                if not header:
                    header = line.strip()[1:]
                else:
                    tmp = line.strip().split("/")
                    heavy = tmp[0]
                    light = tmp[1]

                    # Create both id for further pairing
                    id_light = f"{header}_light"
                    id_heavy = f"{header}_heavy"
                    id_ = header.split("|")[1]
                    f_head = f">{header.replace('-', '_')}"
                    all_data[header].append((id_light, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())}_light;{id_}", light)))
                    all_data[header].append((id_heavy, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())}_heavy;{id_}", heavy)))
                    # Done for this one
                    header = None

    return all_data

def main_human():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["human", "sapiens", "patient", "homo"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Files to process
    raw_files = glob.glob(f"{path}/../../RAW/paired_fasta/human_*.fasta")

    # Output files
    output_light = f"{path}/../../CLEAN/PairedNGS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PairedNGS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_files, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_mouse():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["mouse", "musculus", "mice"]
    org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Files to process
    raw_files = glob.glob(f"{path}/../../RAW/paired_fasta/mouse_*.fasta")

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_files, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)


if __name__ == '__main__':
    print("PairedNGS-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("PairedNGS-mouse")
    main_mouse()
    sys.exit(0)