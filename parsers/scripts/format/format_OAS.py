# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from OAS database
    They might be duplicate sequences, but ids are unique """
import csv
import glob
import re
import gzip
import sys
from collections import defaultdict
import core
import pathlib
import shutil

def get_data(input_folder, organisms):
    """ Get clean data from all OAS files """
    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    all_data = defaultdict(list)
    for folder in glob.glob(f"{input_folder}/*"):
        for input_file in glob.glob(f"{folder}/*csv*/*.gz"):
            sub_folder = input_file.split("/")[-3]
            sub_sub_folder = input_file.split("/")[-2]
            file = input_file.split("/")[-1]
            filename = f"{path}/../../RAW/OAS/{sub_folder}-{sub_sub_folder}-{file[:-3]}"
            # Extract the gz file
            with gzip.open(input_file, "rb") as f_in:
                with open(filename, "wb") as f_out:
                    shutil.copyfileobj(f_in, f_out)
            id_file = ""
            id_seq = ""
            seq = ""
            next_is_seq = False
            with open(filename, encoding="UTF-8") as inpf:
                inpf = csv.reader(inpf)
                line = next(inpf)
                # Get info for headers
                link = line[0].split("Link")[1].split(",")[0].split('"')[2]
                species = line[0].split("Species")[1].split(",")[0].split('"')[2]
                bSource = line[0].split("BSource")[1].split(",")[0].split('"')[2]
                bType = line[0].split("BType")[1].split(",")[0].split('"')[2]
                disease = line[0].split("Disease")[1].split(",")[0].split('"')[2]
                isotype = line[0].split("Isotype")[1].split(",")[0].split('"')[2]
                # It is  the correct organism
                if any(org in species.lower() for org in organisms):
                    # Construct the ref for linking
                    ref = f'{filename.split("/")[-1].replace("-", "/")}.gz'
                    # Construct the id
                    id_file = re.sub("^.*-csv.", "", filename[:-4], flags=re.IGNORECASE)
                    id_file = re.sub("paired", "", id_file, flags=re.IGNORECASE)
                    id_file = re.sub("_all", "", id_file, flags=re.IGNORECASE)
                    id_file = re.sub("_1_", "", id_file, flags=re.IGNORECASE)
                    id_file = id_file.strip("_-")
                    if id_file.count("_") > 2:
                        id_file = "_".join(id_file.split("_")[-2:])
                    id_file = id_file.replace("_", "-")
                    header = next(inpf)
                    pos_id_l = header.index("sequence_id_light")
                    pos_seq_l = header.index("sequence_alignment_aa_light")
                    pos_status_l = header.index("ANARCI_status_light")
                    pos_id_h = header.index("sequence_id_heavy")
                    pos_seq_h = header.index("sequence_alignment_aa_heavy")
                    pos_status_h = header.index("ANARCI_status_heavy")
                    nb_id = 0
                    for line in inpf:
                        # Get the sequence
                        clean_id = f"{id_file}-{nb_id}"
                        # Dans les headers, rajouter les info de tissu, etc
                        header_l = f"{clean_id} {line[pos_id_l]} light"
                        header_h = f"{clean_id} {line[pos_id_h]} heavy"
                        fhead_l = f">{clean_id}_{line[pos_id_l]} Disease={disease} BSource={bSource} BType={bType} Isotype={isotype} Status={line[pos_status_l]} url={link}"
                        fhead_h = f">{clean_id}_{line[pos_id_h]} Disease={disease} BSource={bSource} BType={bType} Isotype={isotype} Status={line[pos_status_h]} url={link}"
                        all_data[clean_id].append((header_l, core.Sequence(f"{' '.join(' '.join(fhead_l.replace('|||', ' ').split()).replace(';', ' ').split())} light;{ref}", line[pos_seq_l])))
                        all_data[clean_id].append((header_h, core.Sequence(f"{' '.join(' '.join(fhead_h.replace('|||', ' ').split()).replace(';', ' ').split())} heavy;{ref}", line[pos_seq_h])))
                        nb_id += 1
    return all_data

def main_human():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["human", "sapiens", "homo"]
    org_name = "Homo_sapiens"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_mouse():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["mouse", "musculus", "mice"]
    org_name = "Mus_musculus"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_bovine():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 220
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["Bos", "Taurus"]
    org_name = "Bos_taurus"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_monkey():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 180
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["Macaca", "mulatta", "monkey", "rhesus", "macaque"]
    org_name = "Macaca_mulatta"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rabbit():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["Oryctolagus", "cuniculus", "rabbit"]
    org_name = "Oryctolagus_cuniculus"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rat():
    """ Format data from OAS dump """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["Rattus", "norvegicus"]
    org_name = "Rattus_norvegicus"

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_folder = f"{path}/../../RAW/opig.stats.ox.ac.uk/webapps/ngsdb/paired/"
    # Output files
    output_light = f"{path}/../../CLEAN/OAS_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/OAS_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_folder, organisms)
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("OAS-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("OAS-mouse")
    main_mouse()
    print()
    print()
    print()
    print()
    print()
    print("OAS-monkey")
    main_monkey()
    print()
    print()
    print()
    print()
    print()
    print("OAS-rabbit")
    main_rabbit()
    print()
    print()
    print()
    print()
    print()
    print("OAS-rat")
    main_rat()
    #print()
    #print()
    #print()
    #print()
    #print()
    #print("OAS-bovine")
    #main_bovine() # Germline annotation database
    sys.exit(0)
