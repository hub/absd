# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Shared functions of parsers
"""
from collections import defaultdict
import pathlib
import itertools
import os
import re
import sys
import subprocess

STARTS_ENDS = {"Homo_sapiens": (
    # All light sequences should start by one of these
    ["AAAMA", "AASG", "AFML", "AGQL", "AGQTQ", "AIQ", "AIRI", "AIRL", "ALSQ",
    "ALTQ", "CFVQ", "DAQL", "DELL", "DFVM", "DIAM", "DIDL", "DIDV", "DIEL",
    "DIEM", "DIFL", "DIHL", "DIHM", "DIK", "DILL", "DILM", "DILV", "DIMM",
    "DIQL", "DIQM", "DIQT", "DIQV", "DIQVT", "DIRL", "DIRV", "DITM", "DIVI",
    "DIVL", "DIVM", "DIVV", "DLLM", "DNVL", "DNVM", "DTPMT", "DTVM", "DTVMT",
    "DVAM", "DVLL", "DVLM", "DVLV", "DVM", "DVQI", "DVQM", "DVVL", "DVVLI",
    "DVVM", "DVVV", "EIAM", "EIEL", "EIFL", "EIGL", "EIIL", "EIIM", "EILL",
    "EILM", "EIML", "EIMM", "EIV", "EKVM", "ELAL", "ELAQ", "ELDM", "ELEL",
    "ELQM", "ELTL", "ELTQ", "ELVM", "EMVL", "EMVM", "EPSL", "ESVL", "ETLM",
    "ETTL", "ETTV", "ETVL", "ETVM", "EVLM", "EVVL", "FLLA", "FLSA", "FMLA",
    "FMLI", "FMLS", "FMLT", "GASV", "GLSQ", "GSPG", "GSQP", "GTPG", "HSAL",
    "HSVL", "HSVS", "IAL", "IEM", "IGVTQ", "IHVTQ", "ILFLC", "ILLT", "IQLTQ",
    "IQM", "IRM", "ISVAP", "IVLA", "IVLP", "IVLS", "IVLT", "IVMA", "IVMI",
    "IVMM", "IVMS", "IVMT", "IWMTQ", "KFML", "KPGA", "KPGE", "LIQPA", "LNQP",
    "LQSG", "LSLP", "LSVA", "LSVEL", "LTLTQ", "LTQF", "LTQP", "LTQSP", "LTQT",
    "MGWSCIILFLVATATGS", "MRLP", "MTQ", "NFIL", "NFLL", "NFML", "NFVL",
    "NLTLI", "PASV", "PGTL", "PHSA", "PHSV", "PPSA", "PPSV", "PSAS", "PSLT",
    "PSSL", "PSTLSA", "PSVS", "PVTL", "QAAL", "QAAV", "QAGL", "QAGP", "QAVL",
    "QAVV", "QCVLT", "QFLL", "QFVL", "QFVP", "QIDL", "QIVLI", "QIVLS", "QIVLT",
    "QLLL", "QLML", "QLVL", "QLVV", "QMTQ", "QPAL", "QPAP", "QPAS", "QPAV",
    "QPEV", "QPGL", "QPIL", "QPLL", "QPML", "QPVL", "QSDL", "QSEL", "QSEV",
    "QSFL", "QSGL", "QSIL", "QSLL", "QSML", "QSPL", "QSPP", "QSPS", "QSQL",
    "QSQLT", "QSSL", "QSTL", "QSVL", "QSVM", "QSVP", "QSVV", "QTAV", "QTEV",
    "QTGV", "QTIV", "QTLV", "QTVL", "QTVM", "QTVV", "QVAL", "QVELT", "QVVF",
    "QVVM", "SALAQ", "SALSQ", "SALTQ", "SALTT", "SELT", "SFDL", "SFEL", "SHEL",
    "SLEL", "SLEV", "SLSA", "SLTC", "SMSVSP", "SNEL", "SSDL", "SSEL", "SSGL",
    "SSLS", "SSMS", "STLS", "SVAL", "SVL", "SVLT", "SVPT", "SVSE", "SVWT",
    "SYAL", "SYDL", "SYDV", "SYEL", "SYEV", "SYGL", "SYKL", "SYQL", "SYVL",
    "TLSA", "TLSL", "TLSV", "TQIP", "TQPH", "TQPP", "TQSP", "TVLTQ", "TVVTQ",
    "VLTQ", "VMTQ", "VSLG", "VSVA", "VTQP", "VVM", "VVTQ", "YDL", "YDLTQ",
    "YEL", "YIGL", "YVLAQ", "YVLTQ"],
    # All light sequences should end by one of these
    ["AEIK", "AKRT", "ALEI", "ANRT", "ARVE", "ASLSS", "ATKV", "AVEI", "CNAY",
    "CSY", "DGSLSG", "DIK", "DLEI", "DYYC", "ELDI", "ELEI", "ELEV", "ELQI",
    "ELRRT", "ERK", "EVDI", "EVEI", "EVEV", "EVTV", "FGGG", "FGGGT", "FGGGTK",
    "FGQG", "FGQGTK", "FGQGTKV", "FKRT", "FTVL", "GAGTKL", "GAKV", "GGGGS",
    "GLGT", "GPGT", "GQGT", "GRGT", "GSGT", "GSKV", "GSRL", "GSTL", "GTAV",
    "GTEL", "GTEV", "GTGT", "GTGTK", "GTKL", "GTKV", "GTMV", "GTNL", "GTNV",
    "GTQV", "GTRL", "GTRV", "GTSL", "GTTL", "GTTV", "HIK", "HLDI", "HLEI",
    "HLQI", "HVEI", "HVTV", "IDLK", "IDVK", "IEIK", "IEMK", "IERT", "IEVK",
    "IKGT", "IKRT", "ILDI", "ILEI", "INRT", "ITFG", "ITVL", "IVDI", "IVEI",
    "IVSS", "KIEI", "KIRT", "KKRT", "KLDI", "KLEF", "KLEI", "KLEL", "KLELR",
    "KLEQL", "KLEV", "KLGI", "KLQI", "KLSV", "KLTV", "KLVI", "KMEI", "KVAI",
    "KVDF", "KVDI", "KVDIK", "KVDL", "KVDLK", "KVDV", "KVEI", "KVEL", "KVEV",
    "KVGI", "KVHI", "KVNI", "KVNV", "KVQI", "KVQV", "KVTV", "LAIK", "LAVL",
    "LDFK", "LDIE", "LDIQ", "LDLK", "LDMK", "LDNK", "LDVK", "LEFK", "LEIE",
    "LEIK", "LEIN", "LEIQ", "LEIR", "LELK", "LEMK", "LENK", "LESK", "LETK",
    "LEVE", "LEVH", "LEVK", "LEVQ", "LGIK", "LIVL", "LKIK", "LLQI", "LNIK",
    "LQIK", "LQVK", "LSVL", "LTIK", "LTVL", "LTVV", "LVEI", "LVIK", "LVVK",
    "MDVK", "MEIK", "MEVK", "MFGG", "MKRT", "MLEI", "MTVL", "MVDI", "MVEI",
    "MVEV", "MVTV", "NLDI", "NLEF", "NLEI", "NLEL", "NLEV", "NLGI", "NLQI",
    "NVAI", "NVDI", "NVEI", "NVEV", "NVL", "NVQ", "NVSS", "PEIK", "PGTK",
    "QAGDQT", "QGTR", "QGTRL", "QKRT", "QLDI", "QLEI", "QLEV", "QLTAL", "QPK",
    "QVDI", "QVDV", "QVEI", "QVEL", "QVEV", "QVTV", "QVYE", "RIEI", "RKRT",
    "RLDI", "RLEI", "RLHI", "RLQI", "RMEI", "RTFG", "RVAV", "RVDF", "RVDI", 
    "RVDL", "RVDLK", "RVDV", "RVEI", "RVHI", "RVQI", "RVQV", "RVTV", "SALGPK",
    "SGSG", "SKVE", "SLDI", "SLEI", "SLQI", "SRLTV", "SSTP", "SSYADS", "STFP",
    "SVEI", "SVEL", "SVEV", "TDRS", "TEVE", "TEVT", "TFGG", "TFGPGT", "TFGQ",
    "TFLTV", "TGTKVT", "THVAAN", "TKLE", "TKLTV", "TKRT", "TKVD", "TKVE",
    "TKVN", "TKVT", "TLDI", "TLEI", "TLEL", "TMVE", "TNLE", "TNVE", "TQLE",
    "TQLIIL", "TQVE", "TRLDIN", "TRLE", "TRLTV", "TRRT", "TRVD", "TRVDKK",
    "TRVE", "TRVP", "TSLE", "TTLE", "TTVD", "TTVE", "TVDF", "TVDI", "TVDL",
    "TVDLK", "TVDV", "TVEF", "TVEI", "TVEL", "TVES", "TVET", "TVEV", "TVTV",
    "VAFK", "VAIK", "VALK", "VAMK", "VAVK", "VAVL", "VDAK", "VDFK", "VDIE",
    "VDIQ", "VDIR", "VDLK", "VDMK", "VDNK", "VDRK", "VDSK", "VDTK", "VDVK",
    "VDVL", "VDVQ", "VDVR", "VEAK", "VEFK", "VEIE", "VEIK", "VEIN", "VEIQ",
    "VEIR", "VELK", "VELR", "VEM", "VENK", "VESK", "VETK", "VEVE", "VEVK",
    "VEVKR", "VEVQ", "VEVR", "VFGG", "VFVL", "VGIK", "VGLK", "VGNK", "VGVK",
    "VHFK", "VHLK", "VHMK", "VHVK", "VHVL", "VIIK", "VIVL", "VKIK", "VKRT",
    "VLEI", "VNIE", "VNIK", "VNLK", "VNMK", "VNVK", "VQIK", "VQVK", "VRIK",
    "VRRT", "VSVI", "VSVL", "VTAL", "VTCL", "VTFL", "VTIK", "VTIL", "VTLK",
    "VTLL", "VTVI", "VTVL", "VTVV", "VVIK", "VVLK", "VVVK", "VVVL", "VYIK",
    "WDSRS", "YLDI", "YLEI", "YTFG"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["AASG", "AGLM", "ALQLV", "AQLV", "ASGF", "ATRLE", "AVALA", "AVKLV",
    "AVSLV", "AVTLD", "DIRIA", "DRLF", "DVHL", "DVKL", "DVQV", "EAEL", "EAQL",
    "EAQV", "EEKL", "EEQ", "EGQL", "EIHL", "EIQ", "ELQL", "ELQLQ", "EMHL",
    "EMQV", "EMR", "EQLV", "EQSG", "ESEGD", "ESGG", "ESGP", "EVEL", "EVHL",
    "EVKK", "EVKL", "EVLL", "EVM", "EVPLV", "EVQ", "EVRL", "EVRV", "EVSL",
    "EVTL", "EVYL", "FTFS", "GASV", "GFTF", "GGGL", "GGGV", "GGPS", "GGSL",
    "GGVV", "GQMV", "HLVA", "HVQV", "INLV", "ITLK", "KESG", "KPGA", "KPGE",
    "LAQSG", "LEE", "LEQS", "LKES", "LLEQ", "LLGQ", "LLQS", "LMQS", "LQES",
    "LQLQ", "LQLV", "LQSG", "LRLS", "LSCA", "LTLS", "LVE", "LVKP", "LVQLV",
    "LVQP", "LVQS", "MEF", "MGWSCIILFLVATATGA", "MQL", "PGAS", "PGGS", "QAIL",
    "QAQ", "QARL", "QATL", "QAYL", "QDLL", "QDQL", "QDRL", "QDTL", "QEEL",
    "QEHL", "QEKL", "QELL", "QEQ", "QERL", "QERLVE", "QES", "QEVL", "QFTL",
    "QGKL", "QGQL", "QGQV", "QGTL", "QHSQ", "QIEL", "QIH", "QIIL", "QINL",
    "QIQ", "QISL", "QISLT", "QITFK", "QITL", "QITM", "QITV", "QLEQ", "QLHL",
    "QLLD", "QLLE", "QLLQ", "QLQ", "QLRL", "QLTL", "QLVA", "QLVD", "QLVE",
    "QLVK", "QLVN", "QLVQ", "QMHL", "QMKL", "QMLA", "QMQ", "QMTL", "QPGG",
    "QPQL", "QSGAE", "QSGP", "QSLE", "QSVE", "QTTL", "QVAL", "QVDL", "QVELK",
    "QVELQ", "QVELV", "QVEM", "QVH", "QVIL", "QVKL", "QVLL", "QVNL", "QVPL",
    "QVQ", "QVRL", "QVRV", "QVSL", "QVTF", "QVTL", "QVTM", "QVTV", "QVYL",
    "RAHL", "RCPL", "RITL", "RLFQ", "RLSC", "RLVE", "RVQL", "SCAA", "SETLS",
    "SGAEV", "SGFT", "SGGG", "SGGS", "SLRL", "SLTC", "SQHL", "SQRLV", "SQSVK",
    "SQVDL", "SQYL", "STLK", "TLKE", "TLSL", "VDSG", "VEAG", "VELV", "VHLE",
    "VKKP", "VKLLE", "VKLV", "VKPT", "VLVK", "VNLL", "VQL", "VQPG", "VQSG",
    "VQVL", "VQVV", "VRLS", "VRLV", "VTLK", "VTLRE"],
    # All heavy sequences should end by one of these
    ["AISS", "ALVT", "ATVS", "AVAS", "AVPS", "AVSA", "AVSP", "AVSS", "AVST",
    "AVSW", "AVTS", "AVTV", "CAK", "CVR", "DIWG", "DLWG", "DPWG", "DYWG",
    "FDYW", "FVSS", "GHRLL", "GQGSLV", "GQGT", "GQGTL", "GRGTL",
    "GTLV", "GTPV", "GTRV", "GVAS", "GVSS", "GWFD", "HVSS", "HYGM", "IASS",
    "IISS", "ITAS", "ITVS", "IVFS", "IVPS", "IVSA", "IVSP", "IVSS", "IVST",
    "IVTS", "KGTTV", "KGTTVT", "KVSS", "KVTV", "KVTVS", "LDYW", "LIVS",
    "LSVSS", "LTVS", "LVAV", "LVSA", "LVSS", "LVSV", "LVSVS", "LVSVT",
    "LVTISS", "LVTV", "LVTVS", "LVTVT", "MDVW", "MITV", "MVFV", "MVSS", "MVSV",
    "MVTISS", "MVTV", "MVTVS", "NISS", "NVAS", "NVFS", "NVSA", "NVSS",
    "PGLRLL", "PVSS", "PVTISS", "PVTV", "PVTVS", "PWGQ", "QGSL", "QGTKVIVS",
    "QGTM", "QGTMV", "QGTMVTV", "QVNV", "QVSV", "QVTV", "QVTVS", "RGYS",
    "RSPSP", "RVSS", "RVSV", "RVTISS", "RVTV", "RVTVS", "SKVTV", "SPSPQ",
    "SVAS", "SVFS", "SVPS", "SVSA", "SVSP", "SVSS", "SVST", "SVTS", "SVTVS",
    "TASP", "TFSS", "TFVRV", "TGSS", "TISP", "TISS", "TITS", "TLIS", "TLSS",
    "TLVT", "TPVIV", "TQVIVS", "TSVF", "TSVTV", "TTVTISS", "TTVTV", "TVAA",
    "TVAP", "TVAS", "TVAT", "TVDS", "TVFS", "TVGS", "TVLS", "TVPP", "TVPS",
    "TVPSS", "TVSA", "TVSP", "TVSS", "TVST", "TVTA", "TVTP", "TVTS", "TVTV",
    "TVTVS", "TVVTV", "TVYS", "VAVS", "VAVSS", "VDVSS", "VHISS", "VIVAS",
    "VIVS", "VIVSS", "VIVVL", "VNVS", "VNVSS", "VPVS", "VPVSS", "VSDL",
    "VSISS", "VSVS", "VSVSS", "VSVTA", "VSVTS", "VTASS", "VTVAS", "VTVF",
    "VTVS", "VTVSS", "VTVTS", "VVS", "VVSA", "VVSS", "VVST", "VVTVS", "VWG",
    "WGKGT", "WGPGT", "WGQG", "WGQGTT", "YGDY", "YMDV", "YVSS", "YWGQ",
    "YWYFDLW"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"] # Not currently used
),
"Mus_musculus": (
    # All light sequences should start by one of these
    ["AIMSA", "AIQ", "ALTT", "ASIS", "ASLAV", "ASLS", "ASQS", "DELL", "DFLM",
    "DIEL", "DIEM", "DIFL", "DIK", "DILI", "DILI", "DILL", "DILM", "DIMI",
    "DIMI", "DIQI", "DIQL", "DIQM", "DIQVT", "DIRM", "DISM", "DIVI", "DIVL",
    "DIVM", "DIWM", "DLLM", "DLVL", "DTVL", "DTVMT", "DVKI", "DVKM", "DVLL",
    "DVLM", "DVQI", "DVQL", "DVQM", "DVVI", "DVVI", "DVVM", "EIGL", "EIIV",
    "EILLT", "EIQM", "EIV", "ELQM", "ELTQ", "ELVM", "ENAL", "ETTV", "ETVL",
    "EVLL", "EVVLT", "FLTL", "FMSTS", "IAL", "IDLT", "IMAA", "IMSA", "IQLTQ",
    "IQM", "IQVT", "IRM", "IVLI", "IVLN", "IVLS", "IVLT", "IVMP", "IVMS",
    "IVMT", "IVRT", "LAVSL", "LPVSL", "LSLPV", "LTLSV", "LTQP", "LTQSP",
    "MTQ", "NIQV", "NIVM", "NIVM", "PAIL", "PASL", "PSSL", "PVSL", "QAVG",
    "QAVV", "QESA", "QIILNIMM", "QILLT", "QIVL", "QIVLF", "QIVLS", "QIVLT",
    "QLVL", "QMNQ", "QMTQ", "QPVL", "QSPA", "QSPS", "QSVL", "QTVLS", "QTVV",
    "QVVM", "SALT", "SELT", "SIVM", "SIVMA", "SLAM", "SLAV", "SLGD", "SLPV",
    "SLSV", "SLTV", "SPSS", "SSEL", "SSLSA", "SVPV", "TGETTQ", "TLSV", "TMAA",
    "TQAA", "TQAA", "TQPA", "TQPS", "TQSP", "TQSS", "TQTP", "TVLTQ", "VIQE",
    "VIQS", "VLSQ", "VLTQ", "VMTQ", "VSLG", "VSLT", "VTQE", "VVM", "VVTQ",
    "YASL", "YLSV"],
    # All light sequences should end by one of these
    ["CRTR", "DIK", "DYYC", "ELEIK", "FGGG", "FGGGT", "FGGGTK", "FGQGTKV",
    "FGSG", "FKRT", "GAGT", "GAGTK", "GGGGS", "GSGT", "GTGTK", "GTKL", "IERT",
    "IKRT", "ITRE", "KLEI", "KLEIK", "KLELK", "KLELR", "KLEV", "KLGIK",
    "KLKMK", "KLQI", "KLTVL", "KVEI", "KVEVK", "LDLK", "LEIK", "LEIN", "LEIR",
    "LELK", "LELR", "LEMK", "LQIK", "LYAG", "MKRT", "NLEIK", "QLEIK", "QPK",
    "RLEIK", "RLTVL", "SGTK", "SRTR", "SSTR", "TFGA", "TFGG", "TFGPGT", "TKLE",
    "TKLTV", "TLEIK", "VEIK", "VELK", "VELR", "VEM", "VKRT", "VQVD", "VTVL",
    "VYAG", "YPLTF"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["AVALA", "AVHL", "DLVK", "DVKL", "DVNL", "DVQV", "EEKL", "EIQ", "ELKK",
    "ELVK", "ELVR", "ESGA", "ESGG", "ESGP", "EVHL", "EVHM", "EVKI", "EVKF", "EVKL",
    "EVLL", "EVM", "EVNL", "EVQ", "EVRL", "EVTL", "EVYL", "GAEL", "GGGL",
    "GGLVQ", "GILQ", "GLVA", "GLVQ", "GPEL", "GPELKK", "GPGL", "GTEL", "IPRC",
    "KPS", "KVKL", "LAQSG", "LEE", "LQES", "LQLQ", "LQLV", "LQQP", "LQQS",
    "LVE", "LVKP", "LVKT", "MEF", "MQL", "PGAS", "QAQ", "QATL", "QAYL", "QDQL",
    "QDTL", "QES", "QFTLK", "QGQL", "QIH", "QIQ", "QITL", "QITQ", "QLLQ",
    "QLQ", "QLVE", "QLVQ", "QMQ", "QREL", "QSGAE", "QSGP", "QVELQ", "QVELV",
    "QVH", "QVIL", "QVKL", "QVQ", "QVRL", "QVTL", "RVQL", "SGGG", "SGPG", 
    "SGTV", "SLSL", "VELV", "VHLQ", "VKLD", "VKLLE", "VKLQ", "VKLV", "VKPG",
    "VNLL", "VQF", "VQL", "VQWQ", "VTLK", "VTLR"],
    # All heavy sequences should end by one of these
    ["AVSS", "CVR", "DSGH", "GQGSLV", "GQGT", "GTLV", "IVSA", "IVSS", "LTVS",
    "LVSVS", "LVTV", "LVTVS", "LVTVT", "MVTVS", "PSPQ", "PVTVS", "QVSVS",
    "SLTG", "SQSP", "SVTVS", "TGTG", "TGTT", "TLKV", "TLPV", "TLRV", "TLSVSS",
    "TLTD", "TLTF", "TLTG", "TLTL", "TLVT", "TSVT", "TSVTV", "TTLTV", "TTVTV",
    "TVAMD", "TVFS", "TVPSS", "TVSA", "TVSS", "TVST", "TVTF", "TVTG", "TVTI",
    "TVTVS", "VAVSS", "VIVSS", "VNVSS", "VPVSS", "VSSA", "VSVSS", "VTASS",
    "VTVAS", "VTVF", "VTVP", "VTVSS", "VTVT", "VTVTS", "VVSS", "VWG", "YWGQ"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"], # Not currently used
),
"Bos_taurus": (
    # All light sequences should start by one of these
    ["ATMQ", "DIQV", "DIVL", "DVVL", "EAVL", "MGIL", "QAGL", "QAVL", "QDVL",
    "QIVI", "QPVL", "QSGL", "QSSL", "QTVI", "SSQL", "SYEL", "TVAQ"],
    # All light sequences should end by one of these
    ["LEIR", "LEVK", "LTVL", "VEIK", "VEIN", "VTVL", "TVTK"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["KVQL", "QVQL"],
    # All heavy sequences should end by one of these
    ["ALSA", "CPAS", "MSLG", "SAES", "SASS", "SISS", "TVSS", "VSLG"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"], # Not currently used
),
"Macaca_mulatta": (
    # All light sequences should start by one of these
    ["AAAMA", "AASG", "AFML", "AGQL", "AGQTQ", "AIQ", "AIRI", "AIRL", "ALSQ",
    "ALTQ", "CFVQ", "DAQL", "DELL", "DFVM", "DIAM", "DIDL", "DIDV", "DIEL",
    "DIEM", "DIFL", "DIHL", "DIHM", "DIK", "DILL", "DILM", "DILV", "DIMM",
    "DIQL", "DIQM", "DIQT", "DIQV", "DIQVT", "DIRL", "DIRV", "DITM", "DIVI",
    "DIVL", "DIVM", "DIVV", "DLLM", "DNVL", "DNVM", "DTPMT", "DTVM", "DTVMT",
    "DVAM", "DVLL", "DVLM", "DVLV", "DVM", "DVQI", "DVQM", "DVVL", "DVVLI",
    "DVVM", "DVVV", "EIAM", "EIEL", "EIFL", "EIGL", "EIIL", "EIIM", "EILL",
    "EILM", "EIML", "EIMM", "EIV", "EKVM", "ELAL", "ELAQ", "ELDM", "ELEL",
    "ELQM", "ELTL", "ELTQ", "ELVM", "EMVL", "EMVM", "EPSL", "ESVL", "ETLM",
    "ETTL", "ETTV", "ETVL", "ETVM", "EVLM", "EVVL", "FLLA", "FLSA", "FMLA",
    "FMLI", "FMLS", "FMLT", "GASV", "GLSQ", "GSPG", "GSQP", "GTPG", "HSAL",
    "HSVL", "HSVS", "IAL", "IEM", "IGVTQ", "IHVTQ", "ILFLC", "ILLT", "IQLTQ",
    "IQM", "IRM", "ISVAP", "IVLA", "IVLP", "IVLS", "IVLT", "IVMA", "IVMI",
    "IVMM", "IVMS", "IVMT", "IWMTQ", "KFML", "KPGA", "KPGE", "LIQPA", "LNQP",
    "LQSG", "LSLP", "LSVA", "LSVEL", "LTLTQ", "LTQF", "LTQP", "LTQSP", "LTQT",
    "MGWSCIILFLVATATGS", "MRLP", "MTQ", "NFIL", "NFLL", "NFML", "NFVL",
    "NLTLI", "PASV", "PGTL", "PHSA", "PHSV", "PPSA", "PPSV", "PSAS", "PSLT",
    "PSSL", "PSTLSA", "PSVS", "PVTL", "QAAL", "QAAV", "QAGL", "QAGP", "QAVL",
    "QAVV", "QCVLT", "QFLL", "QFVL", "QFVP", "QIDL", "QIVLI", "QIVLS", "QIVLT",
    "QLLL", "QLML", "QLVL", "QLVV", "QMTQ", "QPAL", "QPAP", "QPAS", "QPAV",
    "QPEV", "QPGL", "QPIL", "QPLL", "QPML", "QPVL", "QSDL", "QSEL", "QSEV",
    "QSFL", "QSGL", "QSIL", "QSLL", "QSML", "QSPL", "QSPP", "QSPS", "QSQL",
    "QSQLT", "QSSL", "QSTL", "QSVL", "QSVM", "QSVP", "QSVV", "QTAV", "QTEV",
    "QTGV", "QTIV", "QTLV", "QTVL", "QTVM", "QTVV", "QVAL", "QVELT", "QVVF",
    "QVVM", "SALAQ", "SALSQ", "SALTQ", "SALTT", "SELT", "SFDL", "SFEL", "SFVL",
    "SHEL", "SLEL", "SLEV", "SLSA", "SLTC", "SMSVSP", "SNEL", "SSDL", "SSEL",
    "SSGL", "SSLS", "SSMS", "STLS", "SVAL", "SVL", "SVLT", "SVPT", "SVSE",
    "SVWT", "SYAL", "SYDL", "SYDV", "SYEL", "SYEV", "SYGL", "SYKL", "SYQL",
    "SYVL", "TLSA", "TLSL", "TLSV", "TQIP", "TQPH", "TQPP", "TQSP", "TVLTQ",
    "TVVTQ", "VLTQ", "VMTQ", "VSLG", "VSVA", "VTQP", "VVM", "VVTQ", "YDL",
    "YDLTQ", "YEL", "YIGL", "YVLAQ", "YVLTQ"],
    # All light sequences should end by one of these
    ["AEIK", "AKRT", "ALEI", "ANRT", "ARVE", "ASLSS", "ATKV", "AVEI", "CNAY",
    "CSY", "DGSLSG", "DIK", "DLEI", "DYYC", "ELDI", "ELEI", "ELEV", "ELQI",
    "ELRRT", "ERK", "EVDI", "EVEI", "EVEV", "EVTV", "FGGG", "FGGGT", "FGGGTK",
    "FGQG", "FGQGTK", "FGQGTKV", "FKRT", "FTVL", "GAGTKL", "GAKV", "GGGGS",
    "GLGT", "GPGT", "GQGT", "GRGT", "GSGT", "GSKV", "GSRL", "GSTL", "GTAV",
    "GTEL", "GTEV", "GTGT", "GTGTK", "GTKL", "GTKV", "GTMV", "GTNL", "GTNV",
    "GTQV", "GTRL", "GTRV", "GTSL", "GTTL", "GTTV", "HIK", "HLDI", "HLEI",
    "HLQI", "HVEI", "HVTV", "IDLK", "IDVK", "IEIK", "IEMK", "IERT", "IEVK",
    "IKGT", "IKRT", "ILDI", "ILEI", "INRT", "ITFG", "ITVL", "IVDI", "IVEI",
    "IVSS", "KIEI", "KIRT", "KKRT", "KLDI", "KLEF", "KLEI", "KLEL", "KLELR",
    "KLEQL", "KLEV", "KLGI", "KLQI", "KLSV", "KLTV", "KLVI", "KMEI", "KVAI",
    "KVDF", "KVDI", "KVDIK", "KVDL", "KVDLK", "KVDV", "KVEI", "KVEL", "KVEV",
    "KVGI", "KVHI", "KVNI", "KVNV", "KVQI", "KVQV", "KVTV", "LAIK", "LAVL",
    "LDFK", "LDIE", "LDIQ", "LDLK", "LDMK", "LDNK", "LDVK", "LEFK", "LEIE",
    "LEIK", "LEIN", "LEIQ", "LEIR", "LELK", "LEMK", "LENK", "LESK", "LETK",
    "LEVE", "LEVH", "LEVK", "LEVQ", "LGIK", "LIVL", "LKIK", "LLQI", "LNIK",
    "LQIK", "LQVK", "LSVL", "LTIK", "LTVL", "LTVV", "LVEI", "LVIK", "LVVK",
    "MDVK", "MEIK", "MEVK", "MFGG", "MKRT", "MLEI", "MTVL", "MVDI", "MVEI",
    "MVEV", "MVTV", "NLDI", "NLEF", "NLEI", "NLEL", "NLEV", "NLGI", "NLQI",
    "NVAI", "NVDI", "NVEI", "NVEV", "NVL", "NVQ", "NVSS", "PEIK", "PGTK",
    "QAGDQT", "QGTR", "QGTRL", "QKRT", "QLDI", "QLEI", "QLEV", "QLTAL", "QPK",
    "QVDI", "QVDV", "QVEI", "QVEL", "QVEV", "QVTV", "QVYE", "RIEI", "RKRT",
    "RLDI", "RLEI", "RLHI", "RLQI", "RMEI", "RTFG", "RVAV", "RVDF", "RVDI", 
    "RVDL", "RVDLK", "RVDV", "RVEI", "RVHI", "RVQI", "RVQV", "RVTV", "SALGPK",
    "SGSG", "SKVE", "SLDI", "SLEI", "SLQI", "SRLTV", "SSTP", "SSYADS", "STFP",
    "SVEI", "SVEL", "SVEV", "TDRS", "TEVE", "TEVT", "TFGG", "TFGPGT", "TFGQ",
    "TFLTV", "TGTKVT", "THVAAN", "TKLE", "TKLTV", "TKRT", "TKVD", "TKVE",
    "TKVN", "TKVT", "TLDI", "TLEI", "TLEL", "TMVE", "TNLE", "TNVE", "TQLE",
    "TQLIIL", "TQVE", "TRLDIN", "TRLE", "TRLTV", "TRRT", "TRVD", "TRVDKK",
    "TRVE", "TRVP", "TSLE", "TTLE", "TTVD", "TTVE", "TVDF", "TVDI", "TVDL",
    "TVDLK", "TVDV", "TVEF", "TVEI", "TVEL", "TVES", "TVET", "TVEV", "TVTV",
    "VAFK", "VAIK", "VALK", "VAMK", "VAVK", "VAVL", "VDAK", "VDFK", "VDIE",
    "VDIQ", "VDIR", "VDLK", "VDMK", "VDNK", "VDRK", "VDSK", "VDTK", "VDVK",
    "VDVL", "VDVQ", "VDVR", "VEAK", "VEFK", "VEIE", "VEIK", "VEIN", "VEIQ",
    "VEIR", "VELK", "VELR", "VEM", "VENK", "VESK", "VETK", "VEVE", "VEVK",
    "VEVKR", "VEVQ", "VEVR", "VFGG", "VFVL", "VGIK", "VGLK", "VGNK", "VGVK",
    "VHFK", "VHLK", "VHMK", "VHVK", "VHVL", "VIIK", "VIVL", "VKIK", "VKRT",
    "VLEI", "VNIE", "VNIK", "VNLK", "VNMK", "VNVK", "VQIK", "VQVK", "VRIK",
    "VRRT", "VSVI", "VSVL", "VTAL", "VTCL", "VTFL", "VTIK", "VTIL", "VTLK",
    "VTLL", "VTVI", "VTVL", "VTVV", "VVIK", "VVLK", "VVVK", "VVVL", "VYIK",
    "WDSRS", "YLDI", "YLEI", "YTFG"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["AASG", "AGLM", "ALQLV", "AQLV", "ASGF", "ATRLE", "AVALA", "AVKLV",
    "AVSLV", "AVTLD", "DIRIA", "DRLF", "DVHL", "DVKL", "DVQV", "EAEL", "EAQL",
    "EAQV", "EEKL", "EEQ", "EGQL", "EIHL", "EIQ", "ELQL", "ELQLQ", "EMHL",
    "EMQV", "EMR", "EQLV", "EQSG", "ESEGD", "ESGG", "ESGP", "EVEL", "EVHL",
    "EVKK", "EVKL", "EVLL", "EVM", "EVPLV", "EVQ", "EVRL", "EVRV", "EVSL",
    "EVTL", "EVYL", "FTFS", "GASV", "GFTF", "GGGL", "GGGV", "GGPS", "GGSL",
    "GGVV", "GQMV", "HLVA", "HVQV", "INLV", "ITLK", "KESG", "KPGA", "KPGE",
    "LAQSG", "LEE", "LEQS", "LKES", "LLEQ", "LLGQ", "LLQS", "LMQS", "LQES",
    "LQLQ", "LQLV", "LQSG", "LRLS", "LSCA", "LTLS", "LVE", "LVKP", "LVQLV",
    "LVQP", "LVQS", "MEF", "MGWSCIILFLVATATGA", "MQL", "PGAS", "PGGS", "QAIL",
    "QAQ", "QARL", "QATL", "QAYL", "QDLL", "QDQL", "QDRL", "QDTL", "QEEL",
    "QEHL", "QEKL", "QELL", "QEQ", "QERL", "QERLVE", "QES", "QEVL", "QFTL",
    "QGKL", "QGQL", "QGQV", "QGTL", "QHSQ", "QIEL", "QIH", "QIIL", "QINL",
    "QIQ", "QISL", "QISLT", "QITFK", "QITL", "QITM", "QITV", "QLEQ", "QLHL",
    "QLLD", "QLLE", "QLLQ", "QLQ", "QLRL", "QLTL", "QLVA", "QLVD", "QLVE",
    "QLVK", "QLVN", "QLVQ", "QMHL", "QMKL", "QMLA", "QMQ", "QMTL", "QPGG",
    "QPQL", "QSGAE", "QSGP", "QSLE", "QSVE", "QTTL", "QVAL", "QVDL", "QVELK",
    "QVELQ", "QVELV", "QVEM", "QVH", "QVIL", "QVKL", "QVLL", "QVNL", "QVPL",
    "QVQ", "QVRL", "QVRV", "QVSL", "QVTF", "QVTL", "QVTM", "QVTV", "QVYL",
    "RAHL", "RCPL", "RITL", "RLFQ", "RLSC", "RLVE", "RVQL", "SCAA", "SETLS",
    "SGAEV", "SGFT", "SGGG", "SGGS", "SLRL", "SLTC", "SQHL", "SQRLV", "SQSVK",
    "SQVDL", "SQYL", "STLK", "TLKE", "TLSL", "VDSG", "VEAG", "VELV", "VHLE",
    "VKKP", "VKLLE", "VKLV", "VKPT", "VLVK", "VNLL", "VQL", "VQPG", "VQSG",
    "VQVL", "VQVV", "VRLS", "VRLV", "VTLK", "VTLRE"],
    # All heavy sequences should end by one of these
    ["AISS", "ALVT", "ATVS", "AVAS", "AVPS", "AVSA", "AVSP", "AVSS", "AVST",
    "AVSW", "AVTS", "AVTV", "CAK", "CVR", "DIWG", "DLWG", "DPWG", "DYWG",
    "FDYW", "FVSS", "GHRLL", "GQGSLV", "GQGT", "GQGTL", "GRGTL",
    "GTLV", "GTPV", "GTRV", "GVAS", "GVSS", "GWFD", "HVSS", "HYGM", "IASS",
    "IISS", "ITAS", "ITVS", "IVFS", "IVPS", "IVSA", "IVSP", "IVSS", "IVST",
    "IVTS", "KGTTV", "KGTTVT", "KVSS", "KVTV", "KVTVS", "LDYW", "LIVS",
    "LSVSS", "LTVS", "LVAV", "LVSA", "LVSS", "LVSV", "LVSVS", "LVSVT",
    "LVTISS", "LVTV", "LVTVS", "LVTVT", "MDVW", "MITV", "MVFV", "MVSS", "MVSV",
    "MVTISS", "MVTV", "MVTVS", "NISS", "NVAS", "NVFS", "NVSA", "NVSS",
    "PGLRLL", "PVSS", "PVTISS", "PVTV", "PVTVS", "PWGQ", "QGSL", "QGTKVIVS",
    "QGTM", "QGTMV", "QGTMVTV", "QVNV", "QVSV", "QVTV", "QVTVS", "RGYS",
    "RSPSP", "RVSS", "RVSV", "RVTISS", "RVTV", "RVTVS", "SKVTV", "SPSPQ",
    "SVAS", "SVFS", "SVPS", "SVSA", "SVSP", "SVSS", "SVST", "SVTS", "SVTVS",
    "TASP", "TFSS", "TFVRV", "TGSS", "TISP", "TISS", "TITS", "TLIS", "TLSS",
    "TLVT", "TPVIV", "TQVIVS", "TSVF", "TSVTV", "TTVTISS", "TTVTV", "TVAA",
    "TVAP", "TVAS", "TVAT", "TVDS", "TVFS", "TVGS", "TVLS", "TVPP", "TVPS",
    "TVPSS", "TVSA", "TVSP", "TVSS", "TVST", "TVTA", "TVTD", "TVTP", "TVTS",
    "TVTV", "TVTVS", "TVVTV", "TVYS", "VAVS", "VAVSS", "VDVSS", "VHISS",
    "VIVAS", "VIVS", "VIVSS", "VIVVL", "VNVS", "VNVSS", "VPVS", "VPVSS",
    "VSDL", "VSISS", "VSVS", "VSVSS", "VSVTA", "VSVTS", "VTASS", "VTVAS",
    "VTVF", "VTVS", "VTVSS", "VTVTS", "VVS", "VVSA", "VVSS", "VVST", "VVTVS",
    "VWG", "WGKGT", "WGPGT", "WGQG", "WGQGTT", "YGDY", "YMDV", "YVSS", "YWGQ",
    "YWYFDLW"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"] # Not currently used
),
"Oryctolagus_cuniculus": (
    # All light sequences should start by one of these
    ["AAVL", "AAVM", "AIDM", "AIKM", "ALVM", "AQGP", "AQVL", "AQVM", "AVVL",
    "DGVM", "DIQM", "DIVL", "DIVM", "DMTQ", "DPML", "DPVL", "DPVM", "DVVM",
    "ELVM", "LTQT", "MTQT", "QAAL", "QFVL", "QPAL", "QPVL", "QPVP", "QVLT",
    "SFVL", "SHEL", "SLVL", "SVVF", "SYEL", "VLTQ", "VVTQ", "VMTQ", "YVMM"],
    # All light sequences should end by one of these
    ["ELEI", "ELQI", "ELVV", "EVVV", "KVDIK", "KVVIK", "LEIK", "LEIL", "LTVL",
    "LTVT", "VAPS", "VAPT", "VDVK", "VEIK", "VEIQ", "VEIR", "VETK", "VGVK",
    "VIVT", "VVVE", "VVVK"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["EDQL", "EEQ", "EESG", "ELVE", "GVSG", "LEQS", "LKES", "LVES", "QELV",
    "QEQL", "QEQQ", "QKQL", "QLME", "QLVE", "QLVK", "QPVE", "QPVK", "QQLK",
    "QQQL", "QQVK", "QSLE", "QSLG", "QSLQ", "QSVE", "QSVK", "QSVR", "QTVK",
    "QVQL", "RLVT", "SGGG", "SVEE", "TGQSL", "TGSL", "VKES"],
    # All heavy sequences should end by one of these
    ["GTLV", "LVTVS", "TISS", "TVSS", "VIVSS"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"], # Not currently used
),
"Rattus_norvegicus": (
    # All light sequences should start by one of these
    ["AIQV", "AIVL", "ASMS", "DAVV", "DIAI", "DIEL", "DIEM", "DIHM", "DIHV",
    "DIKL", "DIKM", "DILI", "DILL", "DILM", "DIML", "DIMM", "DIQI", "DIQL",
    "DIQM", "DIQV", "DIRM", "DIVI", "DIVL", "DIVM", "DNVL", "DTQM", "DTVL",
    "DTVM", "DVMM", "DVMT", "DVQM", "DVVL", "DVVM", "EIIL", "EIVL", "ETTV",
    "ETVL", "ETVM", "GIQM", "LLSA", "LMTQ", "LTQ", "NIQM", "NIVL", "NIVM",
    "NTVM", "NVMM", "QAVL", "QAVV", "QFTL", "QFVL", "QIML", "QITL", "QMTQ",
    "QVVL", "RTVM", "SLSA", "SYEL", "SYTL", "TGET", "YELI", "YTNS"],
    # All light sequences should end by one of these
    ["KLEI", "KLELN", "KQELK", "KVELK", "LEIK", "LELK", "LTVK", "LTVL", "TKLE",
    "VTVL"],
    # Light sequences stops before these
    ["VAAPS"],

    # All heavy sequences should start by one of these
    ["AVQL", "DGAH", "DVQL", "EAHQ", "EAQL", "EGQL", "EIQL", "ELHL", "ELKL",
    "EMKL", "EMQL", "EVEL", "EVKL", "EVKP", "EVLL", "EVPI", "EVQI", "EVQL",
    "EVQV", "EVRL", "GVKL", "GVQL", "HKLV", "HLIL", "HLVR", "HVQL", "IQLV",
    "KAQL", "KVQV", "LVQP", "QAQL", "QIQL", "QLHL", "QLQE", "QMQL", "QVDL",
    "QVHL", "QVKL", "QVNL", "QVQL", "QVQM", "QVQW", "QVRL", "QVSL", "QVTL",
    "RVKL", "RVYL", "SGAE", "SGPG", "VQLQ", "VQLV"],
    # All heavy sequences should end by one of these
    ["LTVS", "LVIVS", "TVSS", "TVTV", "VTASS", "VTVCS", "VTVS"],
    # Heavy sequences stops before these
    ["AST", "AKT", "AKA"], # Not currently used
)
}

class Sequence:
    """ Definition of a Sequence.

    :param header: complete header of the sequence from the file
    :type header: string
    :param seq: sequence
    :type seq: string
    """
    def __init__(self, header, seq):
        self.header = header
        self.seq = seq

    # self representation for print
    def __repr__(self):
        return f"{self.header}\n{self.seq}\n"

    # Equality between two Sequences
    def __eq__(self, other):
        if isinstance(self, other.__class__):
            if self.seq == other.seq:
                return True
        return False
    def __hash__(self):
        return hash(self.__repr__())

def is_seq_present(l_seq, seq):
    """ Check if a string is present in a list of tuple of Sequences

    :param l_seq: list of Sequences
    :type l_seq: list(tuple(str, :py:class:`~core.Sequence`))
    :param seq: the string to test
    :type seq: str

    :return: True is the string is in the list, False otherwise
    :rtype: boolean
    """
    for a_tuple in l_seq:
        if seq in a_tuple[1].seq:
            return True
    return False

def clean_sub_seq(all_data):
    """ Remove sequences included in sequence for the same id

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])

    :return: the input structure cleaned
    :rtype: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    """
    res_data = defaultdict(list)
    # For each id
    for db_id in all_data:
        # Get all vs all
        for i, _ in enumerate(all_data[db_id]):
            good = True
            for j in range(i+1, len(all_data[db_id])):
                # It is in another seq
                if all_data[db_id][i][1].seq in all_data[db_id][j][1].seq:
                    # Not good
                    good = False
                    break
            # Not in any other sequence for this id
            if good:
                # Add it to clean results
                res_data[db_id].append(all_data[db_id][i])
    # Return the clean results
    return res_data

def pairing_igg(all_data, light_chains, heavy_chains, db_name, path):
    """ Pairs IgG together

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param light_chains: terms indicating a light chain
    :type light_chains: list(str)
    :param heavy_chains: terms indicating a heavy chain
    :type heavy_chains: list(str)
    :param db_name: name of the DB
    :type db_name: str
    :param path: path of this script
    :type path: PosixPath

    :return: the paired sequences
    :rtype: dict(str: set(tuple(str, :py:class:`~core.Sequence`)))
    """
    full_res = defaultdict(set)
    # For removing all chain mention in the header
    all_chains = "|".join(light_chains+heavy_chains)

    # Automatically pairing data in most case
    full_res, seq_founded, pairing_fail = automatic_pairing(all_data,
                                                            light_chains,
                                                            heavy_chains,
                                                            all_chains)
    print("Auto pairing")
    auto_pairing = count_pairs(full_res)
    print(auto_pairing)

    # Automatically pairing when only 2 seq for an id and
    # "light" in one header, "heavy" in the other
    full_res_tmp = naive_pairing(all_data,
                                 seq_founded,
                                 light_chains,
                                 heavy_chains)
    # Update full result
    for i in full_res_tmp:
        full_res[i].update((full_res_tmp[i]))
    print("\nNaive pairing")
    nai_pairing = count_pairs(full_res) - auto_pairing
    print(nai_pairing)

    # Automatic pairing using previous manual pairing
    full_res_tmp = previous_pairing(all_data,
                                    seq_founded,
                                    pairing_fail,
                                    db_name,
                                    path)
    # Update full result
    for i in full_res_tmp:
        full_res[i].update((full_res_tmp[i]))
    print("\nPrevious pairing manual")
    prev_man_pairing = count_pairs(full_res) - auto_pairing - nai_pairing
    print(prev_man_pairing)

    # Automatic pairing using previous manual pairing from any parser
    full_res_tmp = global_previous_pairing(all_data,
                                           seq_founded,
                                           pairing_fail,
                                           path)
    # Update full result
    for i in full_res_tmp:
        full_res[i].update((full_res_tmp[i]))
    print("\nPrevious pairing global")
    prev_glob_pairing = count_pairs(full_res) - auto_pairing - nai_pairing - prev_man_pairing
    print(prev_glob_pairing)

    # Propose to manually pairs remaining sequences
    full_res_tmp = manual_pairing(all_data,
                                  seq_founded,
                                  pairing_fail,
                                  db_name,
                                  path)
    # Update full result
    for i in full_res_tmp:
        full_res[i].update((full_res_tmp[i]))
    print("\nManual")
    man_pairing = count_pairs(full_res) - auto_pairing - nai_pairing - prev_man_pairing - prev_glob_pairing
    print(man_pairing)

    return full_res

def automatic_pairing(all_data, light_chains, heavy_chains, all_chains):
    """ Automatically pairing IgG light seq with heavy seq

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param light_chains: terms indicating a light chain
    :type light_chains: list(str)
    :param heavy_chains: terms indicating a heavy chain
    :type heavy_chains: list(str)
    :param all_chains: all chains keywords concatenated with a | between
    :type all_chains: str

    :return: the paired sequences, headers used and problematic headers
    :rtype: dict(str: set(tuple(str, :py:class:`~core.Sequence`))), set(str), set(dtr)
    """
    full_res = defaultdict(set)
    seq_founded = set()
    pairing_fail = set()

    for db_id in all_data:
        tmp_res = set()
        # Avoid duplicate of header, that need to be paired manually
        # Trying all vs all for this id
        for i in all_data[db_id]:
            found = False
            for j in all_data[db_id]:
                # i is the light
                res = auto_pairing_easy(i,
                                        j,
                                        light_chains,
                                        heavy_chains,
                                        all_chains)
                found = res[2]
                if found:
                    tmp_res.add(res[0])
                    seq_founded.update(res[1])
                    break
                # i is the heavy
                res = auto_pairing_easy(j,
                                        i,
                                        light_chains,
                                        heavy_chains,
                                        all_chains)
                found = res[2]
                if found:
                    tmp_res.add(res[0])
                    seq_founded.update(res[1])
                    break

            # Try harder if not found
            # when one header is longer than the other
            for j in all_data[db_id]:
                # Pairing if i is a light and
                # j got all the keywords of i, modulo "light"
                # or j is a heavy and
                # i got all the keywords of j, modulo "heavy"
                if is_pairing_possible(i, j, light_chains) or\
                   is_pairing_possible(j, i, heavy_chains):
                    # Add this pair
                    tmp_res.add(a_pair(i, j, all_chains))
                    # Both sequences were already used
                    seq_founded.add(i[0])
                    seq_founded.add(j[0])
                    break

                # Pairing if i is a heavy and
                # j got all the keywords of i, modulo "heavy"
                # or j is a light and
                # i got all the keywords of j, modulo "light"
                if is_pairing_possible(i, j, heavy_chains) or \
                   is_pairing_possible(j, i, light_chains):
                    # Add this pair
                    tmp_res.add(a_pair(j, i, all_chains))
                    # Both sequences were already used
                    seq_founded.add(j[0])
                    seq_founded.add(i[0])
                    break
        headers = set()
        for i in tmp_res:
            if i[0] in headers:
                # We need to manual paired this one
                pairing_fail.add(i[0].split(" ")[0].strip())
            headers.add(i[0])
        # Add all single res
        for a_res in tmp_res:
            # This is a good one
            if a_res[0].split(" ")[0] not in pairing_fail:
                full_res[db_id].add((a_res[1], a_res[2]))
    return full_res, seq_founded, pairing_fail

def a_pair(light, heavy, all_chains):
    """ Create a proper entries for the full res dict

    :param light: the light sequence of the pair
    :type light: tuple(str, :py:class:`~core.Sequence`)
    :param heavy: the heavy sequence of the pair
    :type heavy: tuple(str, :py:class:`~core.Sequence`)
    :param all_chains: all chains keywords concatenated with a | between
    :type all_chains: str

    :return: the pair
    :rtype: tuple(str, :py:class:`~core.Sequence`, :py:class:`~core.Sequence`)
    """
    # "neutral" header, without chain mention,
    # to avoid duplicates
    header = re.sub(r"\(?({})[ -]?(chain)?\)?".format(all_chains),
                    "", light[0],
                    flags=re.IGNORECASE).strip().lower()
    # Add in a set the neutral header and both seq
    return (header, light[1], heavy[1])

def auto_pairing_easy(light, heavy, light_chains, heavy_chains, all_chains):
    """ Pairing light and heavy when both headers are identical
    modulo "light"/"heavy"

    :param light: the light sequence of the pair
    :type light: tuple(str, :py:class:`~core.Sequence`)
    :param heavy: the heavy sequence of the pair
    :type heavy: tuple(str, :py:class:`~core.Sequence`)
    :param light_chains: Keywords linked to LIGHT sequences
    :type light_chains: list(str)
    :param heavy_chains: Keywords linked to HEAVY sequences
    :type heavy_chains: list(str)
    :param all_chains: all chains keywords concatenated with a | between
    :type all_chains: str

    :return: The pair if founded, the headers and True if founded
    :rtype: tuple(str, :py:class:`~core.Sequence`, :py:class:`~core.Sequence`), set(str), boolean
    """
    seq_founded = set()
    tmp_res = None
    found = False
    # searching "light" in light
    # and "heavy" in heavy
    # and make sure the id are the same modulo "light"/"heavy"
    # and sequences are different (avoiding 2 same sequences in a IgG)
    if any(chain in light[0].lower() for chain in light_chains) and\
       any(chain in heavy[0].lower() for chain in heavy_chains) and\
       re.sub(all_chains, "", light[0].lower()) ==\
       re.sub(all_chains, "", heavy[0].lower()) and\
       light[1] != heavy[1]:
        # Add in a set the neutral header and both seq
        tmp_res = a_pair(light, heavy, all_chains)
        # Both sequences were already used
        seq_founded.add(light[0])
        seq_founded.add(heavy[0])
        # light is paired
        found = True
    return tmp_res, seq_founded, found

def is_pairing_possible(seq_a, seq_b, some_chains):
    """ Return True when headers of seq_a and seq_b are different
    but all keywords of seq_a are in the seq_b, modulo "light"/"heavy"

    :param seq_a: sequence sequence to test
    :type seq_a: tuple(str, :py:class:`~core.Sequence`)
    :param seq_b: second sequence to test
    :type seq_b: tuple(str, :py:class:`~core.Sequence`)
    :param some_chains: chains keywords
    :type some_chains: list(str)

    :return: True if headers are identical modulo "chain"
    :rtype: boolean
    """
    found = False
    # seq_a has "light"/"heavy" and not seq_b
    # use regex to skip "MU in mutation" and
    # keep light in 10-light
    # but will not work with V-LAMBDA for example
    if any(chain in re.split(r"\W", seq_a[0].lower())
           for chain in some_chains) and\
       all(chain not in re.split(r"\W", seq_b[0].lower())
           for chain in some_chains):
        # Look if seq_b is the correct pair
        # all word of seq_a (except "light"/"heavy" should be in seq_b)
        found = True
        for tmp in re.split(r"\W", seq_a[0]):
            if tmp.lower() not in re.split(r"\W", seq_b[0].lower()) and\
               all(tmp.lower() != chain for chain in some_chains):
                found = False
                break
    return found

def naive_pairing(all_data, seq_founded, light_chains, heavy_chains):
    """ Pairing sequences if they are only 2 for an id and
    "heavy" and "light" are in headers

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param seq_founded: all already-paired headers
    :type seq_founded: set(str) (will be modified)
    :param light_chains: Keywords linked to LIGHT sequences
    :type light_chains: list(str)
    :param heavy_chains: Keywords linked to HEAVY sequences
    :type heavy_chains: list(str)

    :return: the newly paired sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`)))
    """
    full_res = defaultdict(list)
    # For each id
    for db_id in all_data:
        tmp_headers = []
        tmp_seq = []
        # Get all not paired seq for this id
        for j in all_data[db_id]:
            if j[0] not in seq_founded:
                # Backup all headers...
                tmp_headers.append(j[0])
                # and all seq
                tmp_seq.append(j[1])
        # We have two headers alone for this id
        if len(tmp_headers) == 2:
            light = None
            heavy = None
            # First try light, because of MU that causes problem
            # Is the first one a light and the second a heavy?
            # Switch regex to include - compare to \W
            if any(chain in re.split(r"[^\w-]", tmp_headers[0].lower())
                   for chain in light_chains) and\
               any(chain in re.split(r"[^\w-]", tmp_headers[1].lower())
                   for chain in heavy_chains):
                light = (tmp_headers[0], tmp_seq[0])
                heavy = (tmp_headers[1], tmp_seq[1])
            # Is the first one a heavy and the second a light?
            # Switch regex to include - compare to \W
            elif any(chain in re.split(r"[^\w-]", tmp_headers[1].lower())
                     for chain in light_chains) and\
                 any(chain in re.split(r"[^\w-]", tmp_headers[0].lower())
                     for chain in heavy_chains):
                light = (tmp_headers[1], tmp_seq[1])
                heavy = (tmp_headers[0], tmp_seq[0])
            # We have a light AND a heavy
            if light and heavy:
                # Add in the result
                full_res[db_id].append((light[1], heavy[1]))
                # Both sequences were already used
                seq_founded.add(light[0])
                seq_founded.add(heavy[0])
    return full_res

def get_previous_pairs(db_name, path):
    """ Get previous manual pairing result from a file

    :param db_name: name of the DB to retrieve
    :type db_name: str
    :param path: path of this script
    :type path: PosixPath

    :return: the newly paired sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`)))
    """
    # Create a dict of previous answers
    answers = defaultdict(set)
    if os.path.isfile(f"{path}/../../manual/{db_name}.fasta"):
        with open(f"{path}/../../manual/{db_name}.fasta", encoding="utf-8") as dbf:
            get_next = False
            header_light = None
            header_heavy = None
            seq_light = None
            seq_heavy = None
            for line in dbf:
                # Always light first
                # First line of a block
                if line.startswith("!"):
                    group = line[1:].strip()

                # Corresponding sequence
                if get_next:
                    # Next line is not a sequence
                    get_next = False
                    # We have a heavy header
                    if header_heavy:
                        # Get the corresponding heavy sequence
                        seq_heavy = line.strip()
                    # We do not have a heavy header
                    else:
                        # Get the corresponding light sequence
                        seq_light = line.strip()

                # A header
                if line.startswith(">"):
                    # Next line is a sequence
                    get_next = True
                    # No light header yet
                    if not header_light:
                        header_light = line.strip()
                    else:
                        header_heavy = line.strip()

                # An empty line:
                if len(line) == 1:
                    # We have a pair
                    if header_heavy:
                        # Add it to answers
                        answers[group].add((seq_light, seq_heavy))
                    # We have a singleton
                    else:
                        # Add it to answers
                        answers[group].add((seq_light, None))
                    # Reset all values
                    header_light = None
                    header_heavy = None
                    seq_light = None
                    seq_heavy = None

    # Return the data
    return answers

def previous_pairing(all_data, seq_founded, pairing_fail, db_name, path):
    """ Use previous manual pairing

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param seq_founded: all already-paired headers
    :type seq_founded: set(str) (will be modified)
    :param pairing_fail: headers that needs to be manual paired
    :type pairing_fail: set(str)
    :param db_name: name of the DB to retrieve
    :type db_name: str
    :param path: path of this script
    :type path: PosixPath

    :return: the newly paired sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`)))
    """
    full_res = defaultdict(list)
    # Get previously manually paired sequences
    answers = get_previous_pairs(db_name, path)
    # Get all ids not paired yet
    for db_id in all_data:
        nb_headers = 0
        # Get all not paired seq for this id
        for j in all_data[db_id]:
            if j[0] not in seq_founded or \
               j[0].split(" ")[0].lower() in pairing_fail:
                # Backup all headers...
                nb_headers += 1
        # We have at least two headers alone for this id
        if nb_headers > 1:
            all_probl = set()
            # Get all problematic sequences for this header
            for seq in all_data[db_id]:
                all_probl.add(seq)
            # It is in the db of answers
            if db_id in answers:
                # For each answers (a tuple)
                for ans_tuple in answers[db_id]:
                    # Reset previous incomplete results
                    light = False
                    heavy = False
                    # For each candidate in data
                    for seq in set(all_probl):
                        # Found the light
                        if seq[1].seq == ans_tuple[0]:
                            # Create the light
                            light = seq[1]
                            # Remove it from problematic one
                            all_probl.remove(seq)
                            # Sequence found
                            seq_founded.add(seq[0])
                        # Found the heavy
                        if seq[1].seq == ans_tuple[1]:
                            # Create the heavy
                            heavy = seq[1]
                            # Remove it from problematic one
                            all_probl.remove(seq)
                            # Sequence found
                            seq_founded.add(seq[0])
                            # We have a pair
                    if light and heavy:
                        # Add all results
                        full_res[db_id].append((light, heavy))
            # No problem for this id anymore
            if len(all_probl) < 2:
                if db_id in pairing_fail:
                    pairing_fail.remove(db_id)
    return full_res

def global_previous_pairing(all_data, seq_founded, pairing_fail, path):
    """ Use previous manual pairing from any parser

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param seq_founded: all already-paired headers
    :type seq_founded: set(str) (will be modified)
    :param pairing_fail: headers that needs to be manual paired
    :type pairing_fail: set(str)
    :param path: path of this script
    :type path: PosixPath

    :return: the newly paired sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`)))
    """
    full_res = defaultdict(list)
    # Get previously manually paired sequences
    answers = {}
    # If the global file exits
    if os.path.isfile(f"{path}/../../manual/all.txt"):
        # Open it
        with open(f"{path}/../../manual/all.txt", encoding="utf-8") as dbf:
            # Get all paired sequences
            for line in dbf:
                # We have a pair
                if line != "\n":
                    # Add light -> heavy
                    answers[line.strip()] = next(dbf).strip()
    # Get all ids not paired yet
    for db_id in all_data:
        nb_headers = 0
        # Get all not paired seq for this id
        for j in all_data[db_id]:
            if j[0] not in seq_founded or \
               j[0].split(" ")[0].lower() in pairing_fail:
                # Backup all headers...
                nb_headers += 1
        # We have at least two headers alone for this id
        if nb_headers > 1:
            all_probl = set()
            # Get all problematic sequences for this header
            for seq in all_data[db_id]:
                all_probl.add(seq)
            # For each possible pair
            for seq_a, seq_b in itertools.permutations(all_data[db_id], 2):
                # Possible heavy sequences linked to this light
                possible_h = set()
                # For each paired sequences
                for tmp_light, tmp_heavy in answers.items():
                    # seq_a is this light
                    if seq_a[1].seq in tmp_light or tmp_light in seq_a[1].seq:
                        # Get corresponding heavy
                        possible_h.add(tmp_heavy)
                # For each possible heavy linked to this light
                for tmp_heavy in possible_h:
                    # seq_b is this heavy
                    if seq_b[1].seq in tmp_heavy or tmp_heavy in seq_b[1].seq:
                        # We have a match
                        light = seq_a[1]
                        heavy = seq_b[1]
                        # Remove them from problematic one
                        if seq_a in all_probl:
                            all_probl.remove(seq_a)
                            # Sequences found
                            seq_founded.add(seq_a[0])
                        if seq_b in all_probl:
                            all_probl.remove(seq_b)
                            # Sequences found
                            seq_founded.add(seq_b[0])
                        # Add in all results
                        full_res[db_id].append((light, heavy))
                        # Stop for this pair
                        break
            # No problem for this id anymore
            if len(all_probl) < 2:
                if db_id in pairing_fail:
                    pairing_fail.remove(db_id)
    # Return newly paired sequences
    return full_res

def nb_ids_to_pair(all_data, seq_founded, pairing_fail):
    """ Count how many entries need to be done manually
    (at least 2 headers for one id)

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param seq_founded: all already-paired headers
    :type seq_founded: set(str) (will be modified)
    :param pairing_fail: headers that needs to be manual paired
    :type pairing_fail: set(str)

    :return: number of entries to do manually
    :rtype: int
    """
    nb_ids = 0
    for db_id in all_data:
        to_take = False
        # For each ids
        for j in all_data[db_id]:
            # This one is not ok
            if j[0] not in seq_founded or \
               j[0].split(" ")[0].lower() in pairing_fail:
               # Second time we saw this id
                if to_take:
                    # Count this id
                    nb_ids += 1
                    # Next id
                    break
                # We already saw it at least one time
                to_take = True
    # Return the number of ids
    return nb_ids

def manual_pairing(all_data, seq_founded, pairing_fail, db_name, path):
    """ Manually (#or not) pairing sequences

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param seq_founded: all already-paired headers
    :type seq_founded: set(str) (will be modified)
    :param pairing_fail: headers that needs to be manual paired
    :type pairing_fail: set(str)
    :param db_name: name of the DB to retrieve
    :type db_name: str
    :param path: path of this script
    :type path: PosixPath

    :return: the newly paired sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`)))
    """
    # Get the number of ids to manually pair
    nb_ids = nb_ids_to_pair(all_data, seq_founded, pairing_fail)
    print(f"There is {nb_ids} entries to manually pair")

    full_res = defaultdict(list)
    cpt = 1
    for db_id in all_data:
        tmp_headers = []
        tmp_seq = []
        # Get all not paired seq for this id
        for j in all_data[db_id]:
            if j[0] not in seq_founded or \
               j[0].split(" ")[0].lower() in pairing_fail:
                # Backup all headers...
                tmp_headers.append(j[0])
                # and all seq
                tmp_seq.append(j[1])
        # We have at least two headers alone for this id
        if len(tmp_headers) > 1:
            all_id = set()
            print(f"\n{cpt}/{nb_ids} Choose merges for {db_id} in this"\
            " format: light1-heavy1 light2-heavy2:")
            cpt += 1
            for val, _ in enumerate(tmp_headers):
                print(f"{val}-{tmp_seq[val].header}\n{tmp_seq[val].seq}")
                all_id.add(val)

            # User input
            to_merge = input("Input pairs, for example 1-0 2-3. Just return "\
                             "if nothing needs to be merged.").strip()

            # Automatic pairing
            #print("Input pairs, for example 1-0 2-3. Just return if nothing "\
            #      "needs to be merged.")
            #to_merge = "" # for automatic only

            # Open corresponding manual file
            with open(f"{path}/../../manual/{db_name}.fasta", "a", encoding="utf-8") as dbf:
                dbf.write(f"!{db_id}\n")
                if to_merge:
                    # Split on spaces
                    to_merge = to_merge.split(" ")
                    # Display chosen merges
                    for a_m in to_merge:
                        print(f"Merging {a_m.replace('-', ' with ')}")
                    # Something to merge
                    if to_merge != "":
                        for a_merge in to_merge:
                            # idx to merge
                            id_l, id_h = a_merge.strip().split("-")
                            # Add in results
                            full_res[db_id].append((tmp_seq[int(id_l)], tmp_seq[int(id_h)]))
                            # Both sequences were already used
                            seq_founded.add(tmp_headers[int(id_l)])
                            seq_founded.add(tmp_headers[int(id_h)])
                            # We saw these ids
                            all_id.remove(int(id_l))
                            all_id.remove(int(id_h))
                            # Write in db file
                            dbf.write(f"{str(tmp_seq[int(id_l)])}")
                            dbf.write(f"{str(tmp_seq[int(id_h)])}\n")
                for i in all_id:
                    dbf.write(f"{str(tmp_seq[int(i)])}\n")
    # Return results
    return full_res

def remove_short_seq(full_res, min_size):
    """ Remove sequences smaller than min_size and return the result. Also give
    the number of deleted elements.

    :param full_res: all paired sequences
    :type full_res: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param min_size: minimum size of a sequence to stay in results
    :type min_size: int

    :return: result with only long sequences and the number of removed elements
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`))), int
    """
    result = defaultdict(list)
    # For each id
    for id_ in full_res:
        # For each pair
        for light, heavy in full_res[id_]:
            # Both sequences of the pair are long enough
            if len(light.seq) >= min_size and len(heavy.seq) >= min_size:
                # Add them in the result
                result[id_].append((light, heavy))
    # Return the result and the number of removed elements
    return result, len(full_res)-len(result)

def correct_labels(full_res, light_starts, heavy_starts):
    """ Correct lights wrongly labeled as heavies and vice-versa.

    :param full_res: all paired sequences
    :type full_res: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param light_starts: all possible begins of light sequences
    :type light_starts: list(str)
    :param heavy_starts: all possible begins of heavy sequences
    :type heavy_starts: list(str)

    :return: result with corrected sequences and the number of corrections
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`))), int
    """
    corrected = 0
    correct_res = defaultdict(list)

    # For each id
    for id_ in full_res:
        # For each pair
        for light, heavy in full_res[id_]:
            # Correct (if needed) the headers
            correct, changed = correct_errors(light,
                                              heavy,
                                              light_starts,
                                              heavy_starts)
            # Headers were corrected
            if changed:
                corrected += 1
            # Add corrected (or not) results
            correct_res[id_].append(correct)
    # Return the corrected result
    return correct_res, corrected

def correct_errors(light, heavy, light_starts, heavy_starts):
    """ Change light to heavy is missed labeled, and vice versa

    :param light: the supposed light sequence
    :type light: :py:class:`~core.Sequence`
    :param heavy: the supposed heavy sequence
    :type heavy: :py:class:`~core.Sequence`
    :param light_starts: all possible begins of light sequences
    :type light_starts: list(str)
    :param heavy_starts: all possible begins of heavy sequences
    :type heavy_starts: list(str)

    :return: the corrected paired and a boolean indicated if it was corrected
    :rtype: tuple(:py:class:`~core.Sequence`, :py:class:`~core.Sequence`), boolean
    """
    exchange = False
    # For each possible light starts
    for i in light_starts:
        # We have a potential error
        if i.upper() in heavy.seq.upper()[:40]:
            is_heavy = False
            # Check if this could still be a heavy
            for j in heavy_starts:
                # We have a potential good
                if j.upper() in heavy.seq.upper()[:40]:
                    # It is a real heavy
                    is_heavy = True
                    break
            # Not a heavy
            if not is_heavy:
                #print("heavy")
                #print(heavy.seq)
                #print()
                # Correct the header
                exchange = True
                break

    # For each possible heavy starts
    for i in heavy_starts:
        # We have a potential error
        if i.upper() in light.seq.upper()[:40]:
            is_light = False
            # Check if this could still be a light
            for j in light_starts:
                # We have a potential good
                if j.upper() in light.seq.upper()[:40]:
                    # It is a real light
                    is_light = True
                    break
            # Not a light
            if not is_light:
                #print("light")
                #print(light.seq)
                #print()
                # Correct the header
                exchange = True
                break
    # We have to switch light and heavy
    if exchange:
        return (heavy, light), exchange
    # It was a good pair
    return (light, heavy), exchange

def get_proper_igg(full_res, min_size, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before):
    """ Get IgG of a certain size and with correct starts/ends

    :param full_res: all paired sequences
    :type full_res: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param min_size: minimal size of an IgG
    :type min_size: int
    :param max_size: maximum size of an IgG
    :type max_size: int
    :param light_starts: all possible begins of light sequences
    :type light_starts: list(str)
    :param light_ends: all possible ends of light sequences
    :type light_ends: list(str)
    :param light_ends_before: patterns that are after the end of a light sequence
    :type light_ends_before: list(str)
    :param heavy_starts: all possible begins of heavy sequences
    :type heavy_starts: list(str)
    :param heavy_ends: all possible ends of heavy sequences
    :type heavy_ends: list(str)
    :param heavy_ends_before: patterns that are after the end of a heavy sequence
    :type heavy_ends_before: list(str)

    :return: result with corrected sequences, the number of good and bad sequences
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`))), int, int
    """
    validated = 0
    not_validated = 0
    correct_res = defaultdict(list)
    bad_l_starts = set()
    bad_l_ends = set()
    bad_h_starts = set()
    bad_h_ends = set()
    # For each id
    for id_ in full_res:
        # For each pair
        for light, heavy in full_res[id_]:
            # Correct (if needed) the sequences
            correct, valid, reason = make_smaller_igg(light,
                                                      heavy,
                                                      max_size,
                                                      light_starts,
                                                      light_ends,
                                                      light_ends_before,
                                                      heavy_starts,
                                                      heavy_ends,
                                                      heavy_ends_before)
            # It is (now?) a good sequence
            if valid:
                validated += 1
                # Add it in results
                correct_res[id_].append(correct)
            # Not a good sequence
            else:
                not_validated += 1
                # Sequences are long enough?
                if len(light.seq) > min_size and len(heavy.seq) > min_size:
                    if reason == "lstart":
                        bad_l_starts.add(light)
                    elif reason == "lend":
                        bad_l_ends.add(light)
                    elif reason == "hstart":
                        bad_h_starts.add(heavy)
                    elif reason == "hend":
                        bad_h_ends.add(heavy)
    """
    # Display unknown starts/end
    if bad_l_starts:
        print("\nUnknown light starts:")
        for seq in sorted(list(bad_l_starts), key=lambda x: x.seq):
            #print(seq.header)
            print(seq.seq)
    if bad_l_ends:
        print("\nUnknown light ends:")
        for seq in bad_l_ends:
            #print(seq.header)
            print(seq.seq)
    if bad_h_starts:
        print("\nUnknown heavy starts:")
        for seq in sorted(list(bad_h_starts), key=lambda x: x.seq):
            #print(seq.header)
            print(seq.seq)
    if bad_h_ends:
        print("\nUnknown heavy ends:")
        for seq in bad_h_ends:
            #print(seq.header)
            print(seq.seq)
    print()
    """
    # Returns results
    return correct_res, validated, not_validated

def make_smaller_igg(light, heavy, max_size, light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before):
    """ Resize long sequences to ensure starts/ends are coherent
    and discard sequences too long

    :param light: the supposed light sequence
    :type light: :py:class:`~core.Sequence`
    :param heavy: the supposed heavy sequence
    :type heavy: :py:class:`~core.Sequence`
    :param max_size: maximum size of an IgG
    :type max_size: int
    :param light_starts: all possible begins of light sequences
    :type light_starts: list(str)
    :param light_ends: all possible ends of light sequences
    :type light_ends: list(str)
    :param light_ends_before: patterns that are after the end of a light sequence
    :type light_ends_before: list(str)
    :param heavy_starts: all possible begins of heavy sequences
    :type heavy_starts: list(str)
    :param heavy_ends: all possible ends of heavy sequences
    :type heavy_ends: list(str)
    :param heavy_ends_before: patterns that are after the end of a heavy sequence
    :type heavy_ends_before: list(str)

    :return: result with corrected sequences, and if the sequences are good
    :rtype: dict(str: list(tuple(str, :py:class:`~core.Sequence`))), boolean
    """
    # Why this sequence is not good?
    reason = None
    valid = False
    # Light starts
    possible_starts = []
    for i in light_starts:
        pos = light.seq.upper().find(i.upper())
        if pos > -1:
            possible_starts.append((i, pos))
    # We have at least a match
    if len(possible_starts) > 0:
        possible_starts.sort(key=lambda start: start[1])
        # Resize the sequence
        light.seq = light.seq[possible_starts[0][1]:]
        valid = True

    # We have a good light begging
    if valid:
        valid = False
        # Light end
        possible_ends = []
        for i in light_ends:
            pos = light.seq.upper().find(i.upper())
            if pos > -1:
                possible_ends.append((i, pos))
        # We have at least a match
        if len(possible_ends) > 0:
            possible_ends.sort(key=lambda start: len(start[0])+start[1],
                               reverse=True)
            to_take = None
            # Find the longest one that works
            for pos, i in enumerate(possible_ends):
                if len(i[0]) + i[1] < max_size:
                    to_take = pos
                    break
            # We have a good end (not too long)
            if to_take is not None:
                # Resize the sequence
                light.seq = light.seq[:possible_ends[to_take][1]+\
                                       len(possible_ends[to_take][0])]
                valid = True

        # No clear light end
        if not valid:
            # Light end BEFORE
            possible_ends = []
            for i in light_ends_before:
                pos = light.seq.upper().find(i.upper())
                if pos > -1:
                    possible_ends.append((i, pos))
            # We have at least a match
            if len(possible_ends) > 0:
                possible_ends.sort(key=lambda start: len(start[0])+start[1],
                                   reverse=True)
                to_take = None
                # Find the longest one that works
                for pos, i in enumerate(possible_ends):
                    if i[1] < max_size:
                        to_take = pos
                        break
                # We have a good end (not too long)
                if to_take is not None:
                    # Resize the sequence
                    light.seq = light.seq[:possible_ends[to_take][1]]
                    valid = True
    # No good light start
    else:
        reason = "lstart"
    # We have a good light begging and ending
    if valid:
        valid = False
        # Heavy starts
        possible_starts = []
        for i in heavy_starts:
            pos = heavy.seq.upper().find(i.upper())
            if pos > -1:
                possible_starts.append((i, pos))
        # We have at least a match
        if len(possible_starts) > 0:
            possible_starts.sort(key=lambda start: start[1])
            # Resize the sequence
            heavy.seq = heavy.seq[possible_starts[0][1]:]
            valid = True
    # No good light end
    else:
        if not reason:
            reason = "lend"
    # We have a good heavy begging
    if valid:
        valid = False
        # heavy end
        possible_ends = []
        for i in heavy_ends:
            pos = heavy.seq.upper().find(i.upper())
            if pos > -1:
                possible_ends.append((i, pos))
        # We have at least a match
        if len(possible_ends) > 0:
            possible_ends.sort(key=lambda start: len(start[0])+start[1],
                               reverse=True)
            to_take = None
            # Find the longest one that works
            for pos, i in enumerate(possible_ends):
                if len(i[0]) + i[1] < max_size:
                    to_take = pos
                    break
            # We have a good end (not too long)
            if to_take is not None:
                # Resize the sequence
                heavy.seq = heavy.seq[:possible_ends[to_take][1]+\
                                       len(possible_ends[to_take][0])]
                valid = True
    # No good heavy start
    else:
        if not reason:
            reason = "hstart"

        # No clear heavy end
        #if not valid:
        #    # Light end BEFORE
        #    possible_ends = []
        #    for i in heavy_ends_before:
        #        pos = heavy.seq.upper().find(i.upper())
        #        if pos > -1:
        #            possible_ends.append((i, pos))
        #    # We have at least a match
        #    if len(possible_ends) > 0:
        #        possible_ends.sort(key=lambda start: len(start[0])+start[1],
        #                           reverse=True)
        #        to_take = None
        #        # Find the longest one that works
        #        for pos, i in enumerate(possible_ends):
        #            if i[1] < max_size:
        #                to_take = pos
        #                break
        #        # We have a good end (not too long)
        #        if to_take is not None:
        #            # Resize the sequence
        #            heavy.seq = heavy.seq[:possible_ends[to_take][1]]
        #            valid = True    #     valid = True
    # No good heavy end
    if not valid:
        if not reason:
            reason = "hend"

    # Return the sequence, potentially edited, and if they are to take or not
    return (light, heavy), valid, reason

def count_pairs(full_res):
    """ Count the number of paired sequences in full_res

    :param full_res: all paired sequences
    :type full_res: dict(str: [tuple(str, :py:class:`~core.Sequence`)])

    :return: The number of paired sequences
    :rtype: int
    """
    total_igg = 0
    for id_ in full_res:
        for _ in full_res[id_]:
            total_igg += 1
    return total_igg

def launch_igblast(chain, org_name, path, db_name):
    """ Launch IgBlast
    :param chain: chain to use, either LIGHT or HEAVY
    :type chain: str
    :param org_name: organism analyzed
    :type org_name: str
    :param path: path of this script
    :type path: PosixPath
    :param db_name: name of the database
    :type db_name: str
    """
    # Get IgBlast org_name
    if org_name == "Homo_sapiens":
        igb_org_name = "human"
    elif org_name == "Mus_musculus":
        igb_org_name = "mouse"
    elif org_name == "Bos_taurus":
        igb_org_name = "bovine"
    elif org_name == "Macaca_mulatta":
        igb_org_name = "rhesus_monkey"
    elif org_name == "Oryctolagus_cuniculus":
        igb_org_name = "rabbit"
    elif org_name == "Rattus_norvegicus":
        igb_org_name = "rat"
    else:
        print("Error in core.py regarding IgBlast organism name")

    # Ids of pairs with no IG*V group (mostly wrong sequences)
    no_group_ids = set()

    # Launch IgBlast
    try:
        result = subprocess.run(f"igblastp -num_threads 15 -organism {igb_org_name} -query {path}/../../CLEAN/{chain}-{org_name}-{db_name}.fasta -germline_db_V {path}/blastDB/{chain}_{igb_org_name}.fasta",
                                 shell=True,
                                 check=True,
                                 universal_newlines=True,
                                 capture_output=True)
    except subprocess.CalledProcessError as err:
        print(f"command '{err.cmd}' return with error (code "\
              f"{err.returncode}): {err.stderr}", file=sys.stderr)
        sys.exit(1)
    # Link a unique id with a group
    all_igg_groups = {}
    # For each line generated by IgBlast
    res = iter(result.stdout.splitlines())
    for line in res:
        # Counter
        if line.startswith("Query="):
            header = int(line.split('Query= ')[1])
            # Skip 5 more lines
            next(res)
            next(res)
            next(res)
            next(res)
            next(res)
            # Get the best hit results
            group = next(res)
            # We have a best hit
            if group:
                # Two-digit group
                if group[5].isdigit():
                    group = group[:6]
                # One digit group
                else:
                    group = group[:5]
                # Give this group to this unique id
                all_igg_groups[header] = group

            # No best hit
            else:
                no_group_ids.add(header)
    return no_group_ids, all_igg_groups

def get_v_group(full_res, org_name, db_name, path):
    """ Use IgBlast to determine the correct IG*V group of each sequence.
    It uses references from https://www.imgt.org/vquest/refseqh.html that are
    then parsed with 'edit_imgt_file.pl' before makeblastdb. IGKV and IGLV are
    merged into LIGHT.fasta

    :param full_res: all paired sequences
    :type full_res: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param org_name: organism analyzed
    :type org_name: str
    :param db_name: name of the database
    :type db_name: str
    :param path: path of this script
    :type path: PosixPath
    """
    # Dict of pairs with a unique (digit) id
    res_with_unique_id = {}

    # Set of pairs with no IG*V group (mostly wrong sequences)
    no_group = set()

    # All possible groups for LIGHT
    all_groups_l = set()
    # All possible groups for HEAVY
    all_groups_h = set()

    # Unique id for each pair
    cpt_id = 0
    """ Run IgBlast to determinate the IG*V group of each IgG """
    with open(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta", "w", encoding="utf-8") as light_f,\
         open(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta", "w", encoding="utf-8") as heavy_f:
        # For each id
        for an_id in full_res:
            # For each pair
            for pair in full_res[an_id]:
                # Write both sequences
                light_f.write(f">{cpt_id}\n{pair[0].seq}\n")
                heavy_f.write(f">{cpt_id}\n{pair[1].seq}\n")
                res_with_unique_id[cpt_id] = pair
                cpt_id += 1

    no_group_ids_l, all_igg_groups_l = launch_igblast("LIGHT", org_name, path, db_name)
    no_group_ids_h, all_igg_groups_h = launch_igblast("HEAVY", org_name, path, db_name)
    # Get the difference between both results
    no_group_ids = all_igg_groups_l.keys() ^ all_igg_groups_h.keys()
    # Get all ids of pairs with no groups
    no_group_ids.update(no_group_ids_l.union(no_group_ids_h))
    # For each ids with no group
    for ids in no_group_ids:
        # Get pairs with no groups
        no_group.add(res_with_unique_id[ids])
        # Remove them from all_igg_groups
        if ids in all_igg_groups_l:
            del all_igg_groups_l[ids]
        if ids in all_igg_groups_h:
            del all_igg_groups_h[ids]
    # Get all pairs/groups
    all_igg_groups = {}
    for header, group_l in all_igg_groups_l.items():
        group_h = all_igg_groups_h[header]
        all_igg_groups[header] = (group_l, group_h)
        # Add this group to all possible groups
        all_groups_l.add(group_l)
        all_groups_h.add(group_h)

    # Return groups
    return res_with_unique_id, no_group, all_igg_groups, all_groups_l, all_groups_h

def print_files(res_with_unique_id, no_group, all_igg_groups, all_groups_l, light_file, heavy_file, path, db_name):
    """ Print sequences in correct files 

    :param res_with_unique_id: all paired sequences linked to a unique id
    :type res_with_unique_id: dict(int: [tuple(str, :py:class:`~core.Sequence`)])
    :param no_group: all paired sequences with no IGHV group identified
    :type no_group: set()
    :param all_igg_groups: IG*V group for each IgG
    :type all_igg_groups: dict(set)
    :param all_groups_l: all possible IGK/LV groups for this database
    :type all_groups_l: set()
    :param light_file: file where to write light sequences
    :type light_file: str
    :param heavy_file: file where to write heavy sequences
    :type heavy_file: str
    :param path: path of this script
    :type path: PosixPath
    :param db_name: name of the database
    :type db_name: str
    """
    # Get previously manually paired sequences
    all_paired = set()
    # If the global file exits
    if os.path.isfile(f"{path}/../../manual/all.txt"):
        # Open it
        with open(f"{path}/../../manual/all.txt", encoding="utf-8") as dbf:
            # Get all paired sequences
            for line in dbf:
                # We have a pair
                if line != "\n":
                    # Add light -> heavy
                    all_paired.add((line.strip(), next(dbf).strip()))
    # Open all files
    all_opened_files = {}
    # Separate output files by IGK/LV groups
    for group in all_groups_l:
        light_name = str(light_file).replace("/CLEAN/", f"/CLEAN/{group}_")
        heavy_name = str(heavy_file).replace("/CLEAN/", f"/CLEAN/{group}_")
        light_output = open(light_name, "w", encoding="UTF-8")
        heavy_output = open(heavy_name, "w", encoding="UTF-8")
        all_opened_files[group] = (light_output, heavy_output)

    # For each pair
    for an_id, pair in res_with_unique_id.items():
        # Get its group, if it had one
        if an_id in all_igg_groups:
            group_l, group_h = all_igg_groups[an_id]
            # Write both sequences
            all_opened_files[group_l][0].write(f"{pair[0].header};{group_l}\n{pair[0].seq}\n")
            all_opened_files[group_l][1].write(f"{pair[1].header};{group_h}\n{pair[1].seq}\n")
            # Add in all paired
            all_paired.add((pair[0].seq, pair[1].seq))

    # Close all files
    for group in all_opened_files:
        all_opened_files[group][0].close()
        all_opened_files[group][1].close()
    # Re-write all.txt file for this db
    with open(f"{path}/../../manual/all-{db_name}.txt", "w", encoding="utf-8") as allf:
        # For each id
        for light, heavy in all_paired:
            # Write in all.txt file
            allf.write(f"{light}\n")
            allf.write(f"{heavy}\n\n")
    print(f"{len(no_group)} IgG removed (no coherent group)")
    if len(no_group) > 0:
        print(no_group)

def get_igg(all_data, light_chains, heavy_chains, light_file, heavy_file, min_size, max_size, org_name):
    """ Format data from raw sequences into fasta files of paired sequences

    :param all_data: all input sequences
    :type all_data: dict(str: [tuple(str, :py:class:`~core.Sequence`)])
    :param light_chains: Keywords linked to LIGHT sequences
    :type light_chains: list(str)
    :param heavy_chains: Keywords linked to HEAVY sequences
    :type heavy_chains: list(str)
    :param light_file: file where to write light sequences
    :type light_file: str
    :param heavy_file: file where to write heavy sequences
    :type heavy_file: str
    :param min_size: minimum size of a sequence to stay in results
    :type min_size: int
    :param max_size: maximum size of a sequence to stay in results
    :type max_size: int
    :param org_name: organism analyzed
    :type org_name: str
    """

    # Current path
    path = pathlib.Path(__file__).parent.resolve()

    # Name of the DB
    db_name = pathlib.PurePath(light_file).name.split("_")[0]

    # Remove sub-sequences identical inside one id
    all_data = clean_sub_seq(all_data)

    # Pairing IgG
    full_res = pairing_igg(all_data, light_chains, heavy_chains, db_name, path)

    # Remove short sequences
    full_res, nb_too_small = remove_short_seq(full_res, min_size)
    print(f"\n{nb_too_small} sequences too small (<{min_size}aa) were removed")

    # Get correct sizes, starts and ends
    light_starts, light_ends, light_ends_before, heavy_starts, heavy_ends, heavy_ends_before = STARTS_ENDS[org_name]

    # Correct lights wrongly labeled as heavy and vice-versa
    corrected_res, nb_corrected = correct_labels(full_res,
                                                 light_starts,
                                                 heavy_starts)
    print(f"{nb_corrected} IgG headers corrected (inversion heavy/light)")

    # Get proper IgG (correcting length using starts/ends)
    final_res, validated, not_validated = get_proper_igg(corrected_res,
                                                         min_size,
                                                         max_size,
                                                         light_starts,
                                                         light_ends,
                                                         light_ends_before,
                                                         heavy_starts,
                                                         heavy_ends,
                                                         heavy_ends_before)
    print(f"{validated} paired IgG identified and {not_validated} "\
           "IgG unusable (no starts/ends founded in light or heavy)")

    # Get only the seq not too long/short
    final_res, _ = remove_short_seq(final_res, min_size)
    print(f"{count_pairs(final_res)} paired IgG after final size selection")

    # Separate between all IGHV groups
    res_with_unique_id, no_group, all_igg_groups, all_groups_l, all_groups_h = get_v_group(final_res, org_name, db_name, path)
    # Output in files
    print_files(res_with_unique_id, no_group, all_igg_groups, all_groups_l, light_file, heavy_file, path, db_name)
    # Remove the temporary files
    os.remove(f"{path}/../../CLEAN/LIGHT-{org_name}-{db_name}.fasta")
    os.remove(f"{path}/../../CLEAN/HEAVY-{org_name}-{db_name}.fasta")
