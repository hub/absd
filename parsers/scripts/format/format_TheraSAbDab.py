# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a csv file from Thera-SAbDab """
from collections import defaultdict
import sys
import core
import pathlib

def get_data(input_file, all_chains, organisms, to_remove=None):
    """ Get clean data from csv file """
    all_data = defaultdict(list)
    with open(input_file, encoding="UTF-8") as csv_file:
        # Skip header
        next(csv_file)
        # Each line is one or two IgG
        for line in csv_file:
            tmp = line.strip().split(",")
            clean_id = tmp[0]
            seq_l1 = tmp[7]
            seq_h1 = tmp[6]
            seq_l2 = tmp[9]
            seq_h2 = tmp[8]
            header_l = f"{clean_id} INN light"
            header_h = f"{clean_id} INN heavy"
            fhead_l = f">{clean_id}_INN"
            fhead_h = f">{clean_id}_INN"
            all_data[clean_id].append((header_l, core.Sequence(f"{' '.join(' '.join(fhead_l.replace('|||', ' ').split()).replace(';', ' ').split())} light;{clean_id}", seq_l1)))
            all_data[clean_id].append((header_h, core.Sequence(f"{' '.join(' '.join(fhead_h.replace('|||', ' ').split()).replace(';', ' ').split())} heavy;{clean_id}", seq_h1)))
            if seq_l2 != "na" and seq_h2 != "na":
                all_data[clean_id+"2"].append((header_l, core.Sequence(f"{' '.join(' '.join(fhead_l.replace('|||', ' ').split()).replace(';', ' ').split())} light;{clean_id}", seq_l2)))
                all_data[clean_id+"2"].append((header_h, core.Sequence(f"{' '.join(' '.join(fhead_h.replace('|||', ' ').split()).replace(';', ' ').split())} heavy;{clean_id}", seq_h2)))

    return all_data

def main():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/TheraSAbDab_SeqStruc_OnlineDownload.csv"
    # Output files
    output_light = f"{path}/../../CLEAN/Thera-SAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/Thera-SAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("Thera-SAbDab-human")
    main()
    sys.exit(0)
