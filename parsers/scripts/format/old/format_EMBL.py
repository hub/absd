# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a fasta file from IMGT
    They might be duplicate sequences, but ids are unique """
from collections import defaultdict
import re
import sys
from context import prepare_data
import core

class Cds:
    """ Definition of a CDS.

    :param ranges: ranges of position in the sequence
    :param seq: protein sequence
    :type ranges: list(ranges())
    :type seq: string
    """
    def __init__(self, ranges, seq):
        self.ranges = ranges
        self.seq = seq


    # self representation for print
    def __repr__(self):
        return "{}\n{}\n".format(self.ranges, self.seq)

    def __hash__(self):
        return hash(self.__repr__())

    def has_organism(self, organisms, all_organisms):
        """ Check if all its ranges are covered by the wanted organism
            all_organisms are the possibilities for this CDS """
        # Check each range
        for a_range in self.ranges:
            # This range is not in any organism
            found = False
            # For all available organisms
            for org in all_organisms:
                # This organism is good
                if any(orga in org[1].lower() for orga in organisms):
                    # This CDS is in a range of this organism
                    for o_range in org[0]:
                        if a_range[0] in o_range and\
                           a_range[-1] in o_range:
                            found = True
                            break
                # We already have a good organism for this range
                if found:
                    break
            # At least one range not in any organism
            if not found:
                return False
        # We have a good organism for this CDS
        return True

    def has_in_range(self, content):
        """ Check if the content is in range of this CDS """
        # Get all ranges of content
        for o_range in content[0]:
            # For all ranges of this CDS
            for a_range in self.ranges:
                # At least a portion of this content in the CDS
                if o_range[0] in a_range or o_range[-1] in a_range:
                    return True
        # Completely not in this CDS
        return False


def get_starts_ends(tag_line):
    """ Extract the starts/ends from a tag line
        Return a list of range() """
    all_ranges = []

    # Get all intervals
    all_inter = tag_line.split(",")
    # Clean intervals
    clean_inter = []
    for i in all_inter:
        # Remove useless stuff
        tmp = re.sub(r"order|join|complement|[<>\(\)]|.*\:", "", i)
        # Replace .. and ^ to .
        tmp = tmp.replace("..", ".")
        tmp = tmp.replace("^", ".")
        clean_inter.append(tmp.split("."))
    # Create starts/ends
    for i in clean_inter:
        # A single digit 'interval'
        if len(i) == 1:
            all_ranges.append(range(int(i[0]), int(i[0])+1))
        # A normal interval
        elif len(i) == 2:
            all_ranges.append(range(int(i[0]), int(i[1])+1))
        # An error
        else:
            print("Error in tag interval")
    return tuple(all_ranges)

def parse_line(c_line, positions, all_content):
    """ TODO modify all_content """
    # Get the content
    content = c_line.split("\"")[1].strip()
    # Next line is also the same content
    next_line = True
    # In a single line
    if c_line.endswith("\""):
        # Add the content to the set of content, with positions
        all_content.append((positions, content))
        # Next line is not the same content
        next_line = False
    return content, next_line

def parse_multi_line(c_line, positions, all_content, cur_content):
    """ TODO, modify all_content """
    # Add this line in the current content
    content = cur_content + " " + c_line
    # Next line is also the same content
    next_line = True
    # Last line of content
    if c_line.endswith("\""):
        # Add the content, whiteout trailing " to the set of content, with positions
        all_content.append((positions, content[:-1].strip()))
        # Next line is not the same content
        next_line = False
    return content, next_line


def get_data(input_file, input_ids, all_chains, organisms, to_remove):
    """ Get clean data from IMGT file """
    # Dict of list for the main results
    # key -> imgtid_description
    total = 222964
    all_data = defaultdict(list)
    with open(input_file, encoding="UTF-8") as embl_file:
        next_line_or = False
        next_line_fu = False
        next_line_pr = False
        next_line_ge = False
        next_line_ot = False
        next_line_no = False
        next_line_pi = False
        next_line_tr = False

        next_line_tag = False

        all_organisms = []
        all_functions = []
        all_products = []
        all_genes = []
        all_others = []
        all_notes = []
        all_prot_ids = []
        all_trans = []

        pat_id = ""
        seq_id = ""
        pubmed_id = ""

        for line in embl_file:
            #all_.append(line.strip())
            # This entry is from a patent, use this as unique id
            if line.startswith("RL   Patent number "):
                pat_id = line.split("-")[0].split(" ")[-1]
            # Hope for a shared ID
            elif line.startswith("ID"):
                seq_id = line.strip()[5:].split(";")[0]
            elif line.startswith("RX   PUBMED"):
                pubmed_id = line.strip().split(" ")[-1][:-1]
            # Only FT lines matter
            elif line.startswith("FT"):
                # Clean line, without FT
                c_line = line.strip()[5:]

                # We have a line with a tag
                # Might be in several line, ending by ','
                if not c_line.startswith(" ") or next_line_tag:
                    # The first line of this tag?
                    if not c_line.startswith(" "):
                        tag_line = c_line[16:]
                    else:
                        tag_line += c_line.strip()
                    # Next line also in this tag line?
                    if c_line.endswith(","):
                        next_line_tag = True
                    # We are ok for this tag
                    else:
                        # End of this tag line
                        next_line_tag = False
                        # Get the starting/ending positions for this tag
                        # as a tuple of tuple...
                        positions = get_starts_ends(tag_line)

                # A line with potentially important info
                # Each following lines have a cur_starts, cur_ends
                else:
                    # Clean the starting spaces
                    c_line = c_line.strip()

                    # Get organisms in all_organisms set
                    if "/organism=" in c_line:
                        organism, next_line_or = parse_line(c_line,
                                                            positions,
                                                            all_organisms)
                    # This organism is across several lines
                    elif next_line_or:
                        organism, next_line_or = parse_multi_line(c_line,
                                                                  positions,
                                                                  all_organisms,
                                                                  organism)
                    # Get functions in all_functions set
                    elif "/function=" in c_line:
                        function, next_line_fu = parse_line(c_line,
                                                            positions,
                                                            all_functions)
                    # This function is across several lines
                    elif next_line_fu:
                        function, next_line_fu = parse_multi_line(c_line,
                                                                  positions,
                                                                  all_functions,
                                                                  function)
                    # Get products in all_products set
                    elif "/product=" in c_line:
                        product, next_line_pr = parse_line(c_line,
                                                           positions,
                                                           all_products)
                    # This product is across several lines
                    elif next_line_pr:
                        product, next_line_pr = parse_multi_line(c_line,
                                                                 positions,
                                                                 all_products,
                                                                 product)
                    # Get genes in all_genes set
                    elif "/gene=" in c_line:
                        gene, next_line_ge = parse_line(c_line,
                                                        positions,
                                                        all_genes)
                    # This gene is across several lines
                    elif next_line_ge:
                        gene, next_line_ge = parse_multi_line(c_line,
                                                              positions,
                                                              all_genes,
                                                              gene)
                    # Get others in all_others set
                    elif "/other=" in c_line:
                        other, next_line_ot = parse_line(c_line,
                                                         positions,
                                                         all_others)
                    # This other is across several lines
                    elif next_line_ot:
                        other, next_line_ot = parse_multi_line(c_line,
                                                               positions,
                                                               all_others,
                                                               other)
                    # Get notes in all_notes set
                    elif "/note=" in c_line:
                        note, next_line_no = parse_line(c_line,
                                                        positions,
                                                        all_notes)
                    # This note is across several lines
                    elif next_line_no:
                        note, next_line_no = parse_multi_line(c_line,
                                                              positions,
                                                              all_notes,
                                                               note)
                    # Get prot_ids in all_prot_ids set
                    elif "/protein_id=" in c_line:
                        prot_id, next_line_pi = parse_line(c_line,
                                                           positions,
                                                           all_prot_ids)
                    # This prot_id is across several lines
                    elif next_line_pi:
                        prot_id, next_line_pi = parse_multi_line(c_line,
                                                                 positions,
                                                                 all_prot_ids,
                                                                 prot_id)
                    # Get transs in all_trans set
                    elif "/translation=" in c_line:
                        trans, next_line_tr = parse_line(c_line,
                                                         positions,
                                                         all_trans)
                    # This trans is across several lines
                    elif next_line_tr:
                        trans, next_line_tr = parse_multi_line(c_line,
                                                               positions,
                                                               all_trans,
                                                               trans)

            # End of this entry
            elif line.startswith("//"):
                # Counter
                total-=1
                if total%1000 == 0:
                    print(total)
                # Use translation to find relevant CDS for this entry
                for i in all_trans:
                    # Remove short sequences
                    if len(i[1]) > 70:
                        # Construct a Cds
                        # The sequence should not contain spaces nor .
                        seq = i[1].replace(".", "").replace(" ", "")
                        tmp_cds = Cds(i[0], seq)
                        # We have a good organism for this CDS
                        if tmp_cds.has_organism(organisms, all_organisms):
                            # Merge all infos
                            all_info = ""
                            # using dict to get an ordered set
                            for content in list(dict.fromkeys(all_functions)):
                                if tmp_cds.has_in_range(content):
                                    all_info += content[1] + " "
                            for content in list(dict.fromkeys(all_products)):
                                if tmp_cds.has_in_range(content):
                                    all_info += content[1] + " "
                            for content in list(dict.fromkeys(all_genes)):
                                if tmp_cds.has_in_range(content):
                                    all_info += content[1] + " "
                            for content in list(dict.fromkeys(all_others)):
                                if tmp_cds.has_in_range(content):
                                    all_info += content[1] + " "
                            for content in list(dict.fromkeys(all_notes)):
                                if tmp_cds.has_in_range(content):
                                    all_info += content[1] + " "
                            # Don't add the protein_id, often uniq per protein
                            header = all_info
                            for content in list(dict.fromkeys(all_prot_ids)):
                                if tmp_cds.has_in_range(content):
                                    header += content[1] + " "
                            # Remove double spaces
                            all_info = re.sub(" +", " ", all_info[:-1])
                            # Remove double spaces
                            header = re.sub(" +", " ", header[:-1])
                            # What id to use to have something
                            # in common between light and heavy?
                            if pat_id:
                                embl_id = pat_id
                            elif pubmed_id:
                                embl_id = pubmed_id
                            else:
                                embl_id = seq_id
                            # The seq is not already there and
                            # does not contain #,
                            # does contain a chain,
                            # does not contain a wrong keyword
                            if "#" not in seq and \
                               any(chain in all_info.lower() for chain in all_chains) and\
                               not any(to_rm in all_info.lower() for to_rm in to_remove) and\
                               not core.is_seq_present(all_data[embl_id], seq):
                                # Key=idimgt_description, val=Sequence object
                                all_data[embl_id].append((all_info,
                                                          core.Sequence(header,
                                                          seq)))
                # Reset all sets
                all_organisms = []
                all_functions = []
                all_products = []
                all_genes = []
                all_others = []
                all_notes = []
                all_prot_ids = []
                all_trans = []
                # Reset all ids
                pat_id = ""
                pubmed_id = ""
                seq_id = ""
                embl_id = ""

    return all_data

def check_data(all_data, light_chains, heavy_chains):
    """ Get only coherent data """
    good_data = {}
    for db_id in all_data:
        # At least 2 seq for an id an not more than 100
        if len(all_data[db_id]) > 1 and len(all_data[db_id]) <= 100:
            has_light = False
            has_heavy = False
            # Get only when we have at least one light and one heavy
            for seq in all_data[db_id]:
                if any(chain in seq[0].lower() for chain in light_chains):
                    has_light = True
                if any(chain in seq[0].lower() for chain in heavy_chains):
                    has_heavy = True
            if has_light and has_heavy:
                good_data[db_id] = all_data[db_id]
    return good_data

def main():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Xenomouse",
                 "signal-regulatory",
                 "Fibrinogen",
                 "TCR",
                 "subcomponent",
                 "hydrolase",
                 "dynein",
                 "Cathepsin",
                 "peptidase",
                 "protease",
                 "MYELOPEROXIDASE",
                 "HLA",
                 "antigen",
                 "THROMBIN",
                 "COAGULATION FACTOR",
                 "factor VII",
                 "factor X",
                 "protein C",
                 "cell-surface antigen",
                 "Myosin",
                 "T cell receptor",
                 "T-cell receptor",
                 "amyloid",
                 "IgM ",
                 " IgM",
                 "thyroglobulin"]

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"
    #organisms = ["mouse", "musculus"]
    #org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "light", "kappa", "lambda", "l-chain", "l chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "heavy", "alpha", "gamma", "delta", "epsilon", "mu ", " mu", "h-chain", "h chain"]

    # File to process
    raw_file = "../../RAW/all_EMBL.dat"
    # File to process
    raw_file_ids = "../../RAW/all_EMBL_ids.txt"
    # Output files
    output_light = f"EMBL_{org_name}_light.fasta"
    output_heavy = f"EMBL_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    to_remove = [to_rm.lower() for to_rm in to_remove]
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, raw_file_ids, all_chains, organisms, to_remove)
    print(len(all_data))
    # Check data
    all_data = check_data(all_data, light_chains, heavy_chains)
    print(len(all_data))
    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size)

if __name__ == '__main__':
    main()
    sys.exit(0)

"""
On garde les synthetic?
{((range(1, 271),), 'Camelus dromedarius'), ((range(271, 1573),), 'Homo sapiens'), ((range(1, 1573),), 'synthetic construct')}
j'ai la séquence de 1 à 1573...

Bon, j'abandonne

On n'a pas de moyen efficace de pairer les Ig, par pubmed id je me retoruve des fois avec 300seq, super lent ensuite
Et surtout j'ai 46200 séquences pas liées.
Je pourrais utiliser les dates d'ajout, la liste ds auteurs, etc... Mais un moment, ca devient nimp

On a des soucsi d'espace par exemple dans les notes. Exemple :
light chai n sequence
mais aussi :
light chainsequence
si je mets un espace entre les differentes ligens de notes par exemple, des fois il n'y en a pas besoin, et ça casse la similarité des chaines
je pourrais virer tous les espaces dans core, pour faire les checks, mais un moment...

Si je prends comme id du tuple la description + id protein, j'ai moins de soucis, alors que je devrais en a avoir plus
"""