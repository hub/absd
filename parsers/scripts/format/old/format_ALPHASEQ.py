# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a csv file
    from AlphaSeq (https://zenodo.org/record/5095284)
    They might be duplicate sequences, but ids are unique """
import re
import sys
from collections import defaultdict
import core

def get_data(input_file):
    """ Get clean data from IMGT file """
    # Dict of list for the main results
    # key -> imgtid_description
    all_data = defaultdict(list)
    with open(input_file, encoding="UTF-8") as alpha_file:
        next(alpha_file)
        for line in alpha_file:
            id_, _, _, _, _, _, heavy, light, _, _, _, _, _, _ = line.split(",")
            header_l = f"{id_}_ALPHASEQ light;{id_}"
            header_h = f"{id_}_ALPHASEQ heavy;{id_}"
            all_data[id_].append((header_l, core.Sequence(">"+header_l, light)))
            all_data[id_].append((header_h, core.Sequence(">"+header_h, heavy)))
    return all_data

def main():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]
    # Organism to extract
    organisms = ["human", "sapiens", "homo"]
    org_name = "Homo_sapiens"

    # File to process
    raw_file = "../RAW/ALPHASEQ.csv"

    # Output files
    output_light = "../CLEAN/ALPHASEQ_Homo_sapiens_light.fasta"
    output_heavy = "../CLEAN/ALPHASEQ_Homo_sapiens_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    main()
    sys.exit(0)
