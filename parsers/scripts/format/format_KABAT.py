# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a fasta file from the kabat
    They might be duplicate sequences, but ids are unique
"""
from collections import defaultdict
import os
import sys
import core
import pathlib

def get_data(input_file, all_chains, organisms, to_remove=None):
    """ Get clean data from kabat file """
    # Dict of list for the main results
    # key -> kabatid_description
    all_data = defaultdict(list)
    short_header = None
    with open(input_file, encoding="UTF-8") as kabat_file:
        for line in kabat_file:
            # Header
            if line.startswith(">"):
                # We already have a header, end of the last sequence
                if short_header:
                    # We need something like 'light chain' or 'chain kappa'
                    if any(chain in short_header.lower() for chain in all_chains):
                        to_add = True
                        # Check if this is NOT an IgG
                        for to_rm in to_remove:
                            # At least one wrong keyword, do not add
                            if to_rm.lower() in short_header.lower():
                                to_add = False
                                break
                        # Remove starting/ending ???
                        seq = seq.strip("?")
                        # It is a proper IgG, and this seq is not already in
                        if "?" not in seq and\
                           to_add and not core.is_seq_present(all_data[kabat_id],
                                                         line.strip()):
                            # We need the id without the _1 or else from
                            # full_kabat_id AND the description,
                            # to later do the pairing
                            clean_id = f"{kabat_id} {description}"
                            # Key=idkabat_description, val=Sequence object
                            f_head = f"{header}"
                            all_data[kabat_id].append((clean_id,
                                                        core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())};{kabat_id}",
                                                                  seq)))
                seq = ""
                short_header = None
                # We need 'sapiens' or 'human', etc in the header
                if any(org in line.lower() for org in organisms):
                    # Full header
                    header = line.strip()
                    # Complete kabat id with > like >7PMI_4
                    full_kabat_id = line.strip().split("|")[0]
                    # kabat id like 7PMI, no _L or _H
                    kabat_id = full_kabat_id[1:-2].lower()
                    # Description, everything after the first |
                    description = "|".join(line.split("|")[1:])
                    # Removing double comma typo
                    description = description.replace(", , ", ", ")
                    # Removing double (or more) spaces
                    description = " ".join(description.split())
                    # Short header, for precise search
                    short_header = full_kabat_id + "|" + description
            # We have a header, let take the sequence
            else:
                seq += line.strip()
    # End of the last sequence
    if short_header:
        # We need something like 'light chain' or 'chain kappa'
        if any(chain in short_header.lower() for chain in all_chains):
            to_add = True
            # Check if this is NOT an IgG
            for to_rm in to_remove:
                # At least one wrong keyword, do not add
                if to_rm.lower() in short_header.lower():
                    to_add = False
                    break
            # It is a proper IgG, and this seq is not already in
            if "?" not in seq and\
               to_add and not core.is_seq_present(all_data[kabat_id],
                                             line.strip()):
                # We need the id without the _1 or else from
                # full_kabat_id AND the description,
                # to later do the pairing
                clean_id = f"{kabat_id} {description}"
                # Key=idkabat_description, val=Sequence object
                f_head = f"{header}"
                all_data[kabat_id].append((clean_id,
                                            core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())};{kabat_id}",
                                                      seq)))
    return all_data

def main_human():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

def main_mouse():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["mouse", "musculus"]
    org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

def main_bovine():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 220
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Bos", "Taurus"]
    org_name = "Bos_taurus"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

def main_monkey():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 180
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Macaca", "mulatta", "monkey", "rhesus", "macaque"]
    org_name = "Macaca_mulatta"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

def main_rabbit():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Oryctolagus", "cuniculus", "rabbit"]
    org_name = "Oryctolagus_cuniculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

def main_rat():
    """ Format data from raw KABAT fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Rattus", "norvegicus"]
    org_name = "Rattus_norvegicus"

    # Keywords linked to LIGHT sequences
    light_chains = ["LIGHT"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["HEAVY"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # Replace all spaces by underscores
    with open(f"{path}/../../RAW/kabat2000.faa", encoding="utf-8") as inp_f,\
         open(f"{path}/../../RAW/kabat2000.fasta", "w", encoding="utf-8") as out_f:
         for line in inp_f:
            if " " in line:
                out_f.write(line.replace(" ", "_"))
            else:
                out_f.write(line)

    # File to process
    raw_file = f"{path}/../../RAW/kabat2000.fasta"

    # Output files
    output_light = f"{path}/../../CLEAN/KABAT_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/KABAT_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

    # Clean the initial downloaded file
    os.remove(f"{path}/../../RAW/kabat2000.fasta")

if __name__ == '__main__':
    print("KABAT-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("KABAT-mouse")
    main_mouse()
    print()
    print()
    print()
    print()
    print()
    print("KABAT-monkey")
    main_monkey()
    print()
    print()
    print()
    print()
    print()
    print("KABAT-rabbit")
    main_rabbit()
    print()
    print()
    print()
    print()
    print()
    print("KABAT-rat")
    main_rat()
    #print()
    #print()
    #print()
    #print()
    #print()
    #print("KABAT-bovine")
    #main_bovine() # Germline annotation database
    sys.exit(0)
