# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a csv file from the PLAbDab
    They might be duplicate sequences, but ids are unique
"""
from collections import defaultdict
import csv
import gzip
import os
import sys
import core
import pathlib
import shutil

def get_data(input_file, all_chains, organisms, to_remove=None):
    """ Get clean data from PDB file """
    # Dict of list for the main results
    # key -> pdbid_description
    all_data = defaultdict(list)
    cpt = 0

    # Open this file
    with open(input_file, encoding="UTF-8") as csv_file:
        csv_file = csv.reader(csv_file)
        for line in csv_file:
            # Ger relevant columns
            name = line[0].replace("_", "-")
            heavy = line[1]
            light = line[2]
            origin = line[7]
            title = line[9]
            if light and heavy:
                # Create both id for further pairing
                id_light = f"{name}_light"
                id_heavy = f"{name}_heavy"
                to_add = True
                # Check if we need to not keep it
                for to_rm in to_remove:
                    # At least one wrong keyword, do not add
                    if to_rm.lower() in origin.lower():
                        to_add = False
                        break
                # It is a proper IgG and the correct organism
                if to_add and any(org in origin.lower() for org in organisms):
                    # Do the light
                    clean_id = f"{id_light} {origin} {organisms[0]}"
                    # Key=id_description, val=Sequence object
                    f_head = f">{name.replace('-', '_')}_{origin}_{organisms[0]}"
                    all_data[name].append((clean_id, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())}_light;{name.replace('-', '_')}", light)))
                    # Do the heavy
                    clean_id = f"{id_heavy} {origin} {organisms[0]}"
                    # Key=id_description, val=Sequence object
                    f_head = f">{name.replace('-', '_')}_{origin}_{organisms[0]}"
                    all_data[name].append((clean_id, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())}_heavy;{name.replace('-', '_')}", heavy)))

    return all_data

def main_human():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["human", "sapiens", "patient", "homo"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_mouse():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["mouse", "musculus", "mice"]
    org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_bovine():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 220
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Bos", "Taurus"]
    org_name = "Bos_taurus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_monkey():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 180
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Macaca", "mulatta", "monkey", "rhesus", "macaque"]
    org_name = "Macaca_mulatta"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rabbit():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Oryctolagus", "cuniculus", "rabbit"]
    org_name = "Oryctolagus_cuniculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rat():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Rattus", "norvegicus"]
    org_name = "Rattus_norvegicus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()

    # File to process
    raw_file = f"{path}/../../RAW/paired_sequences.csv"
    # Extract the gz file
    with gzip.open(f"{path}/../../RAW/paired_sequences.csv.gz", "rb") as f_in:
        with open(raw_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Output files
    output_light = f"{path}/../../CLEAN/PLAbDab_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/PLAbDab_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Remove this file
    os.remove(raw_file)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("PLAbDab-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("PLAbDab-mouse")
    main_mouse()
    print()
    print()
    print()
    print()
    print()
    print("PLAbDab-monkey")
    main_monkey()
    print()
    print()
    print()
    print()
    print()
    print("PLAbDab-rabbit")
    main_rabbit()
    print()
    print()
    print()
    print()
    print()
    print("PLAbDab-rat")
    main_rat()
    #print()
    #print()
    #print()
    #print()
    #print()
    #print("PLAbDab-bovine")
    #main_bovine() # Germline annotation database
    sys.exit(0)