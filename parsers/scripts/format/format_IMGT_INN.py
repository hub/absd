# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a fasta file from the PDB
    They might be duplicate sequences, but ids are unique
    Use: https://www.rcsb.org/downloads/fasta to get the fasta file
    Max 1000 ids, use split antibodies.txt
    Id are like 5esz, no _1 or anything
    Same script for SACS and manual PDB
"""
from collections import defaultdict
import glob
import re
import sys
import core
import pathlib

def get_data(input_folder, all_chains, organisms, to_remove=None):
    """ Get clean data from PDB file """
    # Dict of list for the main results
    # key -> pdbid_description
    all_data = defaultdict(list)
    for file in glob.glob(f"{input_folder}*.inn"):
        with open(file) as in_file:
            to_take = False
            light = ""
            heavy = ""
            for line in in_file:
                # Name
                if "REMARK 410 INN name" in line:
                    name = next(in_file)[11:].strip()
                # Header
                if "REMARK 410 Chain ID " in line:
                    id_ = line.split("(")[1][:-3]
                    chain = line.split("(")[1][-3:-2]
                # Everything after REMARK 410 Chain amino acid sequence
                if "REMARK 410 Chain amino acid sequence" in line:
                    to_take = True
                    tmp_seq = ""
                if "REMARK 410 V-DOMAIN" in line:
                    to_take = False
                    if chain == "L":
                        light = tmp_seq
                    elif chain == "H":
                        heavy = tmp_seq
                    else:
                        if light:
                            heavy = tmp_seq
                        else:
                            light = tmp_seq
                if to_take:
                    if not re.search("[^A-Z]", line[11:].strip()):
                        tmp_seq += (line[11:].strip())

        if light and heavy:
            clean_id = f"{id_} light {name} human mAB"
            f_head = f">{id_}_{name}_human mAB INN"
            all_data[id_].append((clean_id, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())} light;{id_}", light)))

            clean_id = f"{id_} heavy {name} human mAB"
            f_head = f">{id_}_{name} human mAB INN"
            all_data[id_].append((clean_id, core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())} heavy;{id_}", heavy)))
    return all_data

def main():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT-INN/"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT-INN_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT-INN_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("IMGT_INN-human")
    main()
    sys.exit(0)
