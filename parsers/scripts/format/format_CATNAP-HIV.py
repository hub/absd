# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a csv file from Thera-SAbDab """
from collections import defaultdict
import sys
import os
import gzip
import core
import pathlib

def next_sequence(file):
    """ Return each sequence of a file as a tuple (header, seq) using a
    generator. The file can be in fasta or fastq format, gzipped or not.

    :param file: fasta/fastq file to read, gzipped or not
    :type file: str

    :return: the current peptide in the file
    :rtype: tuple(str, str)
    """

    # If the file is empty, return None
    # @WARNING does not work for empty gz file
    if not os.path.isfile(file) or os.path.getsize(file) <= 0:
        return None
    # Is it a GZIP file?
    test_file = open(file, "rb")
    # Get the first values
    magic = test_file.read(2)
    # Close the file
    test_file.close()

    # Open the file, GZIP or not
    with (gzip.open(file, "rb") if magic == b"\x1f\x8b"
          else open(file, "rb")) as in_file:
        first_line = in_file.readline().decode('utf-8')
        # FASTQ file
        if first_line.startswith("@"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fastq header
                if header.startswith("@"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Skip the two next lines
                    in_file.readline()
                    in_file.readline()
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # (multi?)FASTA file
        elif first_line.startswith(">"):
            # Go to beginning of the file
            in_file.seek(0)
            # Read each line
            for line in in_file:
                # Consider this line as a header
                header = line.decode('utf-8').strip()
                # It is a proper fasta header
                if header.startswith(">"):
                    # Get the sequence
                    sequence = in_file.readline().decode('utf-8').strip()
                    # Get current offset
                    current_offset = in_file.tell()
                    # Get next line
                    next_l = in_file.readline().decode('utf-8').strip()
                    # While this next line is not a fasta header...
                    while next_l and not next_l.startswith(">"):
                        # Add this to the Sequence
                        sequence += next_l
                        # Get current offset
                        current_offset = in_file.tell()
                        # Get next line
                        next_l = in_file.readline().decode('utf-8').strip()
                    # Next line is a fasta header, go back to its beginning
                    in_file.seek(current_offset)
                    # Return header and sequence and wait for the next one
                    yield (header, sequence.upper())

        # Not a valid file
        else:
            # Stop the generator with the error to show
            print(f"File error: enable to understand type of file {file} "\
                  f"({first_line[0]})", file=sys.stderr)
            sys.exit(1)

def get_data(input_file_l, input_file_h, organisms=None, to_remove=[]):
    """ Get clean data from csv file """
    all_data = defaultdict(list)
    tmp = set()
    cpt = 0
    # Get the lights
    for seq in next_sequence(input_file_l):
        to_add = False
        clean_id = seq[0].split("_")[-1][7:]
        header = f"{seq[0][1:]} CATNAP-HIV light"
        fhead = f">{seq[0][1:]}_CATNAP-HIV"
        # Check if this is the correct species
        if organisms:
            for org in organisms:
                # At least one good keyword, add
                if org.lower() in header.lower():
                    to_add = True
                    break
        # No specific organism, take everything
        else:
            to_add = True
        # Check if this is NOT an IgG to take
        for to_rm in to_remove:
            # At least one wrong keyword, do not add
            if to_rm.lower() in header.lower():
                to_add = False
                break
        # Add it if necessary
        if to_add:
            all_data[clean_id].append((header, core.Sequence(f"{' '.join(' '.join(fhead.replace('|||', ' ').split()).replace(';', ' ').split())} light;{clean_id}", seq[1])))
    # Get the heavies
    for seq in next_sequence(input_file_h):
        to_add = False
        clean_id = seq[0].split("_")[-1][7:]
        header = f"{seq[0][1:]} CATNAP-HIV heavy"
        fhead = f">{seq[0][1:]}_CATNAP-HIV"
        # Check if this is the correct species
        if organisms:
            for org in organisms:
                # At least one good keyword, add
                if org.lower() in header.lower():
                    to_add = True
                    break
        # No specific organism, take everything
        else:
            to_add = True
        # Check if this is NOT an IgG to take
        for to_rm in to_remove:
            # At least one wrong keyword, do not add
            if to_rm.lower() in header.lower():
                to_add = False
                break
        # Add it if necessary
        if to_add:
            all_data[clean_id].append((header, core.Sequence(f"{' '.join(' '.join(fhead.replace('|||', ' ').split()).replace(';', ' ').split())} light;{clean_id}", seq[1])))

    return all_data

def main_human():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["macaque", "Mus_musculus", "Bos_taurus", "llama", "Rabbit"]

    # Organism to extract
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, None, to_remove)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_mouse():
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Mus", "Musculus"]
    org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, organisms)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_bovine(): # Not good, no igBlast database
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 220
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Bos", "Taurus"]
    org_name = "Bos_taurus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, organisms)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_monkey(): # Not good, no igBlast database
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 180
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Macaca", "mulatta", "monkey", "rhesus", "macaque"]
    org_name = "Macaca_mulatta"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, organisms)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rabbit(): # Not good, no igBlast database
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Oryctolagus", "cuniculus", "rabbit"]
    org_name = "Oryctolagus_cuniculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, organisms)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rat(): # Not good, no igBlast database
    """ Format data from raw PDB fasta file """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = []

    # Organism to extract
    organisms = ["Rattus", "norvegicus"]
    org_name = "Rattus_norvegicus"

    # Keywords linked to LIGHT sequences
    light_chains = ["light"]
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # Files to process
    raw_file_l = f"{path}/../../RAW/CATNAP-HIV_light.fasta"
    raw_file_h = f"{path}/../../RAW/CATNAP-HIV_heavy.fasta"
    # Output files
    output_light = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/CATNAP-HIV_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file_l, raw_file_h, organisms)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("CATNAP-HIV-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("CATNAP-HIV-mouse")
    main_mouse()
    print()
    print()
    print()
    print()
    print()
    print("CATNAP-HIV-monkey")
    main_monkey()
    print()
    print()
    print()
    print()
    print()
    print("CATNAP-HIV-rabbit")
    main_rabbit()
    print()
    print()
    print()
    print()
    print()
    print("CATNAP-HIV-rat")
    main_rat()
    #print()
    #print()
    #print()
    #print()
    #print()
    #print("CATNAP-HIV-bovine")
    #main_bovine() # Germline annotation database
    sys.exit(0)
