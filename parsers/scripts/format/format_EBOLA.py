# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a fasta file from IMGT
    They might be duplicate sequences, but ids are unique """
import re
import sys
from collections import defaultdict
import core
import pathlib

def get_data(input_file):
    """ Get clean data from IMGT file """
    # Dict of list for the main results
    # key -> imgtid_description
    all_data = {}
    id_seq = ""
    seq = ""
    next_is_seq = False
    with open(input_file, encoding="UTF-8") as embl_file:
        for line in embl_file:
            # We have the id
            if line.startswith("LOCUS"):
                id_seq = line[12:].split(" ")[0]
            # Get the sequence
            elif line.strip().startswith("/translation"):
                seq = line.strip().split("\"")[1]
                if not line.strip().endswith("\""):
                    next_is_seq = True
            elif next_is_seq:
                seq += line.strip()
                if not line.strip().endswith("\""):
                    next_is_seq = True
                else:
                    next_is_seq = False
                    seq = seq[:-1]
            # End of this record
            elif line.strip() == "//":
                all_data[id_seq] = seq
    return all_data

def pairing_igg(all_data, all_ids_file):
    """ Pairing IgG using ids file"""
    all_res = defaultdict(list)
    with open(all_ids_file) as id_file:
        # Skip header line
        next(id_file)
        # For each line
        for line in id_file:
            tmp = re.sub("[ \t]+", " ", line.strip()).split(" ")
            id_l = tmp[2]
            id_h = tmp[1]
            clean_id = id_l + "-" + id_h
            header_l = f"{clean_id} EBOLA light"
            header_h = f"{clean_id} EBOLA heavy"
            fhead_l = f">{clean_id}_EBOLA light"
            fhead_h = f">{clean_id}_EBOLA heavy"
            all_res[clean_id].append((header_l, core.Sequence(f"{' '.join(' '.join(fhead_l.replace('|||', ' ').split()).replace(';', ' ').split())};{id_l}", all_data[id_l])))
            all_res[clean_id].append((header_h, core.Sequence(f"{' '.join(' '.join(fhead_h.replace('|||', ' ').split()).replace(';', ' ').split())};{id_h}", all_data[id_h])))
    return all_res

def main():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # Keywords linked to LIGHT sequences
    light_chains = ["light"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["heavy"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/EBOLA.gb"
    # File to process
    raw_file_ids = f"{path}/../../RAW/EBOLA_ids_paired.tsv"
    # Output files
    output_light = f"{path}/../../CLEAN/EBOLA_Homo_sapiens_light.fasta"
    output_heavy = f"{path}/../../CLEAN/EBOLA_Homo_sapiens_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]

    # Get the fasta from file
    all_res = get_data(raw_file)
    # Pairing data
    all_data = pairing_igg(all_res, raw_file_ids)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, "Homo_sapiens")

if __name__ == '__main__':
    print("EBOLA-human")
    main()
    sys.exit(0)
