# -*- coding: utf-8 -*-

########################################################################
# Author: Nicolas Maillet                                              #
# Copyright © 2024 Institut Pasteur, Paris.                            #
# See the COPYRIGHT file for details                                   #
#                                                                      #
# This file is part of AntiBody Sequence Database (ABSD) software.     #
#                                                                      #
# ABSD is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# any later version.                                                   #
#                                                                      #
# ABSD is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public license    #
# along with ABSD (LICENSE file).                                      #
# If not, see <http://www.gnu.org/licenses/>.                          #
########################################################################

""" Extract all paired IgG from a fasta file from IMGT
    They might be duplicate sequences, but ids are unique """
from collections import defaultdict
import re
import sys
import core
import pathlib

def get_data(input_file, all_chains, organisms, to_remove, min_size):
    """ Get clean data from IMGT file """
    # Dict of list for the main results
    # key -> imgtid_description
    all_data = defaultdict(list)
    with open(input_file, encoding="UTF-8") as imgt_file:
        # Skip first lines
        line = imgt_file.readline()
        while not line.strip().endswith("."):
            line = imgt_file.readline()
        # Start parsing
        cpt = 1 # Line id in the file
        second_chain = None # Normal case
        for line in imgt_file:
            # Stop reading the file when encounter this
            if "domains selected in" in line:
                break
            line = line.strip()
            if line:
                s_line = line.split("\t")
                # Get proper list, not ending spaces
                # If header:
                # 0 line id in the file
                # 1 imgt id, always there
                # 2 human, might be empty, remove is so
                # 3 descr, might be empty, don't care
                # 4 VH V-LAMBDA, might be empty, remove is so
                # 5 [24.17.38.11], not needed
                s_line = [field.strip() for field in s_line]
                # We have a header
                if s_line[0] == str(cpt) and len(s_line) > 4:
                    cpt += 1
                    # We need 'sapiens' or 'human', etc in the header
                    # and a chain
                    if any(org in s_line[2].lower() for org in organisms) and\
                       s_line[4]:
                        # Full header, without line number
                        header = ">" + re.sub(r"^\d+ \t", "", line)
                        # Complete imgt id with > like >1a0q_H
                        full_imgt_id = ">" + s_line[1]
                        # imgt id like 1a0q
                        imgt_id = full_imgt_id[1:].split("_")[0].lower()
                        # Chain
                        chain = s_line[4]
                        # Description
                        description = s_line[3]
                        # Removing double comma typo
                        description = description.replace(", , ", ", ")
                        # Removing double (or more) spaces
                        description = " ".join(description.split())
                    else:
                        header = None

                # We have a header
                elif header:
                    # There is a sequence!
                    if len(line) > min_size:
                        # We need the id without the _1 or else from
                        # full_imgt_id AND the description AND chain,
                        # to later do the pairing
                        clean_id = f"{imgt_id} {description} {chain}"
                        # The sequence should not contain spaces nor .
                        seq = line.replace(".", "").replace(" ", "")
                        # The seq is not already there and
                        # does not contain #
                        # but contains a chain
                        if "#" not in seq and \
                            not core.is_seq_present(all_data[imgt_id], seq) and\
                            any(chain in header.lower() for chain in all_chains):
                            to_add = True
                            # Check if this is NOT an IgG
                            for to_rm in to_remove:
                                # At least one wrong keyword, do not add
                                if to_rm.lower() in clean_id.lower():
                                    to_add = False
                                    break
                            if to_add:
                                # Key=idimgt_description, val=Sequence object
                                f_head = f"{header}"
                                all_data[imgt_id].append((clean_id,
                                                          core.Sequence(f"{' '.join(' '.join(f_head.replace('|||', ' ').split()).replace(';', ' ').split())};{imgt_id}",
                                                          seq)))
                        if second_chain:
                            # Change the header
                            header = second_header
                            # Change the chain
                            chain = second_chain
                            # End of this special case
                            second_chain = None
                    # No sequence! It is a multi-line 2-seq malakia
                    else:
                        # A line with only the second chain
                        if any(a_chain in line.lower() for a_chain in all_chains):
                            # Chain
                            second_chain = line
                        # A line with description
                        else:
                            # Add this line to the header
                            header += " " + line
                            # Second full header, we need to replace the chain
                            second_header = header.replace(chain, second_chain)
    return all_data

def main_human():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["human", "sapiens"]
    org_name = "Homo_sapiens"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_mouse():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["mouse", "musculus"]
    org_name = "Mus_musculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_bovine():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 220
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["Bos", "Taurus"]
    org_name = "Bos_taurus"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_monkey():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 180
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["Macaca", "mulatta", "monkey", "rhesus", "macaque"]
    org_name = "Macaca_mulatta"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rabbit():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["Oryctolagus", "cuniculus", "rabbit"]
    org_name = "Oryctolagus_cuniculus"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

def main_rat():
    """ Format data from raw res of IMGT web page """
    # Minimal et maximal size of an IgG
    min_size = 80
    max_size = 150
    # headers/sequences to ignore in the input file
    # if containing one of these words
    to_remove = ["Polymeric immunoglobulin receptor", "T-cell"]

    # Organism to extract
    organisms = ["Rattus", "norvegicus"]
    org_name = "Rattus_norvegicus"

    # Keywords linked to LIGHT sequences
    light_chains = ["V-LAMBDA", "V-LIKE", "V-KAPPA", "V-ALPHA", "V-BETA", "l-chain"] # Should we add V-DELTA?
    # Keywords linked to HEAVY sequences
    heavy_chains = ["VH", "h-chain"]

    # Path of this file
    path = pathlib.Path(__file__).parent.resolve()
    # File to process
    raw_file = f"{path}/../../RAW/IMGT2.txt"
    # Output files
    output_light = f"{path}/../../CLEAN/IMGT2_{org_name}_light.fasta"
    output_heavy = f"{path}/../../CLEAN/IMGT2_{org_name}_heavy.fasta"

    # Get all chains/organisms in lower
    light_chains = [chain.lower() for chain in light_chains]
    heavy_chains = [chain.lower() for chain in heavy_chains]
    all_chains = light_chains + heavy_chains
    organisms = [org.lower() for org in organisms]

    # Get the fasta from file
    all_data = get_data(raw_file, all_chains, organisms, to_remove, min_size)

    # Do the rest!
    core.get_igg(all_data, light_chains, heavy_chains, output_light, output_heavy, min_size, max_size, org_name)

if __name__ == '__main__':
    print("IMGT2-human")
    main_human()
    print()
    print()
    print()
    print()
    print()
    print("IMGT2-mouse")
    main_mouse()
    print()
    print()
    print()
    print()
    print()
    print("IMGT2-monkey")
    main_monkey()
    print()
    print()
    print()
    print()
    print()
    print("IMGT2-rabbit")
    main_rabbit()
    print()
    print()
    print()
    print()
    print()
    print("IMGT2-rat")
    main_rat()
    #print()
    #print()
    #print()
    #print()
    #print()
    #print("IMGT2-bovine")
    #main_bovine() # Germline annotation database
    sys.exit(0)
