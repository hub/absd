# Builder image
# =============

FROM node:22-alpine3.21 AS builder
WORKDIR /usr/build

# Copy only the relevant files to build the application
COPY ./src/ ./src/
COPY ./data/sources.json ./data/sources.json
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
COPY ./tsconfig.json ./tsconfig.json
COPY ./vite.config.ts ./vite.config.ts

# Install all dependencies
RUN npm ci

# Switch to production mode for the build
ENV NODE_ENV="production"
RUN npm run build

# Production image
# ================

FROM node:22-alpine3.21
WORKDIR /usr/src/absd
ENV NODE_ENV="production"

LABEL name="absd" \
      description="AntiBody Sequence Database, a website to easily build a set of antibody chain sequences gathered from different sources."

# Copy only the relevant files
COPY --from=builder /usr/build/src/server ./src/server
COPY --from=builder /usr/build/src/scripts ./src/scripts
COPY --from=builder /usr/build/dist/ ./dist/
COPY --from=builder /usr/build/package.json ./package.json
COPY --from=builder /usr/build/package-lock.json ./package-lock.json
COPY ./data/ ./data/

# Install production dependencies and remove the cache for extra space saving
RUN npm ci && npm cache clean --force

# Use the built-in node user to respect the principle of least privilege (PoLP)
USER node

# Setup port and command to run the container
EXPOSE 3000
CMD [ "node", "src/server/app.js" ]
