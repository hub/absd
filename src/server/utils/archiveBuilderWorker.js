// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream, createWriteStream } from "node:fs"
import { rm } from "node:fs/promises"
import { tmpdir } from "node:os"
import { join } from "node:path"
import { pipeline } from "node:stream/promises"
import { parentPort, workerData } from "node:worker_threads"
import { createGzip } from "node:zlib"
import { MongoClient } from "mongodb"
import envConfig from '../config/env.js'
import AntibodyFastaStream from "../models/AntibodyFastaStream.js"
import AntibodyJsonStream from "../models/AntibodyJsonStream.js"

// =========================================================================

const tmpDir = tmpdir()
const dataFormat = workerData.archiveFormat
const tmpFileExt = dataFormat === 'fasta' ? '.fasta' : '.json'
const tmpFileName = `${workerData.archiveId}${tmpFileExt}`
const tmpFilePath = join(tmpDir, tmpFileName)
const archiveName = `${tmpFileName}.gz`
const archivePath = join(tmpDir, archiveName)

const mongoURI = `mongodb://${envConfig.ABSD_DB_HOST}:${envConfig.ABSD_DB_PORT}/${envConfig.ABSD_DB_NAME}`
const database = await MongoClient.connect(mongoURI)
const dataStream = database
  .db()
  .collection('antibodies')
  .find(workerData.requestWhere, {
    projection: workerData.requestFields
  })
  .stream()

const transformStream = dataFormat === 'fasta'
  ? new AntibodyFastaStream()
  : new AntibodyJsonStream()

const tmpFileStream = createWriteStream(tmpFilePath)
const compressStream = createGzip({ level: 1 }) // Minimum compression to increase speed
const archiveStream = createWriteStream(archivePath)

// Create a temporary file and then compress it.
// This is more performant by around 35% than compressing on the fly.
await pipeline(
  dataStream,
  transformStream,
  tmpFileStream
).then(async () => {
  return await pipeline(
    createReadStream(tmpFilePath),
    compressStream,
    archiveStream
  )
}).then(async () => {
  parentPort.postMessage({
    archiveName,
    archivePath
  })
  await rm(tmpFilePath, { force: true })
  process.exit(0)
}).catch(async (err) => {
  await rm(tmpFilePath, { force: true })
  await rm(archivePath, { force: true })
  throw err
})
