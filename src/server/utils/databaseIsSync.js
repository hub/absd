// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { readdir } from 'node:fs/promises'
import { dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

/**
 * Check if the database is already there and synchronized with the
 * last version of the data by checking the stats version date.
 * Used mainly to check if the database should be rebuilt or not.
 * @param {object} fastify - The Fastify server object
 * @returns {boolean} If the database is sync or not
 */
export default async function databaseIsSync(fastify) {
  // Get the stats of the current database
  const dbStats = await fastify.mongo.db
    .collection('statistics')
    .findOne()

  // Stop here if the database does not exist
  if (!dbStats?.versionDate) return false

  // Then fetch the stats of the last stats file for comparison
  const currentPath = dirname(fileURLToPath(import.meta.url))
  const dataFolderPath = resolve(currentPath, '../../../data')
  const dataFolder = await readdir(dataFolderPath)

  const statsFilePath = dataFolder
    .filter((file) => { return file.endsWith('-stats.json') })
    .sort()
    .reverse()
    .map(file => { return resolve(dataFolderPath, file) })
    .shift()

  const lastStatsDate = new Date(statsFilePath.match(/(\d{4}-\d{2}-\d{2})-stats/)[1])

  return dbStats.versionDate.getTime() === lastStatsDate.getTime()
}
