// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Pre-validation hook.
 * Parse the url parameters to get the proper format
 * for the ones that are not simple strings.
 */
export default function parseRequest(request, reply, done) {
  if (request.query.keywords) {
    request.query.keywords = request.query.keywords
      .split(',')
      .map(motif => motif.trim())
  }

  if (request.query.species) {
    request.query.species = request.query.species.split(',')
  }

  if (request.query.sources) {
    request.query.sources = request.query.sources.split(',')
  }

  if (request.query.heavySegments) {
    request.query.heavySegments = request.query.heavySegments.split(',')
  }

  if (request.query.lightSegments) {
    request.query.lightSegments = request.query.lightSegments.split(',')
  }

  if (request.query.motif) {
    request.query.motif = request.query.motif
      .trim()
      .toUpperCase()
  }

  return done()
}
