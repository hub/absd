// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import escapeString from '../utils/escapeString.js'

/**
 * Pre-handler hook.
 * Build the MongoDB query for the database based on the
 * querystring parameters.
 */
export default function buildDBQuery(request, reply, done) {
  const where = {
    $and: []
  }

  if (request.query.keywords) {
    request.query.keywords
      .map(term => { return escapeString(term) })
      .forEach(term => {
        where.$and.push({
          $or: [
            { 'heavyChain.rawFasta': { $regex: term, $options: 'i' } },
            { 'lightChain.rawFasta': { $regex: term, $options: 'i' } }
          ]
        })
      })
  }

  if (request.query.species) {
    where.$and.push({
      species: {
        $in: request.query.species
      }
    })
  }

  if (request.query.sources) {
    where.$and.push({
      $or: [
        { 'heavyChain.headers.source': { $in: request.query.sources } },
        { 'lightChain.headers.source': { $in: request.query.sources } }
      ]
    })
  }

  if (request.query.heavySegments) {
    where.$and.push({
      'heavyChain.headers.vGeneSegment': {
        $in: request.query.heavySegments
      }
    })
  }

  if (request.query.lightSegments) {
    where.$and.push({
      'lightChain.headers.vGeneSegment': {
        $in: request.query.lightSegments
      }
    })
  }

  if (request.query.motif) {
    const $or = []

    request.query.motif
      .forEach(motif => {
        $or.push({ 'heavyChain.sequence': { $regex: motif } })
        $or.push({ 'ligthChain.sequence': { $regex: motif } })
      })

    where.$and.push({ $or })
  }

  // Empty $and operators raise an error in MongoDB
  // so where needs to remain empty as well.
  if (where.$and.length !== 0) request.where = where

  return done()
}
