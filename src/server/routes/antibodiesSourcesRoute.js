// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Route to fetch the list of data sources currently found in the
 * antibodies headers, through the whole database.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/sources',
  handler: function (request, reply) {
    this.mongo.db
      .collection('antibodies')
      .distinct('heavyChain.headers.source')
      .then(sources => {
        return reply
          .code(200)
          .header('content-type', 'application/json')
          .send(sources)
      }).catch(err => {
        reply.code(500).send(err)
      })
  },
  schema: {
    response: {
      200: {
        type: 'array',
        items: {
          type: 'string'
        }
      }
    }
  }
}
