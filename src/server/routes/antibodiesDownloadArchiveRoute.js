// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Route to check if the archive building process.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/download/:archiveId',
  handler: function (request, reply) {
    const archiveBuilder = this.archiveJobs.get(request.params.archiveId)

    if (!archiveBuilder) {
      return reply.code(404).send({
        message: 'Not found'
      })
    }

    if (archiveBuilder.isErrored) {
      return reply.code(500).send({
        message: 'Internal error',
        error: archiveBuilder.error.message
      })
    }

    if (archiveBuilder.isDone) {
      return reply.code(200).send({
        message: 'Done',
        id: archiveBuilder.archiveId
      })
    }

    return reply.code(202).send({
      message: 'Accepted',
      id: archiveBuilder.archiveId
    })
  },
  schema: {
    params: {
      type: 'object',
      properties: {
        archiveId: {
          type: 'string',
          pattern: '^[a-zA-Z0-9.-]+$'
        }
      },
      required: ['archiveId']
    }
  }
}
