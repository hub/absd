// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Route to fetch the list of V gene segments currently present in
 * the database.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/v-gene-segments',
  handler: function (request, reply) {
    Promise.all([
      this.mongo.db
        .collection('antibodies')
        .distinct('heavyChain.headers.vGeneSegment'),

      this.mongo.db
        .collection('antibodies')
        .distinct('lightChain.headers.vGeneSegment')
    ]).then(segments => {
      return reply
        .code(200)
        .header('content-type', 'application/json')
        .send({
          heavySegments: segments[0],
          lightSegments: segments[1]
        })
    }).catch(err => {
      reply.code(500).send(err)
    })
  },
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          lightSegments: {
            type: 'array',
            items: {
              type: 'string'
            }
          },
          heavySegments: {
            type: 'array',
            items: {
              type: 'string'
            }
          }
        }
      }
    }
  }
}
