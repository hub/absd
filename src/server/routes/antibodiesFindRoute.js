// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import buildDBQuery from "../hooks/buildDBQuery.js"
import parseRequest from "../hooks/parseRequest.js"
import filterParams from "../queryParams/filterParams.json" with { type: "json"}
import limitParams from "../queryParams/limitParams.json" with { type: "json"}
import sortParams from "../queryParams/sortParams.json" with { type: "json"}

/**
 * List of fields to not send to the client
 * for performance and bandwidth reasons.
 */
const fieldsToSkip = {
  _id: false,
  'heavyChain.rawFasta': false,
  'lightChain.rawFasta': false
}

/**
 * Route for getting antibodies matching a request.
 */
export default {
  method: 'GET',
  url: '/api/antibodies',
  preValidation: [
    parseRequest
  ],
  preHandler: [
    buildDBQuery
  ],
  handler: function (request, reply) {
    this.mongo.db
      .collection('antibodies')
      .find(request.where, { projection: fieldsToSkip })
      .sort(request.query.sortBy || null, request.query.sortOrder || null)
      .limit(request.query.limit || 20)
      .skip(request.query.skip || 0)
      .toArray()
      .then(antibodies => {
        return reply
          .code(200)
          .header('content-type', 'application/json')
          .send(antibodies)
      }).catch(err => {
        reply.code(500).send(err)
      })
  },
  schema: {
    querystring: {
      type: 'object',
      additionalProperties: false,
      properties: {
        ...filterParams,
        ...limitParams,
        ...sortParams
      }
    }
  }
}
