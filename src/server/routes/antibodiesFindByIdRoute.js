// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Route for getting an antibody by hashId.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/:hashId',
  handler: function (request, reply) {
    this.mongo.db
      .collection('antibodies')
      .findOne({
        hashId: request.params.hashId
      }, {
        projection: { _id: false }
      })
      .then(antibody => {
        return reply
          .code(200)
          .header('content-type', 'application/json')
          .send(antibody)
      }).catch(err => {
        reply.code(500).send(err)
      })
  },
  schema: {
    params: {
      type: 'object',
      properties: {
        hashId: {
          type: 'string'
        }
      },
      required: ['hashId']
    }
  }
}
