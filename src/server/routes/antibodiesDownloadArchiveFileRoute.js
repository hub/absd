// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream } from "node:fs"

/**
 * Route to download a user-requested archive once its done.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/download/:archiveId/file',
  handler: function (request, reply) {
    const archiveBuilder = this.archiveJobs.get(request.params.archiveId)

    reply.header('Content-Disposition', `attachment;filename="${archiveBuilder.archiveName}"`)
    reply.send(createReadStream(archiveBuilder.archivePath))
  },
  schema: {
    params: {
      type: 'object',
      properties: {
        archiveId: {
          type: 'string',
          pattern: '^[a-zA-Z0-9.-]+$'
        }
      },
      required: ['archiveId']
    }
  }
}
