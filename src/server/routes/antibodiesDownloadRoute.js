// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { randomUUID } from "node:crypto"
import buildDBQuery from "../hooks/buildDBQuery.js"
import parseRequest from "../hooks/parseRequest.js"
import { ArchiveBuilder } from "../models/ArchiveBuilder.js"
import downloadParams from "../queryParams/downloadParams.json" with { type: "json"}
import filterParams from "../queryParams/filterParams.json" with { type: "json"}

/**
 * List of database fields to get or not, depending
 * of the requested data format.
 * This is for performance and bandwidth reasons.
 */
const fieldsProjection = {
  fasta: {
    _id: false,
    'lightChain.rawFasta': true,
    'lightChain.sequence': true,
    'heavyChain.rawFasta': true,
    'heavyChain.sequence': true
  },
  json: {
    _id: false,
    'heavyChain.rawFasta': false,
    'lightChain.rawFasta': false
  }
}

/**
 * Route for downloading all antibodies matching a request.
 * Start a worker thread for building the archive to not block
 * the main thread.
 * Return the url where the file will be available.
 */
export default {
  method: 'GET',
  url: '/api/antibodies/download',
  preValidation: [
    parseRequest
  ],
  preHandler: [
    buildDBQuery
  ],
  handler: function (request, reply) {
    const archiveId = `absd-${randomUUID()}`
    const requestFields = request.query.format === 'fasta'
      ? fieldsProjection.fasta
      : fieldsProjection.json

    this.archiveJobs.add(new ArchiveBuilder({
      archiveId,
      archiveFormat: request.query.format,
      requestWhere: request.where,
      requestFields
    }))

    reply.code(202).send({
      message: 'Accepted',
      archiveId: archiveId
    })
  },
  schema: {
    querystring: {
      type: 'object',
      additionalProperties: false,
      properties: {
        ...filterParams,
        ...downloadParams
      }
    }
  }
}
