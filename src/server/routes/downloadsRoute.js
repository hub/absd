// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import fs from 'node:fs'
import { dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

/**
 * The absolute path of this file.
 */
const currentPath = dirname(fileURLToPath(import.meta.url))

/**
 * Route for getting the list of database archives in the data folder.
 */
export default {
  method: 'GET',
  url: '/api/downloads',
  handler: function (request, reply) {
    const dirPath = resolve(currentPath, '../../../data')
    const archivesList = fs.readdirSync(dirPath)
      .filter((file) => {
        return file.endsWith('.tar.gz')
      })
      .sort()
      .reverse()

    reply.send(archivesList)
  },
  schema: {
    response: {
      200: {
        type: 'array',
        items: {
          type: 'string'
        }
      }
    }
  }
}
