// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * Statistics about the antibodies.
 */
export class Statistics {
  versionDate = ""
  antibodies = 0
  species = new Set()
  sources = new Set()
  antibodiesPerSpecies = {}
  antibodiesPerSource = {}
  antibodiesPerHeavySegment = {}
  antibodiesPerLightSegment = {}
  antibodiesOnlyInSource = {}
  antibodiesInMultipleSources = {}

  /**
   * Stringify the stats object with a replacer function
   * to handle the sets.
   * @returns {string} The stringified stats object
   */
  toString() {
    return JSON.stringify(this, (key, value) => {
      return value instanceof Set ? [...value] : value
    }, 2)
  }
}
