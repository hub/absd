// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "node:stream"
import { Antibody } from "./Antibody.js"

/**
 * Transform stream for turning antibodies entries from the database
 * into a list of their stringified version and write it to a file
 * in the next stream.
 * Used to build archives for downloads.
 */
export default class AntibodyJsonStream extends Transform {
  constructor(opt) {
    const options = { ...opt }
    // Object mode is mandatory since this stream
    // is made to process antibodies objects.
    options.objectMode = true
    super(options)

    // Marker to know when is the first entry
    // to be processed.
    this.firstEntry = true
  }

  // At the start of the processing, start with a '['
  // to open an array.
  _construct(done) {
    this.push('[\n')
    return done()
  }

  // Stringify the antibodies one by one.
  // Detect the first line so that the comma and new lines (',\n')
  // are appended later, in order to not have a trailing
  // comma at the end of the entries.
  // This is mandatory since while streaming, we can always know when
  // an entry is the first one but we will never know when an entry
  // will be the last one.
  _transform(antibody, encoding, done) {
    if (this.firstEntry) {
      this.push(Antibody.toString(antibody))
      this.firstEntry = false
      return done()
    }

    this.push(`,\n${Antibody.toString(antibody)}`)
    return done()
  }

  // When all antibodies have been processed,
  // close the array.
  _flush(done) {
    this.push(']\n')
    return done()
  }
}
