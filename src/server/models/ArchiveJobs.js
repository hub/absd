// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { rm } from 'node:fs/promises'

/**
 * Class handling the worker system used for building
 * user-requested archives.
 * There should be only one instance, created at server start.
 */
export class ArchiveJobs {

  /**
   * List of ArchiveBuilder instances
   */
  #list = new Map()

  constructor() {
    /**
     * Routine to clean the tmp folder every 10 minutes in order
     * to ensure that residual files from the downloads
     * don't accumulate.
     */
    setInterval(() => {
      return this.cleanUpBuilders()
    }, 600_000)
  }

  /**
   * Add a new ArchiveBuilder instance to the jobs list.
   * @param {ArchiveBuilder} archiveBuilder - An instance of ArchiveBuilder
   */
  add(archiveBuilder) {
    const id = archiveBuilder.archiveId
    this.#list.set(id, archiveBuilder)
  }

  /**
   * Get an ArchiveBuilder by its id.
   * @param {string} archiveId - The ArchiveBuilder id
   * @returns {ArchiveBuilder | undefined} The ArchiveBuilder instance or undefined if not found
   */
  get(archiveId) {
    return this.#list.get(archiveId)
  }

  /**
   * Remove both the ArchiveBuilder from the list and
   * the file from the system.
   * @param {string} archiveId - The ArchiveBuilder id
   */
  remove(archiveId) {
    if (!this.#list.has(archiveId)) return

    const archivePath = this.#list.get(archiveId).archivePath
    this.#list.delete(archiveId)

    rm(archivePath, { force: true })
  }

  /**
   * Remove ArchiveBuilders and their files
   * if they are older than 10 minutes.
   */
  async cleanUpBuilders() {
    this.#list.forEach((builder, id) => {
      if (Date.now() > (builder.terminatedAt + 600_000)) {
        this.remove(id)
      }
    })
  }
}
