// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createHash } from "node:crypto";

/**
 * The main Antibody model.
 * Purely static class to factorize the Antibody related functions.
 */
export class Antibody {
  /**
   * Make a unique list of the sources for an antibody.
   * @param {object} antibody - The given antibody
   * @return {Set<string>} The list of sources present in this antibody
   */
  static listSources(antibody) {
    const sources = new Set()
    antibody.heavyChain.headers.forEach(header => { sources.add(header.source) })
    antibody.lightChain.headers.forEach(header => { sources.add(header.source) })
    return sources
  }

  /**
   * Make a unique list of the v gene segments for an antibody.
   * @param {object} antibody - The given antibody
   * @return {object} An object with the two lists of heavy and light chain segments.
   */
  static listSegments(antibody) {
    const segments = {
      heavyChain: new Set(),
      lightChain: new Set()
    }
    antibody.heavyChain.headers.forEach(header => { segments.heavyChain.add(header.vGeneSegment) })
    antibody.lightChain.headers.forEach(header => { segments.lightChain.add(header.vGeneSegment) })
    return segments
  }

  /**
   * Generate a sha256 hash for a given antibody,
   * using the species, light and heavy sequences.
   * @param {object} antibody - The given antibody
   * @returns {string} The sha256 hash
   */
  static generateHashId(antibody) {
    const hashInput = antibody.species +
      antibody.heavyChain.sequence +
      antibody.lightChain.sequence;

    return createHash('sha256')
      .update(hashInput, 'ascii')
      .digest('hex');
  }

  /**
   * Turn an antibody object into a FASTA entry,
   * using the rawFasta property.
   * @param {object} antibody - The given antibody
   * @returns {string} The antibody as a FASTA data string
   */
  static toFasta(antibody) {
    return [
      `>${antibody.lightChain.rawFasta}`,
      antibody.lightChain.sequence,
      `>${antibody.heavyChain.rawFasta}`,
      antibody.heavyChain.sequence
    ].join('\n')
  }

  /**
   * Turn an antibody object into string.
   * @param {object} antibody - The given antibody
   * @returns {string} the stringified antibody
   */
  static toString(antibody) {
    return JSON.stringify(antibody)
  }
}
