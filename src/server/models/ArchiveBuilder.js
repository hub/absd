// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Worker } from "node:worker_threads"

/**
 * Class handling the process of creating an archive for download,
 * using node workers.
 * @link https://nodejs.org/api/worker_threads.html
 */
export class ArchiveBuilder {
  static #workerFile = './src/server/utils/archiveBuilderWorker.js'

  archiveFormat = 'fasta'
  archiveId = ''
  archiveName = ''
  archivePath = ''
  error = null
  isDone = false
  terminatedAt = null
  isErrored = false
  requestFields = {}
  requestWhere = {}
  worker = null

  constructor(data) {
    this.archiveId = data.archiveId
    this.archiveFormat = data.archiveFormat === 'json'
      ? 'json'
      : 'fasta'
    this.requestWhere = data.requestWhere
    this.requestFields = data.requestFields

    // Start the worker and handle the results by listening to events
    this.worker = new Worker(ArchiveBuilder.#workerFile, {
      workerData: {
        archiveId: this.archiveId,
        archiveFormat: this.archiveFormat,
        requestWhere: this.requestWhere,
        requestFields: this.requestFields
      }
    }).on('message', (message) => {
      // The worker sends the archive name and path once its done
      this.archiveName = message.archiveName
      this.archivePath = message.archivePath
    }).on('exit', () => {
      this.isDone = true
      this.terminatedAt = Date.now()
    }).on('error', (err) => {
      this.isErrored = true
      this.error = err
    })
  }
}
