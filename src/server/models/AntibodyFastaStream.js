// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "node:stream"
import { Antibody } from "./Antibody.js"

/**
 * Transform stream for turning antibodies entries from the database
 * into their FASTA version and write it to a file in the next stream.
 * Used to build archives for downloads.
 */
export default class AntibodyFastaStream extends Transform {
  constructor(opt) {
    const options = { ...opt }
    // Object mode is mandatory since this stream
    // is made to process antibodies objects.
    options.objectMode = true
    super(options)
  }

  // Use the toFasta function to simply turn an entry into
  // a FASTA string.
  _transform(antibody, encoding, done) {
    this.push(`${Antibody.toFasta(antibody)}\n`)
    return done()
  }
}
