// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream } from 'node:fs'
import { dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'
import fastifyMongo from '@fastify/mongodb'
import fastifyStatic from '@fastify/static'
import Fastify from 'fastify'
import buildDatabase from "../../src/scripts/databaseBuilder.js"
import envConfig from './config/env.js'
import { ArchiveJobs } from './models/ArchiveJobs.js'
import antibodiesCountRoute from './routes/antibodiesCountRoute.js'
import antibodiesDownloadArchiveFileRoute from './routes/antibodiesDownloadArchiveFileRoute.js'
import antibodiesDownloadArchiveRoute from './routes/antibodiesDownloadArchiveRoute.js'
import antibodiesDownloadRoute from './routes/antibodiesDownloadRoute.js'
import antibodiesFindByIdRoute from './routes/antibodiesFindByIdRoute.js'
import antibodiesFindRoute from './routes/antibodiesFindRoute.js'
import antibodiesSourcesRoute from './routes/antibodiesSourcesRoute.js'
import antibodiesSpeciesRoute from './routes/antibodiesSpeciesRoute.js'
import antibodiesVGeneSegmentsRoute from './routes/antibodiesVGeneSegmentsRoute.js'
import downloadsFileRoute from './routes/downloadsFileRoute.js'
import downloadsRoute from './routes/downloadsRoute.js'
import healthCheckRoute from './routes/healthCheckRoute.js'
import readyCheckRoute from './routes/readyCheckRoute.js'
import statisticsRoute from './routes/statisticsRoute.js'
import databaseIsSync from './utils/databaseIsSync.js'

/**
 * Setup the fastify instance with custom
 * configuration for several server parts.
 * @see https://fastify.dev/docs/latest/Reference/Server/
 */
const fastify = Fastify({
  // Configuration for AJV.
  ajv: {
    customOptions: {
      allErrors: true,
      strictNumbers: true,
      removeAdditional: false
    }
  },

  // Configuration for the Pino logger.
  logger: {
    base: null
  },

  // Marks if the server is behind a proxy.
  trustProxy: envConfig.PROXY
})

// Absolute path of this file
const currentPath = dirname(fileURLToPath(import.meta.url))

// Path of the dist folder
const distPath = resolve(currentPath, '../../dist')
const indexPath = resolve(distPath, 'index.html')

// Plugins
// =========================================================================

fastify.register(fastifyStatic, {
  prefix: '/',
  root: distPath,
  index: 'index.html',
  preCompressed: true
})
fastify.register(fastifyMongo, {
  url: `mongodb://${envConfig.ABSD_DB_HOST}:${envConfig.ABSD_DB_PORT}/${envConfig.ABSD_DB_NAME}`,
  forceClose: true
})

// Route controllers
// =========================================================================

// Decorate the server with a boolean marking when it is ready.
// Used by the readyCheck route.
fastify.decorate('serverIsReady', false)

// Decorate the server with an object
// to store the workers while then run.
fastify.decorate('archiveJobs', new ArchiveJobs())

// Decorate the request with a where object
// to store the database query across handlers
fastify.decorateRequest('where')
fastify.addHook('onRequest', async (request) => {
  request.where = {}
})

// Register all controllers
fastify.route(antibodiesCountRoute)
fastify.route(antibodiesDownloadArchiveFileRoute)
fastify.route(antibodiesDownloadArchiveRoute)
fastify.route(antibodiesDownloadRoute)
fastify.route(antibodiesFindByIdRoute)
fastify.route(antibodiesFindRoute)
fastify.route(antibodiesSourcesRoute)
fastify.route(antibodiesSpeciesRoute)
fastify.route(antibodiesVGeneSegmentsRoute)
fastify.route(downloadsFileRoute)
fastify.route(downloadsRoute)
fastify.route(statisticsRoute)
fastify.route(healthCheckRoute)
fastify.route(readyCheckRoute)

// If the route does not match any of the above, send the index page
// to display the client 404 page since the app is a SPA.
fastify.setNotFoundHandler((request, reply) => {
  reply
    .type('text/html;charset=utf-8')
    .send(createReadStream(indexPath, 'utf8'))
})

// Global HTTP response headers
// =========================================================================

// NOTE: style-src 'unsafe-inline' is needed because of Plotly
fastify.addHook('onSend', async (request, reply) => {
  reply.headers({
    "Content-Security-Policy":
      "default-src 'self'; \
      base-uri 'self'; \
      connect-src 'self' https://plausible.pasteur.cloud/api/event; \
      font-src 'self'; \
      form-action 'self'; \
      frame-ancestors 'self'; \
      img-src 'self' data:; \
      object-src 'none'; \
      script-src 'self' https://plausible.pasteur.cloud/js/script.js; \
      script-src-attr 'none'; \
      style-src 'self' 'unsafe-inline'; \
      upgrade-insecure-requests",
    "Cross-Origin-Opener-Policy": "same-origin",
    "Cross-Origin-Resource-Policy": "same-origin",
    "Origin-Agent-Cluster": "?1",
    "Permissions-Policy": "camera=(), geolocation=(), microphone=()",
    "Referrer-Policy": "no-referrer",
    "Strict-Transport-Security": "max-age=31536001; includeSubDomains",
    "X-Content-Type-Options": "nosniff",
    "X-DNS-Prefetch-Control": "off",
    "X-Frame-Options": "SAMEORIGIN",
    "X-Permitted-Cross-Domain-Policies": "none"
  })
})

// Start
// =========================================================================

fastify.listen({
  host: envConfig.ABSD_SERVER_HOST,
  port: envConfig.ABSD_SERVER_PORT
}).then(async () => {
  const dbShouldBeRebuilt = !await databaseIsSync(fastify)

  if (envConfig.NODE_ENV === 'production' && dbShouldBeRebuilt) {
    return await buildDatabase()
  }
}).then((dbResults) => {
  if (dbResults) {
    fastify.log.info(`${envConfig.ABSD_DB_NAME} database created`)
  }
  fastify.log.info({ envConfig })
  fastify.serverIsReady = true
}).catch(err => {
  fastify.log.error(err)
  process.exit(1)
})
