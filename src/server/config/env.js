// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import envSchema from 'env-schema'

export default envSchema({
  schema: {
    type: 'object',
    properties: {
      NODE_ENV: {
        description: 'Type of environment. Can be either "development" or "production".',
        type: 'string',
        enum: ['development', 'production'],
        default: 'development'
      },
      ABSD_SERVER_HOST: {
        description: 'The IP adress to listen to',
        type: 'string',
        default: '127.0.0.1'
      },
      ABSD_SERVER_PORT: {
        description: 'Port of the server',
        type: 'number',
        default: 3000
      },
      ABSD_SERVER_PROXY: {
        description: 'If the application is behind a proxy',
        type: 'boolean',
        default: false
      },
      ABSD_DB_HOST: {
        description: 'IP address of the database',
        type: 'string',
        default: '127.0.0.1'
      },
      ABSD_DB_PORT: {
        description: 'Port of the database',
        type: 'number',
        default: 27017
      },
      ABSD_DB_NAME: {
        description: 'Name of the database',
        type: 'string',
        default: 'absd'
      }
    }
  }
})
