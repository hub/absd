// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream } from 'node:fs'
import { readdir } from 'node:fs/promises'
import { dirname, resolve } from 'node:path'
import { pipeline } from 'node:stream/promises'
import { fileURLToPath } from 'node:url'
import { SplitFastaStream } from './streams/SplitFastaStream.js'
import { ParseFastaStream } from './streams/ParseFastaStream.js'
import { ValidateFastaStream } from './streams/ValidateFastaStream.js'

// =========================================================================

const currentPath = dirname(fileURLToPath(import.meta.url))
const dataFolderPath = resolve(currentPath, '../../data')
const dataFolder = await readdir(dataFolderPath)
const fastaList = dataFolder.filter(fileName => {
  return fileName.endsWith('.fasta')
})

for (const fastaFileName of fastaList) {
  // Validate that the fasta file name follows the format "Genus_species.fasta".
  if (!ValidateFastaStream.fileNameFormat.test(fastaFileName)) {
    console.log(`\x1B[1;31mError with file name: ${fastaFileName} \nFasta file names must follow the format "Genus_species.fasta".\x1B[0m`)
    process.exit(1)
  }

  const fastaFile = resolve(dataFolderPath, fastaFileName)

  await pipeline(
    createReadStream(fastaFile, { encoding: 'utf-8' }),
    new SplitFastaStream(),
    new ParseFastaStream(),
    new ValidateFastaStream(),
  ).then(async () => {
    console.log(`${fastaFile}: \x1B[1;32mvalidation complete\x1B[0m`)
  }).catch(err => {
    console.log(`${fastaFile}: \x1B[1;31merror\x1B[0m`)
    console.log(err.message)
    process.exit(1)
  })
}
