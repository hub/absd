// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream } from 'node:fs'
import { readdir } from 'node:fs/promises'
import { dirname, resolve } from 'node:path'
import { pipeline } from 'node:stream/promises'
import { fileURLToPath } from 'node:url'
import { SplitFastaStream } from './streams/SplitFastaStream.js'
import { ParseFastaStream } from './streams/ParseFastaStream.js'
import { SanitizeFastaStream } from './streams/SanitizeFastaStream.js'
import { BuildAntibodiesStream } from './streams/BuildAntibodiesStream.js'
import { StatisticsStream } from './streams/StatisticsStream.js'
import { Statistics } from '../server/models/Statistics.js'

// =========================================================================

export default async function () {
  const currentPath = dirname(fileURLToPath(import.meta.url))
  const dataFolderPath = resolve(currentPath, '../../data')
  const dataFolder = await readdir(dataFolderPath)
  const fastaList = dataFolder.filter(fileName => {
    return fileName.endsWith('.fasta')
  })

  const stats = new Statistics()

  for (const fastaFileName of fastaList) {
    const fastaFile = resolve(dataFolderPath, fastaFileName)
    const species = fastaFileName.replace('.fasta', '').replace('_', ' ')

    await pipeline(
      createReadStream(fastaFile),
      new SplitFastaStream(),
      new ParseFastaStream(),
      new SanitizeFastaStream(),
      new BuildAntibodiesStream({ species }),
      new StatisticsStream({ stats })
    ).catch(err => {
      console.log(`${fastaFile}: \x1B[1;31merror\x1B[0m`)
      console.log(err.message)
      process.exit(1)
    })
  }

  return stats
}
