// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import buildStatistics from './fastaStatsBuilder.js'

/**
 * Wrapper file of the fastaStatsBuilder to display the
 * statistics from the command line, using a npm script.
 */
buildStatistics().then(stats => {
  console.log(stats.toString())
  process.exit(0)
}).catch(err => {
  console.log(err)
  process.exit(1)
})
