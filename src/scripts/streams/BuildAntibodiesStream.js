// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "stream";

/**
 * Turn a fasta entry into an antibody chain object.
 * An entry header contains several headers, delimited by three pipes,
 * gathered from different sources. Each of these subheaders contains
 * their id, the V gene segment and the name of the source at the end,
 * delimited by semicolons:
 * header;id;vGeneSegment;source|||header;id;vGeneSegment;source...
 * Example: 6W7S|||6W7S_3|Chain C[auth L]|2G10 (Fab light chain)|Homo sapiens (9606);6w7s;IGLV5;SACS
 * @param chain - The fasta entry containing the information of an antibody chain
 * @returns The antibody chain object
 */
function buildAntibodyChain(chain) {
  const [, ...headers] = chain.header.split('|||')
  const chainHeaders = []

  headers.forEach(header => {
    const splittedHeader = header.match(/(.+);(.+);(.+);(.+)$/) ?? []
    const headerText = splittedHeader[1]
    const id = splittedHeader[2]
    const vGeneSegment = splittedHeader[3]
    const source = splittedHeader[4]

    chainHeaders.push({
      id,
      vGeneSegment,
      source,
      header: headerText
    })
  })

  return {
    headers: chainHeaders,
    sequence: chain.sequence,
    rawFasta: chain.header
  }
}

// =========================================================================

/**
 * Stream to build the antibodies objects from id, species and
 * the buildAntibodyChain function.
 */
export class BuildAntibodiesStream extends Transform {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.species = options.species;
    this.lightChain = null;
  }

  _transform(fastaEntry, encoding, done) {
    // Light chains always come first, so store it
    // for the next entry.
    if (!this.lightChain) {
      this.lightChain = fastaEntry;
      return done();
    }

    // If a light chain is stored, then we can build
    // the antibody.
    this.push({
      id: this.lightChain.header.split('|||')[0],
      species: this.species,
      lightChain: buildAntibodyChain(this.lightChain),
      heavyChain: buildAntibodyChain(fastaEntry)
    });

    this.lightChain = null;
    return done();
  }
}
