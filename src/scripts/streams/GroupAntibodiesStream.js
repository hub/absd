// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "stream";

/**
 * Group antibodies in a single array and push this array
 * to the next stream for insertion in the database.
 * Doing so allows a faster insertion.
 * The size of the group can be changed via the constructor parameters.
 */
export class GroupAntibodiesStream extends Transform {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.groupSize = options.groupSize;
    this.group = [];
  }

  _transform(antibody, encoding, done) {
    if (this.group.length >= this.groupSize) {
      this.push(this.group);
      this.group = [];
    }

    this.group.push(antibody);

    return done();
  }

  _flush(done) {
    if (this.group.length > 0) {
      this.push(this.group);
    }

    return done();
  }
}
