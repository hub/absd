// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "stream";

/**
 * Sanitize the headers and sequence of the fasta entries by removing
 * extra spaces and tabs.
 */
export class SanitizeFastaStream extends Transform {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);
  }

  _transform(fastaEntry, encoding, done) {
    const sanitizedHeader = fastaEntry.header.trim().replace(/\s+/g, ' ');
    const sanitizedSequence = fastaEntry.sequence.trim();

    fastaEntry.header = sanitizedHeader;
    fastaEntry.sequence = sanitizedSequence;

    this.push(fastaEntry);

    return done();
  }
}
