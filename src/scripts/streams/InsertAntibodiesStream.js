// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { MongoClient } from "mongodb";
import { Writable } from "stream";
import config from "../../server/config/env.js";

/**
 * Insert groups of antibodies into the MongoDB database.
 */
export class InsertAntibodiesStream extends Writable {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.host = options.host;
    this.port = options.port;
    this.name = options.name;
    this.mongoClient = null;
    this.collection = null;
    this.totalInserted = 0;
  }

  // Create a database connection after stream initialization
  _construct(done) {
    const uri = `mongodb://${config.ABSD_DB_HOST}:${config.ABSD_DB_PORT}`;
    this.mongoClient = new MongoClient(uri);
    const db = this.mongoClient.db(config.ABSD_DB_NAME);
    this.collection = db.collection('antibodies');
    return done();
  }

  // Insert array of antibodies
  _write(antibodiesGroup, encoding, done) {
    this.collection.insertMany(antibodiesGroup).then(results => {
      this.totalInserted += results.insertedCount;
      return done();
    }).catch(err => {
      return done(err);
    });
  }

  // Log the results of insertion and close
  // the database connection after the stream ends.
  _final(done) {
    console.log(`\x1B[1;32mDone\x1B[0m - Antibodies inserted: \x1B[1m${this.totalInserted}\x1B[0m`);
    this.mongoClient.close()
      .then(() => {
        return done();
      })
      .catch(err => {
        return done(err);
      });
  }
}
