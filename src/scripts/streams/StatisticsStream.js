// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Writable } from 'node:stream'
import { Antibody } from '../../server/models/Antibody.js'

/**
 * Build the statistics by incrementing a given stats object.
 */
export class StatisticsStream extends Writable {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.stats = options.stats
  }

  _write(antibody, encoding, done) {
    const species = antibody.species;
    const sources = Antibody.listSources(antibody);
    const segments = Antibody.listSegments(antibody);

    // Total antibodies
    this.stats.antibodies += 1;

    // Total species
    this.stats.species.add(antibody.species);

    // Total sources
    this.stats.sources = this.stats.sources.union(sources);

    // Antibodies per species
    this.stats.antibodiesPerSpecies[species] = this.stats.antibodiesPerSpecies[species]
      ? this.stats.antibodiesPerSpecies[species] + 1
      : 1;

    // Antibodies per source
    sources.forEach(source => {
      this.stats.antibodiesPerSource[source] = this.stats.antibodiesPerSource[source]
        ? this.stats.antibodiesPerSource[source] + 1
        : 1;
    });

    // Antibodies per heavy segment
    segments.heavyChain.forEach(segment => {
      this.stats.antibodiesPerHeavySegment[segment] = this.stats.antibodiesPerHeavySegment[segment]
        ? this.stats.antibodiesPerHeavySegment[segment] + 1
        : 1;
    });

    // Antibodies per light segment
    segments.lightChain.forEach(segment => {
      this.stats.antibodiesPerLightSegment[segment] = this.stats.antibodiesPerLightSegment[segment]
        ? this.stats.antibodiesPerLightSegment[segment] + 1
        : 1;
    });

    // Antibodies only in one source
    if (sources.size === 1) {
      const source = Array.from(sources)[0];
      this.stats.antibodiesOnlyInSource[source] = this.stats.antibodiesOnlyInSource[source]
        ? this.stats.antibodiesOnlyInSource[source] + 1
        : 1;
    }

    // Antibodies in multiple sources
    if (sources.size >= 1) {
      this.stats.antibodiesInMultipleSources[sources.size] = this.stats.antibodiesInMultipleSources[sources.size]
        ? this.stats.antibodiesInMultipleSources[sources.size] + 1
        : 1;
    }

    return done();
  }
}
