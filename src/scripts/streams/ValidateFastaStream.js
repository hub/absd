// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createHash } from 'crypto';
import { Writable } from 'stream';

/**
 * Stream for validating fasta entries.
 */
export class ValidateFastaStream extends Writable {
  static fileNameFormat = /^[A-Z][a-z]+_[a-z]+\.fasta$/
  static idFormat = /^[^\s^]+$/
  static subHeaderFormat = /.+;[^\s]+;[^\s]+;[^\s]+$/
  static sequenceFormat = /^[ABCDEFGHIKLMNPQRSTUVWXYZ]+$/

  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.sequenceHashes = new Set();
  }

  _write(fastaObject, encoding, done) {
    const [id, ...headers] = fastaObject.header.split('|||');
    const sequence = fastaObject.sequence;

    if (!ValidateFastaStream.idFormat.test(id)) {
      return done(new Error(`\x1B[1;31mError with header: ${header} \nID must not contain any space.\x1B[0m`));
    }

    for (const subheader of headers) {
      if (!ValidateFastaStream.subHeaderFormat.test(subheader)) {
        return done(new Error(`\x1B[1;31mError with subheader: ${subheader} \nThe subheaders must have an id, V gene segment and source at their end, separated by semicolons: header;id;vGeneSegment;source. ID, V gene segment and source must not contain any space.\x1B[0m`));
      }
    }

    if (!ValidateFastaStream.sequenceFormat.test(sequence)) {
      return done(new Error(`\x1B[1;31mError with sequence: ${sequence} \nSequences must only contain characters among "ABCDEFGHIKLMNPQRSTUVWXYZ".\x1B[0m`));
    }

    const hash = createHash('sha256').update(sequence).digest('hex');

    // Check the uniqueness of each sequence trough the whole fasta file
    if (this.sequenceHashes.has(hash)) {
      return done(new Error(`\x1B[1;31mDuplication error with sequence: ${sequence} \nThis one already exist in the file.`));
    } else {
      this.sequenceHashes.add(hash);
    }

    return done();
  }
}
