// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { Transform } from "stream";

/**
 * Buffer and decode each chunk into a series of fasta strings,
 * each containing the header and the sequence.
 */
export class SplitFastaStream extends Transform {
  constructor(options) {
    if (!options) options = {};
    options.objectMode = true;
    super(options);

    this.buffer = '';
    this.firstLine = true;
  }

  _transform(chunk, encoding, done) {
    this.buffer += chunk.toString();

    const data = this.buffer.split(/^>/gm);
    const lastUnfinished = data.pop();

    if (this.firstLine) {
      data.shift();
      this.firstLine = false;
    }

    this.buffer = lastUnfinished;

    data.forEach((entry) => {
      this.push(entry);
    });

    return done();
  }

  _flush(done) {
    this.push(this.buffer);

    this.buffer = '';
    this.firstLine = true;

    return done();
  }
}
