// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import { createReadStream } from 'node:fs'
import { readdir, readFile } from 'node:fs/promises'
import { dirname, resolve } from 'node:path'
import { pipeline } from 'node:stream/promises'
import { fileURLToPath } from 'node:url'
import { MongoClient } from 'mongodb'
import { SplitFastaStream } from './streams/SplitFastaStream.js'
import { ParseFastaStream } from './streams/ParseFastaStream.js'
import { SanitizeFastaStream } from './streams/SanitizeFastaStream.js'
import { BuildAntibodiesStream } from './streams/BuildAntibodiesStream.js'
import { GenerateHashIdStream } from './streams/GenerateHashIdStream.js'
import { GroupAntibodiesStream } from './streams/GroupAntibodiesStream.js'
import { InsertAntibodiesStream } from './streams/InsertAntibodiesStream.js'
import AntibodySchema from '../server/schemas/Antibodies.json' with { type: "json" }
import StatisticsSchema from '../server/schemas/Statistics.json' with { type: "json" }
import config from '../server/config/env.js'

/**
 * Create or cleanup existing database to repopulate it
 * from a clean state.
 */
async function createDatabase() {
  const uri = `mongodb://${config.ABSD_DB_HOST}:${config.ABSD_DB_PORT}`
  const mongoClient = new MongoClient(uri)
  const db = mongoClient.db(config.ABSD_DB_NAME)
  const collectionsList = await db
    .listCollections()
    .toArray()

  // Remove the old collections to ensure a clean state
  if (collectionsList.find(coll => coll.name === 'antibodies')) {
    await db.dropCollection('antibodies')
  }

  if (collectionsList.find(coll => coll.name === 'statistics')) {
    await db.dropCollection('statistics')
  }

  // Create a new collection with their schema for validation
  await db.createCollection('antibodies', { validator: AntibodySchema })
  await db.createCollection('statistics', { validator: StatisticsSchema })

  // Build the indexes
  const antibodyColl = db.collection('antibodies')
  await antibodyColl.createIndex({ hashId: 1 }, { unique: true })
  await antibodyColl.createIndex({ id: 1 })
  await antibodyColl.createIndex({ species: 1 })
  await antibodyColl.createIndex({ 'heavyChain.headers.source': 1 })
  await antibodyColl.createIndex({ 'lightChain.headers.source': 1 })
  await antibodyColl.createIndex({ 'heavyChain.headers.vGeneSegment': 1 })
  await antibodyColl.createIndex({ 'lightChain.headers.vGeneSegment': 1 })

  console.log(`Database \x1B[1;32m${config.ABSD_DB_NAME}\x1B[0m created`)
}

/**
 * Insert the last statistics file data into the database.
 * This process is simple enough to not need streams.
 * @param {object} stats - The statistics object
 * @param {object} dbConfig - An object with the database configuration
 * @returns {Promise<object>} - A promise with the results of the insertion.
 */
async function insertStatistics(stats, dbConfig) {
  const uri = `mongodb://${dbConfig.host}:${dbConfig.port}`
  const mongoClient = new MongoClient(uri)
  const db = mongoClient.db(dbConfig.name)
  const collection = db.collection('statistics')

  return collection.insertOne(stats)
}

/**
 * Get the last statistics from the stats files.
 * @returns {Promise<object>} A promise with the statistics object.
 */
async function getStatistics() {
  const currentPath = dirname(fileURLToPath(import.meta.url))
  const dataFolderPath = resolve(currentPath, '../../data')
  const dataFolder = await readdir(dataFolderPath)

  const statsFilePath = dataFolder
    .filter((file) => { return file.endsWith('-stats.json') })
    .sort()
    .reverse()
    .map(file => { return resolve(dataFolderPath, file) })
    .shift()

  const statsDate = new Date(statsFilePath.match(/(\d{4}-\d{2}-\d{2})-stats/)[1])
  const statsFile = await readFile(statsFilePath, 'utf8')
  const stats = JSON.parse(statsFile)
  stats.versionDate = statsDate

  return stats
}

// =========================================================================

export default async function buildDatabase() {
  const currentPath = dirname(fileURLToPath(import.meta.url))
  const dataFolderPath = resolve(currentPath, '../../data')
  const dataFolder = await readdir(dataFolderPath)
  const fastaList = dataFolder.filter(fileName => {
    return fileName.endsWith('.fasta')
  })
  const stats = await getStatistics()

  await createDatabase()
  await insertStatistics(stats, {
    host: config.ABSD_DB_HOST,
    port: config.ABSD_DB_PORT,
    name: config.ABSD_DB_NAME
  })

  for (const fastaFileName of fastaList) {
    const fastaFile = resolve(dataFolderPath, fastaFileName)
    const species = fastaFileName.replace('.fasta', '').replace('_', ' ')

    console.log(`Inserting ${fastaFileName}...`)

    await pipeline(
      createReadStream(fastaFile),
      new SplitFastaStream(),
      new ParseFastaStream(),
      new SanitizeFastaStream(),
      new BuildAntibodiesStream({ species }),
      new GenerateHashIdStream(),
      new GroupAntibodiesStream({ groupSize: 1000 }),
      new InsertAntibodiesStream({
        host: config.ABSD_DB_HOST,
        port: config.ABSD_DB_PORT,
        name: config.ABSD_DB_NAME
      })
    ).catch(err => {
      console.log(`${fastaFile}: \x1B[1;31merror\x1B[0m`)
      console.log(err)
      process.exit(1)
    })
  }
}
