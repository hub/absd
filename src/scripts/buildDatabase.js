// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import buildDatabase from './databaseBuilder.js'
import config from '../server/config/env.js'

console.log(`--- Start building the \x1B[1m${config.ABSD_DB_NAME}\x1B[0m database`)

/**
 * Wrapper of the databaseBuilder to build
 * the database from the command line, with the
 * npm script 'npm run build:db'.
 */
buildDatabase().then(() => {
  console.log('--- \x1B[32mDatabase successfully created !\x1b[0m')
  process.exit(0)
}).catch(err => {
  console.log(JSON.stringify(err, null, 2))
  process.exit(1)
})
