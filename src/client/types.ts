// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import type { APIRequest } from './models/APIRequest'

// Types and interfaces used through the application
// =========================================================================

/**
 * The main State of teh app, handled by the store.
 */
export interface AppState {
  antibodies: Antibody[]
  antibodiesCount: number
  archives: Array<string>
  statistics: Readonly<Statistics> | null
  isDownloading: boolean
  isFetchingAntibodies: boolean
  isFetchingCount: boolean
  noResults: boolean
  request: APIRequest
  readonly sourceMeta: SourceMeta
}

/**
 * The structure of main antibody object used in
 * the application.
 */
export interface Antibody {
  id: string
  hashId: string
  species: string
  heavyChain: {
    headers: FastaHeader[]
    sequence: string
  }
  lightChain: {
    headers: FastaHeader[]
    sequence: string
  }
}

/**
 * Parsed fasta header from the original data file
 * representing one chain fasta header among the list.
 */
export interface FastaHeader {
  id: string
  vGeneSegment: string
  source: string
  header: string
}

/**
 * Structure of the sources.json file in the data folder.
 * This file is directly imported in the front-end code and thus
 * needs typing.
 */
export type SourceMeta = Record<FastaHeader['source'], {
  link?: string
}>

/**
 * The structure of statistics object used in
 * the application.
 */
export interface Statistics {
  versionDate: string
  antibodies: number
  species: Array<Antibody['species']>
  sources: Array<FastaHeader['source']>
  antibodiesPerSpecies: Record<Antibody['species'], number>
  antibodiesPerSource: Record<FastaHeader['source'], number>,
  antibodiesOnlyInSource: Record<FastaHeader['source'], number>
  antibodiesInMultipleSources: Record<string, number>
  antibodiesPerHeavySegment: Record<FastaHeader['vGeneSegment'], number>
  antibodiesPerLightSegment: Record<FastaHeader['vGeneSegment'], number>
}

/**
 * The filters for the antibodies requests.
 */
export interface APIFilters {
  keywords: string
  species: Array<Antibody['species']>
  sources: Array<FastaHeader['source']>
  heavySegments: Array<FastaHeader['vGeneSegment']>
  lightSegments: Array<FastaHeader['vGeneSegment']>
  motif: string
}

/**
 * The pagination parameters for the antibodies requests.
 */
export interface APIPagination {
  limit: number
  skip: number
}

/**
 * The sorting parameters for the antibodies requests.
 */
export interface APISort {
  by: string
  order: SortOrder
}

/**
 * Values to describe an ascending or descending order
 * for sorting features.
 */
export type SortOrder = 'asc' | 'desc'

/**
 * The request send to the server to fetch the antibodies.
 */
export type APIFetchParams = Partial<APIFilters & APIPagination & APISort>
export type APICountParams = Partial<APIFilters>
export type APIDownloadParams = Partial<APIFilters & { format?: APIDownloadFormat }>
export type APIDownloadFormat = 'fasta' | 'json'

/**
 * The response from the server when fetching antibodies.
 */
export type APIFetchResponse = Antibody[]

/**
 * The response from the server when fetching the total
 * number of antibodies.
 */
export interface APICountResponse {
  count: number
}

/**
 * The response from the server when requesting a download
 * of the main table data. It only returns the name of the
 * archive to download, in order to pass it to another endpoint.
 */
export type APIDownloadResponse = {
  archiveId: string
}

/**
 * The response from the server when checking if an archive build
 * has been completed.
 */
export interface APIUserMadeArchiveResponse {
  message: string
  id?: string
  error?: string
}

/**
 * The structure of the advanced filters.
 * Basically the same as APIFilters but without keywords.
 * The use of typescript "Omit" is not possible since it does not
 * enforce the absence of a specific key, it just ignores its presence.
 * @link https://www.typescriptlang.org/docs/handbook/2/objects.html#excess-property-checks
 */
export type AdvancedFilters = {
  species: Array<Antibody['species']>
  sources: Array<FastaHeader['source']>
  heavySegments: Array<FastaHeader['vGeneSegment']>
  lightSegments: Array<FastaHeader['vGeneSegment']>
  motif: string
}

/**
 * The structure of the chips displayed by the advanced search.
 */
export type FilterChip = [keyof AdvancedFilters, string]
