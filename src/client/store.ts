// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import type { AxiosError } from 'axios'
import { defineStore } from 'pinia'
import sourceMeta from '../../data/sources.json'
import api from './api'
import { APIRequest } from './models/APIRequest'
import router from './router'
import type {
  APICountParams,
  APIDownloadParams,
  APIFetchParams,
  Antibody,
  AppState,
  FastaHeader,
  Statistics
} from './types'
import alphanumSort from './utils/alphanumSort'

export const useStore = defineStore('store', {
  state: (): AppState => {
    return {
      antibodies: [],
      antibodiesCount: 0,
      archives: [],
      statistics: null,
      isDownloading: false,
      isFetchingAntibodies: false,
      isFetchingCount: false,
      request: new APIRequest(),
      noResults: false,
      sourceMeta
    }
  },
  getters: {
    /**
     * Get the current list of sources from the statistics.
     * @param state - The state of the store
     * @returns The list of sources from the stats
     */
    antibodiesSources: (state): Statistics['sources'] => {
      return state.statistics?.sources || []
    },

    /**
     * Get the current list of species from the statistics.
     * @param state - The state of the store
     * @returns The list of species from the stats
     */
    antibodiesSpecies: (state): Statistics['species'] => {
      return state.statistics?.species || []
    },

    /**
     * Get the current list of heavy segments from the statistics.
     * @param state - The state of the store
     * @returns The list of heavy segments from the stats
     */
    antibodiesHeavySegments: (state): Array<keyof Statistics['antibodiesPerHeavySegment']> => {
      return Object
        .keys(state.statistics?.antibodiesPerHeavySegment || {})
        .sort(alphanumSort)
    },

    /**
     * Get the current list of light segments from the statistics.
     * @param state - The state of the store
     * @returns The list of light segments from the stats
     */
    antibodiesLightSegments: (state): Array<keyof Statistics['antibodiesPerHeavySegment']> => {
      return Object
        .keys(state.statistics?.antibodiesPerLightSegment || {})
        .sort(alphanumSort)
    },

    /**
     * Get the current version date of the database.
     * @param state - The state of the store
     * @returns The version date as an ISO string
     */
    antibodiesVersionDate: (state): Statistics['versionDate'] => {
      return state.statistics?.versionDate.replace(/T.*/, "") || ""
    }
  },
  actions: {
    /**
     * The main action for fetching antibodies from the server,
     * used inside components.
     * Stores the results in the corresponding store variable
     * instead of returning anything.
     * @param request - The request parameters for getting antibodies
     */
    fetchAntibodies(request: APIFetchParams): void {
      this.isFetchingAntibodies = true
      api.fetchAntibodies(request).then(response => {
        this.noResults = response.data.length === 0
        this.antibodies = response.data
      }).catch((err: AxiosError) => {
        alert(err.message)
        console.log(err)
      }).finally(() => {
        this.isFetchingAntibodies = false
      })
    },

    /**
     * The main action to count the total number of antibodies
     * matching a request.
     * Stores the results in the corresponding store variable
     * instead of returning anything.
     * @param countRequest - The request parameters for counting
     */
    countAntibodies(countRequest: APICountParams): void {
      // Avoid to send an empty request to get an information
      // we already have in the stats.
      if (Object.keys(countRequest).length === 0 && this.statistics?.antibodies) {
        this.antibodiesCount = this.statistics.antibodies
        return
      }

      this.isFetchingCount = true
      api.countAntibodies(countRequest).then(response => {
        this.antibodiesCount = response.data.count
      }).catch((err: AxiosError) => {
        alert(err.message)
        console.log(err)
      }).finally(() => {
        this.isFetchingCount = false
      })
    },

    /**
     * Action to trigger the building of an archive by the server
     * in order to download it.
     * Re-route to the download waiting page.
     * @param request - The request parameters to filter antibodies and set the archive format
     */
    downloadAntibodies(request: APIDownloadParams): void {
      this.isDownloading = true
      api.downloadAntibodies(request).then(response => {
        router.push({
          name: 'DownloadPage',
          params: {
            archiveId: response.data.archiveId
          }
        })
      }).catch((err: AxiosError) => {
        alert(err.message)
        console.log(err)
      }).finally(() => {
        this.isDownloading = false
      })
    },

    /**
     * Get the database statistics from the server and stores it in the
     * corresponding variable in the store.
     */
    getStatistics(): void {
      api.getStatistics().then(response => {
        this.statistics = Object.freeze(response.data)
      }).catch((err: AxiosError) => {
        alert(err.message)
        console.log(err)
      })
    },

    /**
     * Build a link to an antibody external resource using the template
     * in the sources metadata. Those can be found in the `data/sources.json` file.
     * @param id - One of the antibody ID found in its FASTA headers
     * @param source - The name of the source
     * @returns The corresponding link, usually a url.
     */
    getSourceLink(id: Antibody['id'], source: FastaHeader['source']): string {
      const link = this.sourceMeta[source]?.link
      if (link === undefined) return ''
      return link.replace(/{id}/g, id)
    },

    /**
     * Fetch the list of pre-made archives from the server
     * and stores it in the store.
     */
    fetchArchives(): void {
      api.fetchArchives().then(response => {
        this.archives = response.data
      }).catch((err: AxiosError) => {
        alert(err.message)
        console.log(err)
      })
    }
  }
})
