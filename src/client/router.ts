// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { createRouter, createWebHistory } from 'vue-router'

// =========================================================================

export default createRouter({
  // Use the History API of the browser
  history: createWebHistory(),

  // Main list of routes for the application
  routes: [
    {
      path: '/',
      component: async () => await import('./components/TheHomePage.vue'),
      name: 'HomePage'
    },
    {
      path: '/statistics',
      component: async () => await import('./components/TheStatisticsPage.vue'),
      name: 'StatisticsPage'
    },
    {
      path: '/antibody/:hashId',
      component: async () => await import('./components/TheAntibodyPage.vue'),
      name: 'AntibodyPage',
      props: true
    },
    {
      path: '/antibodies/downloads/:archiveId',
      component: async () => await import('./components/TheDownloadPage.vue'),
      name: 'DownloadPage',
      props: true
    },
    {
      path: '/about',
      component: async () => await import('./components/TheAboutPage.vue'),
      name: 'AboutPage'
    },
    {
      path: '/:pathMatch(.*)*',
      component: async () => await import('./components/The404Page.vue'),
      name: '404Page'
    }
  ]
})
