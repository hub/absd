// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/**
 * A custom strategy to sort the numbers inside a string numerically instead
 * of alphabatically.
 * This function is made to be used as a compare function of the array.sort()
 * method.
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#comparefn
 * @param item - Current item of the iterator
 * @param nextItem - Next item of the iterator to compare with the current one
 * @returns -1 if item should come before nextItem, 1 if it should come after, 0 if equals.
 */
export default function alphanumSort(item: string, nextItem: string): -1 | 0 | 1 {

  // Split each doc by groups of letters and numbers
  const chunks = item.match(/([^\d]+)|([\d]+)/g) ?? []
  const nextChunks = nextItem.match(/([^\d]+)|([\d]+)/g) ?? []

  for (let i = 0; i < chunks.length; i++) {

    // If the next chunks is shorter and equal so far, put it before
    if (!nextChunks[i]) {
      return 1
    }

    // Sort by numbers if letters are equal so far
    if (Number.parseInt(chunks[i])) {
      if (Number.parseInt(chunks[i]) > Number.parseInt(nextChunks[i])) return 1
      if (Number.parseInt(chunks[i]) < Number.parseInt(nextChunks[i])) return -1
    }

    // Sort by letters
    else {
      if (chunks[i] > nextChunks[i]) return 1
      if (chunks[i] < nextChunks[i]) return -1
    }
  }

  // If the current gene is shorter and equal so far, put it before
  return -1
};
