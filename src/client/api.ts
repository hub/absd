// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from 'axios'
import qs from 'qs'
import type {
  APICountParams,
  APICountResponse,
  APIDownloadParams,
  APIDownloadResponse,
  APIFetchParams,
  APIFetchResponse,
  APIUserMadeArchiveResponse,
  Antibody,
  Statistics
} from './types'

// Initialize Axios instance
// =========================================================================

const apiURL = import.meta.env.BASE_URL
const base = axios.create({
  baseURL: apiURL,
  paramsSerializer: (params) => {
    return qs.stringify(params, { arrayFormat: 'comma' })
  }
})

// API
// =========================================================================

export default Object.freeze({
  fetchAntibodies: async (params: APIFetchParams): Promise<AxiosResponse<APIFetchResponse>> => {
    return await base.get('/api/antibodies', { params })
  },
  fetchOneAntibody: async (hashId: Antibody['hashId']): Promise<AxiosResponse<Antibody | null>> => {
    return await base.get(`/api/antibodies/${hashId}`)
  },
  countAntibodies: async (params: APICountParams): Promise<AxiosResponse<APICountResponse>> => {
    return await base.get('/api/antibodies/count', { params })
  },
  downloadAntibodies: async (params: APIDownloadParams): Promise<AxiosResponse<APIDownloadResponse>> => {
    return await base.get('/api/antibodies/download', { params })
  },
  getStatistics: async (): Promise<AxiosResponse<Statistics>> => {
    return await base.get('/api/statistics')
  },
  fetchArchives: async (): Promise<AxiosResponse<string[]>> => {
    return await base.get('/api/downloads')
  },
  fetchUserMadeArchive: async (archiveName: string): Promise<AxiosResponse<APIUserMadeArchiveResponse>> => {
    return await base.get(`/api/antibodies/download/${archiveName}`)
  },
  userMadeArchiveURL: (archiveName: string): string => {
    return `/api/antibodies/download/${archiveName}/file`
  },
  archiveURL: (archiveName: string): string => {
    return `/api/downloads/${archiveName}`
  }
})
