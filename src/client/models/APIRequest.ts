// ABSD
// Copyright (C) 2025 Institut Pasteur
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import type {
  APIFetchParams,
  APIFilters,
  APIPagination,
  APISort,
  AdvancedFilters
} from "../types"

/**
 * Encapsulate the logic of the main request used to fetch the antibodies.
 * This object is used by the application components that need to set up
 * parts of the request.
 */
export class APIRequest {
  /**
   * The filters for the antibodies request.
   */
  filters: APIFilters = {
    keywords: '',
    species: [],
    sources: [],
    heavySegments: [],
    lightSegments: [],
    motif: ''
  }

  /**
   * The pagination parameters to limit the number
   * of fetched antibodies.
   */
  pagination: APIPagination = {
    limit: 20,
    skip: 0
  }

  /**
   * The sorting parameters of the fetched antibodies.
   */
  sort: APISort = {
    by: '',
    order: 'asc'
  }

  /**
   * List the advanced filters, which corresponds to
   * the filters without the keywords.
   * @returns The list of list of advanced filters
   */
  advancedFilters(): AdvancedFilters {
    const { keywords, ...advancedFilters} = { ...this.filters }
    return advancedFilters
  }

  /**
   * Build and return the fetch params to pass to the API.
   * Contains the filters, pagination and sorting parameters,
   * to fetch antibodies.
   * @returns The formatted API params for fetching antibodies
   */
  fetchParams(): Partial<APIFetchParams> {
    const filters: Partial<APIFilters> = {}
    let filter: keyof APIFilters

    for (filter in this.filters) {
      const value = this.filters[filter]

      if (value.length > 0) {
        // TODO: Enhance this "as" type or change this process
        // This needed type is stupid af but typescript is not helping
        filters[filter] = value as string[] & string
      }
    }

    const pagination = this.pagination

    const sort = this.sort.by
      ? { sortBy: this.sort.by, sortOrder: this.sort.order }
      : {}

    return {
      ...filters,
      ...pagination,
      ...sort
    }
  }

  /**
   * Build and return the count params to pass to the API.
   * Contains only filters, since its the only thing needed for counting.
   * @returns The formatted API params for counting antibodies
   */
  countParams() {
    const filters: Partial<APIFilters> = {}
    let filter: keyof APIFilters

    for (filter in this.filters) {
      const value = this.filters[filter]

      if (value.length > 0) {
        // TODO: Enhance this "as" type or change this process
        // This needed type is stupid af but typescript is not helping
        filters[filter] = value as string[] & string
      }
    }

    return filters
  }

  /**
   * Build and return the download params to pass to the API.
   * They are the same as the count params, so this is just an alias
   * for semantics clarity.
   * @returns The formatted API params for downloading antibodies
   */
  downloadParams() { return this.countParams() }
}
