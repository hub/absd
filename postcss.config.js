/**
 * This is the configuration for PostCSS.
 * @link https://postcss.org/
 *
 * This config file is automatically discovered and used by vite.
 * @link https://vitejs.dev/guide/features.html#postcss
 */
import autoprefixer from "autoprefixer"
import cssnano from "cssnano"

export default {
  plugins: [
    autoprefixer,
    cssnano
  ]
}
